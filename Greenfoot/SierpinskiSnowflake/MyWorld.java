import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class MyWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MyWorld extends World
{

    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1200, 1000, 1); 

        prepare();
    }

    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare()
    {
        double length = 800;
        double theta0 = -Math.PI/12;
        double x0 = 100;
        double y0 = 350;
        double x1 = x0 + length * Math.cos(theta0);
        double y1 = y0 + length * Math.sin(theta0);
        double x2 = x1 + length * Math.cos(theta0 + 2*Math.PI/3);
        double y2 = y1 + length * Math.sin(theta0 + 2*Math.PI/3);
        
        Line line1 = new OuterLine(x0, y0, x1, y1, true);
        Line line2 = new OuterLine(x1, y1, x2, y2, true);
        Line line3 = new OuterLine(x2, y2, x0, y0, true);
        
        addObject(line1, 0, 0);
        addObject(line2, 0, 0);
        addObject(line3, 0, 0);
    }
}
