import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
//import java.awt.geom.Line2D.Double;

public class Line extends Actor
{
    double x1, y1, x2, y2, midX, midY, length; //Should be doubles
    int x1Adj, x2Adj, y1Adj, y2Adj;
    boolean hasIterated;
    boolean draw;
    
    public Line(double xStart, double yStart, double xEnd, double yEnd, boolean shouldDraw)
    {
        x1 = xStart;
        y1 = yStart;
        x2 = xEnd;
        y2 = yEnd;
        hasIterated = false;
        
        x1Adj = (int) (x1 - Math.min(x1, x2));
        x2Adj = (int) (x2 - Math.min(x1, x2));
        y1Adj = (int) (y1 - Math.min(y1, y2));
        y2Adj = (int) (y2 - Math.min(y1, y2));
        
        midX = (x1+x2)/2;
        midY = (y1+y2)/2;
        
        length = Math.hypot(x2-x1, y2-y1);
        
        draw = shouldDraw;
    }
    
    protected void addedToWorld(World world)
    {
        this.setImage(drawSelf());
    }
    
    private GreenfootImage drawSelf()
    {
        GreenfootImage image = new GreenfootImage((int)Math.abs(x2-x1)+2, (int)Math.abs(y2-y1)+2);
        double xMin = Math.min(x1, x2);
        double yMin = Math.min(y1, y2);
        
        if(draw)
        {
            image.drawShape((java.awt.Shape)(new java.awt.geom.Line2D.Double(x1-xMin, y1-yMin, x2-xMin, y2-yMin)));
            this.setLocation((int)((x1+x2)/2)-1, (int)((y1+y2)/2)-1);
        }
        return image;
    }
    
    public void act() 
    {
        if(!hasIterated)
        {
            if(length > 6)
            {
                //this.setImage(drawSelf());
                
                iterate();
            }
            hasIterated = true;
        }
    }
    
    public void iterate()
    {
        double theta = Math.atan2(y2-y1, x2-x1);
        double newX = midX + (length/2.0)*Math.cos(theta + Math.PI/3.0);
        double newY = midY + (length/2.0)*Math.sin(theta + Math.PI/3.0);
        
        Line innerLine1 = new Line(newX, newY, midX, midY, true);
        getWorld().addObject(innerLine1, 0, 0);
        getWorld().addObject(new Line(x1, y1, midX, midY, false), 0, 0);
        getWorld().addObject(new Line(midX, midY, x2, y2, false), 0, 0);
    }
}
