import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class OuterLine here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class OuterLine extends Line
{
    boolean newTriangle;
    
    public OuterLine(double xStart, double yStart, double xEnd, double yEnd, boolean isNewTri)
    {
        super(xStart, yStart, xEnd, yEnd, isNewTri);
        newTriangle = isNewTri;
    }
    
    public void iterate()
    {
        double theta = Math.atan2(y2-y1, x2-x1);
        double newX = midX + (length/2.0)*Math.cos(theta + Math.PI/3.0);
        double newY = midY + (length/2.0)*Math.sin(theta + Math.PI/3.0);
        
        if(newTriangle)
        {
            Line innerLine1 = new Line(newX, newY, midX, midY, true);
            getWorld().addObject(innerLine1, 0, 0);
            getWorld().addObject(new Line(x1, y1, midX, midY, false), 0, 0);
            getWorld().addObject(new Line(midX, midY, x2, y2, false), 0, 0);
        }
        
        double thirdX = (x1+x1+x2)/3;
        double thirdY = (y1+y1+y2)/3;
        newX = thirdX + (length/3)*Math.cos(theta - Math.PI/3);
        newY = thirdY + (length/3)*Math.sin(theta - Math.PI/3);
        double third2X = (x1+x2+x2)/3;
        double third2Y = (y1+y2+y2)/3;
        
        getWorld().addObject(new OuterLine(x1, y1, thirdX, thirdY, false), 0, 0);
        getWorld().addObject(new OuterLine(thirdX, thirdY, newX, newY, true), 0, 0);
        getWorld().addObject(new OuterLine(newX, newY, third2X, third2Y, true), 0, 0);
        getWorld().addObject(new OuterLine(third2X, third2Y, x2, y2, false), 0, 0);
        getWorld().addObject(new Line(third2X, third2Y, thirdX, thirdY, false), 0, 0);
    }
}
