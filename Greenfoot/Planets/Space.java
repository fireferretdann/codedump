import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.LinkedList;

public class Space extends World
{
    final static int WIDTH = 1500;
    final static int HEIGHT = 800;
    final double G = 1;
    double[] centerOfMass = new double[2];

    public Space()
    {    
        super(WIDTH, HEIGHT, 1);
        prepare();
    }

    public LinkedList<Planet> getPlanets()
    {
        return new LinkedList<Planet>(this.getObjects(Planet.class));
    }

    public void act()
    {
        calculateCenterOfMass();
        for(Planet p : getPlanets())
            p.prepareMove();
        for(Planet p : getPlanets())
            p.move();
    }
    
    private void prepare()
    {
        Planet planet = new Planet(100, -.5, .5);
        addObject(planet,675,340);
        Planet planet2 = new Planet(75, .4, -.4);
        addObject(planet2,745,407);
        Planet planet3 = new Planet(.1, -.1, -.3);
        addObject(planet3,1247,244);
    }
    
    public void calculateCenterOfMass()
    {
        centerOfMass[0] = 0;
        centerOfMass[1] = 0;
        
        double totalMass = 0;
        LinkedList<Planet> planets = getPlanets();
        for(Planet p : planets)
        {
            centerOfMass[0] += p.x*p.m;
            centerOfMass[1] += p.y*p.m;
            
            totalMass += p.m;
        }
        
        centerOfMass[0] /= totalMass;
        centerOfMass[1] /= totalMass;
        
        centerOfMass[0] -= WIDTH/2;
        centerOfMass[1] -= HEIGHT/2;
    }
}
