import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Planet here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Planet extends Actor
{
    Space space;
    double x;
    double y;
    double vx;
    double vy;
    double m;
    
    public void addedToWorld(World w)
    {
        space = (Space)w;
        x = this.getX();
        y = this.getY();
    }
    
    public Planet(double mass, double velocityX, double velocityY)
    {
        m = mass;
        vx = velocityX;
        vy = velocityY;
    }
    
    public void move()
    {
        x += vx;
        y += vy;
        
        this.setLocation((int)(x - space.centerOfMass[0]), (int)(y - space.centerOfMass[1]));
    }
    
    public void prepareMove()
    {
        for(Planet p : space.getPlanets())
        {
            if(!p.equals(this))
                gravitateTo(p);
        }
    }
    
    public void gravitateTo(Planet p)
    {
        double dx = p.x-this.x;
        double dy = p.y-this.y;
        double distance = Math.sqrt(dx*dx + dy*dy);
        double angle = Math.atan2(dx, dy) - Math.PI;
        
        double gravityMag = -space.G * p.m / (distance*distance);
        
        vx += gravityMag * Math.sin(angle);
        vy += gravityMag * Math.cos(angle);
    }
}
