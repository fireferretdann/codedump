import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Display here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Display extends Actor
{
    public static final Font FONT = new Font(true, false, 25);
    public static final Color ROULETTE_RED = new Color(227, 30, 36);
    public String text = "Texty Text";
    
    public Display()
    {
        refresh();
    }
    
    public void refresh()
    {
        setImage(new GreenfootImage(15*text.length(), 30));
        getImage().setColor(ROULETTE_RED);
        getImage().setFont(FONT);
        getImage().drawString(text, 5, 23);
    }
}
