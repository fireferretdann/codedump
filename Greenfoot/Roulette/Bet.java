import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


public class Bet extends Actor
{
    public static final int MINIMUM_BET = 1;
    public static final Font FONT = new Font(true, false, 20);
    
    public int winningNumbers[];
    public int payoffMultiplier; // Ex: for a 2:1 ratio this would be 3
    public int betAmount;
    public static GreenfootImage defaultImage = new GreenfootImage(30, 30);
    public Roulette board;
    
    public Bet(int[] nums, int returns)
    {
        winningNumbers = nums;
        payoffMultiplier = returns;
        this.setImage(new GreenfootImage(defaultImage));
        betAmount = 0;
    }
    
    public void addedToWorld(World world)
    {
        board = (Roulette) world;
    }
    
    public void addTo()
    {
        betAmount += MINIMUM_BET;
        board.cash -= MINIMUM_BET;
        refreshImage();
    }
    
    public void subtractFrom()
    {
        if(betAmount > 0)
        {
            betAmount -= MINIMUM_BET;
            board.cash += MINIMUM_BET;
        }
        refreshImage();
    }
    
    public void refreshImage()
    {
        if(betAmount == 0)
            this.setImage(new GreenfootImage(defaultImage));
        else
        {
            getImage().setColor(Color.MAGENTA.darker());
            getImage().fillOval(0, 0, 30, 30);
            
            getImage().setColor(Color.WHITE);
            getImage().setFont(FONT);
            if(betAmount < 10)
                getImage().drawString(""+betAmount, 10, 23);
            else
                getImage().drawString(""+betAmount, 4, 23);
        }
    }
}
