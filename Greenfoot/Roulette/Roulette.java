import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.Random;
import javafx.scene.control.TextInputDialog;

public class Roulette extends World
{
    public boolean debugging = false;
    
    public Random rand = new Random();
    public int cash = 100;
    public Display cashDisplay;
    public Display resultDisplay;
    public Display bet1;
    public Display bet100;
    public Display betN;
    public Display changeDisplay;
    public Display clearButton;
    
    private int betsLeft = 0;
    
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public Roulette()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1495, 756, 1);
        prepare();
        
        if(debugging)
        {
            Bet.defaultImage.setColor(Color.YELLOW);
            Bet.defaultImage.fill();
        }
    }
    
    public void toggleDebugging()
    {
        debugging = !debugging;
        
        if(debugging)
        {
            Bet.defaultImage.setColor(Color.YELLOW);
            Bet.defaultImage.fill();
        }
        else
        {
            Bet.defaultImage = new GreenfootImage(30,30);
            for(Bet b : getObjects(Bet.class))
                b.refreshImage();
        }
    }

    public void act()
    {
        int oldCash = cash;
        if(betsLeft > 0)
        {
            while(betsLeft > 0)
            {
                runBet();
                betsLeft --;
            }
            changeDisplay.text = "Net profit from last batch: " + (cash-oldCash);
            changeDisplay.refresh();
        }
        
        MouseInfo mouse = Greenfoot.getMouseInfo();
        
        if(mouse != null  &&  mouse.getClickCount() > 0)
        {
            if(mouse.getActor() instanceof Bet)
            {
                Bet b = (Bet)mouse.getActor();
                
                if(mouse.getButton() == 1)
                    b.addTo();
                else if(mouse.getButton() == 3)
                    b.subtractFrom();
                
                cashDisplay.text = "Cash on Hand: " + cash;
                cashDisplay.refresh();
            }
            else if(bet1.equals(mouse.getActor()))
            {
                betsLeft ++;
            }
            else if(bet100.equals(mouse.getActor()))
            {
                betsLeft += 100;
            }
            else if(betN.equals(mouse.getActor()))
            {
                int n = Integer.parseInt(Greenfoot.ask("Repeat betting this many times: "));
                betsLeft += n;
            }
            else if(cashDisplay.equals(mouse.getActor()))
            {
                int newCash = Integer.parseInt(Greenfoot.ask("What would you like to reset your cash to?"));
                cash = newCash;
                cashDisplay.text = "Cash on Hand: " + cash;
                cashDisplay.refresh();
            }
            else if(clearButton.equals(mouse.getActor()))
            {
                for(Bet b : getObjects(Bet.class))
                {
                    while(b.betAmount > 0)
                    {
                        b.subtractFrom();
                    }
                }
                cashDisplay.text = "Cash on Hand: " + cash;
                cashDisplay.refresh();
            }
        }
    }
    
    public void runBet()
    {
        int chickenDinner = rand.nextInt(37);
        int oldCash = cash;
        
        for(Bet b : getObjects(Bet.class))
        {
            for(int num : b.winningNumbers)
                if(num == chickenDinner)
                    cash += b.betAmount*b.payoffMultiplier;
            
            cash -= b.betAmount; // Re-ante's
        }
        
        resultDisplay.text = "Winning Number: " + chickenDinner;
        resultDisplay.refresh();
        cashDisplay.text = "Cash on Hand: " + cash;
        cashDisplay.refresh();
    }
    
    private void prepare()
    {
        cashDisplay = new Display();
        addObject(cashDisplay,1357,65);
        cashDisplay.text = "Cash on Hand: " + cash;
        cashDisplay.refresh();

        resultDisplay = new Display();
        addObject(resultDisplay,290,65);
        resultDisplay.text = "Winning Number: ";
        resultDisplay.refresh();

        changeDisplay = new Display();
        addObject(changeDisplay,850,65);
        changeDisplay.text = "Net profit from last batch: ";
        changeDisplay.refresh();

        bet1 = new Display();
        addObject(bet1,490,689);
        bet1.text = "Bet x1";
        bet1.refresh();

        bet100 = new Display();
        addObject(bet100,839,687);
        bet100.text = "Bet x100";
        bet100.refresh();

        betN= new Display();
        addObject(betN,1148,690);
        betN.text = "Bet xN";
        betN.refresh();

        clearButton = new Display();
        addObject(clearButton,1018,595);
        clearButton.text = "Clear Bets";
        clearButton.refresh();

        ///////////////////////////  Single Numbers  ///////////////////////

        addObject(new Bet(new int[] {0}, 36), 620, 316);

        addObject(new Bet(new int[] {1}, 36), 690, 389);
        addObject(new Bet(new int[] {2}, 36), 690, 316);
        addObject(new Bet(new int[] {3}, 36), 690, 243);

        addObject(new Bet(new int[] {4}, 36), 749, 389);
        addObject(new Bet(new int[] {5}, 36), 749, 316);
        addObject(new Bet(new int[] {6}, 36), 749, 243);

        addObject(new Bet(new int[] {7}, 36), 808, 389);
        addObject(new Bet(new int[] {8}, 36), 808, 316);
        addObject(new Bet(new int[] {9}, 36), 808, 243);

        addObject(new Bet(new int[] {10}, 36), 867, 389);
        addObject(new Bet(new int[] {11}, 36), 867, 316);
        addObject(new Bet(new int[] {12}, 36), 867, 243);

        addObject(new Bet(new int[] {13}, 36), 926, 389);
        addObject(new Bet(new int[] {14}, 36), 926, 316);
        addObject(new Bet(new int[] {15}, 36), 926, 243);

        addObject(new Bet(new int[] {16}, 36), 985, 389);
        addObject(new Bet(new int[] {17}, 36), 985, 316);
        addObject(new Bet(new int[] {18}, 36), 985, 243);

        addObject(new Bet(new int[] {19}, 36), 1044, 389);
        addObject(new Bet(new int[] {20}, 36), 1044, 316);
        addObject(new Bet(new int[] {21}, 36), 1044, 243);

        addObject(new Bet(new int[] {22}, 36), 1103, 389);
        addObject(new Bet(new int[] {23}, 36), 1103, 316);
        addObject(new Bet(new int[] {24}, 36), 1103, 243);

        addObject(new Bet(new int[] {25}, 36), 1162, 389);
        addObject(new Bet(new int[] {26}, 36), 1162, 316);
        addObject(new Bet(new int[] {27}, 36), 1162, 243);

        addObject(new Bet(new int[] {28}, 36), 1221, 389);
        addObject(new Bet(new int[] {29}, 36), 1221, 316);
        addObject(new Bet(new int[] {30}, 36), 1221, 243);

        addObject(new Bet(new int[] {31}, 36), 1280, 389);
        addObject(new Bet(new int[] {32}, 36), 1280, 316);
        addObject(new Bet(new int[] {33}, 36), 1280, 243);

        addObject(new Bet(new int[] {34}, 36), 1339, 389);
        addObject(new Bet(new int[] {35}, 36), 1339, 316);
        addObject(new Bet(new int[] {36}, 36), 1339, 243);

        //////////////////////////////////  Specials  ////////////////////////

        addObject(new Bet(new int[] {1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34}, 3), 1398, 389);
        addObject(new Bet(new int[] {2, 5, 8, 11, 14, 17, 20, 24, 27, 29, 32, 35}, 3), 1398, 316);
        addObject(new Bet(new int[] {3, 6, 9, 12, 15, 18, 21, 25, 28, 30, 33, 36}, 3), 1398, 243);

        addObject(new Bet(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}, 3), 779, 454);
        addObject(new Bet(new int[] {13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24}, 3), 1015, 454);
        addObject(new Bet(new int[] {25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36}, 3), 1251, 454);

        addObject(new Bet(new int[] {2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36}, 2), 838, 516);
        addObject(new Bet(new int[] {1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35}, 2), 1192, 516);

        addObject(new Bet(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18}, 2), 720, 516);
        addObject(new Bet(new int[] {19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36}, 2), 1310, 516);

        addObject(new Bet(new int[] {1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36}, 2), 956, 516);
        addObject(new Bet(new int[] {2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35}, 2), 1074, 516);

        addObject(new Bet(new int[] {0, 1}, 18), 661, 389);
        addObject(new Bet(new int[] {0, 2}, 18), 661, 319);
        addObject(new Bet(new int[] {0, 3}, 18), 661, 243);

        addObject(new Bet(new int[] {0, 1, 2}, 12), 661, 352);
        addObject(new Bet(new int[] {0, 2, 3}, 12), 661, 280);

        addObject(new Bet(new int[] {0, 1, 2, 3}, 9), 661, 424);

        /////////////////////////////  Horizontal Doubles  ////////////////////

        addObject(new Bet(new int[] {1, 4}, 18), 720, 389);
        addObject(new Bet(new int[] {2, 5}, 18), 720, 316);
        addObject(new Bet(new int[] {3, 6}, 18), 720, 243);

        addObject(new Bet(new int[] {4, 7}, 18), 779, 389);
        addObject(new Bet(new int[] {5, 8}, 18), 779, 316);
        addObject(new Bet(new int[] {6, 9}, 18), 779, 243);

        addObject(new Bet(new int[] {7, 10}, 18), 838, 389);
        addObject(new Bet(new int[] {8, 11}, 18), 838, 316);
        addObject(new Bet(new int[] {9, 12}, 18), 838, 243);

        addObject(new Bet(new int[] {10, 13}, 18), 897, 389);
        addObject(new Bet(new int[] {11, 14}, 18), 897, 316);
        addObject(new Bet(new int[] {12, 15}, 18), 897, 243);

        addObject(new Bet(new int[] {13, 16}, 18), 956, 389);
        addObject(new Bet(new int[] {14, 17}, 18), 956, 316);
        addObject(new Bet(new int[] {15, 18}, 18), 956, 243);

        addObject(new Bet(new int[] {16, 19}, 18), 1015, 389);
        addObject(new Bet(new int[] {17, 20}, 18), 1015, 316);
        addObject(new Bet(new int[] {18, 21}, 18), 1015, 243);

        addObject(new Bet(new int[] {19, 22}, 18), 1074, 389);
        addObject(new Bet(new int[] {20, 23}, 18), 1074, 316);
        addObject(new Bet(new int[] {21, 24}, 18), 1074, 243);

        addObject(new Bet(new int[] {22, 25}, 18), 1133, 389);
        addObject(new Bet(new int[] {23, 26}, 18), 1133, 316);
        addObject(new Bet(new int[] {24, 27}, 18), 1133, 243);

        addObject(new Bet(new int[] {25, 28}, 18), 1192, 389);
        addObject(new Bet(new int[] {26, 29}, 18), 1192, 316);
        addObject(new Bet(new int[] {27, 30}, 18), 1192, 243);

        addObject(new Bet(new int[] {28, 31}, 18), 1251, 389);
        addObject(new Bet(new int[] {29, 32}, 18), 1251, 316);
        addObject(new Bet(new int[] {30, 33}, 18), 1251, 243);

        addObject(new Bet(new int[] {31, 34}, 18), 1310, 389);
        addObject(new Bet(new int[] {32, 35}, 18), 1310, 316);
        addObject(new Bet(new int[] {33, 36}, 18), 1310, 243);

        ///////////////////////////  Vertical Doubles  ///////////////////////////

        addObject(new Bet(new int[] {1, 2}, 18), 690, 352);
        addObject(new Bet(new int[] {2, 3}, 18), 690, 280);

        addObject(new Bet(new int[] {4, 5}, 18), 749, 352);
        addObject(new Bet(new int[] {5, 6}, 18), 749, 280);

        addObject(new Bet(new int[] {7, 8}, 18), 808, 352);
        addObject(new Bet(new int[] {8, 9}, 18), 808, 280);

        addObject(new Bet(new int[] {10, 11}, 18), 867, 352);
        addObject(new Bet(new int[] {11, 12}, 18), 867, 280);

        addObject(new Bet(new int[] {13, 14}, 18), 926, 352);
        addObject(new Bet(new int[] {14, 15}, 18), 926, 280);

        addObject(new Bet(new int[] {16, 17}, 18), 985, 352);
        addObject(new Bet(new int[] {17, 18}, 18), 985, 280);

        addObject(new Bet(new int[] {19, 20}, 18), 1044, 352);
        addObject(new Bet(new int[] {20, 21}, 18), 1044, 280);

        addObject(new Bet(new int[] {22, 23}, 18), 1103, 352);
        addObject(new Bet(new int[] {23, 24}, 18), 1103, 280);

        addObject(new Bet(new int[] {25, 26}, 18), 1162, 352);
        addObject(new Bet(new int[] {26, 27}, 18), 1162, 280);

        addObject(new Bet(new int[] {28, 29}, 18), 1221, 352);
        addObject(new Bet(new int[] {29, 30}, 18), 1221, 280);

        addObject(new Bet(new int[] {31, 32}, 18), 1280, 352);
        addObject(new Bet(new int[] {32, 33}, 18), 1280, 280);

        addObject(new Bet(new int[] {34, 35}, 18), 1339, 352);
        addObject(new Bet(new int[] {35, 36}, 18), 1339, 280);

        ////////////////////////////  Four's  //////////////////////

        addObject(new Bet(new int[] {1, 2, 4, 5}, 9), 720, 352);
        addObject(new Bet(new int[] {2, 3, 5, 6}, 9), 720, 280);

        addObject(new Bet(new int[] {4, 5, 7, 8}, 9), 779, 352);
        addObject(new Bet(new int[] {5, 6, 7, 8}, 9), 779, 280);

        addObject(new Bet(new int[] {7, 8, 10, 11}, 9), 838, 352);
        addObject(new Bet(new int[] {8, 9, 11, 12}, 9), 838, 280);

        addObject(new Bet(new int[] {10, 11, 13, 14}, 9), 897, 352);
        addObject(new Bet(new int[] {11, 12, 14, 15}, 9), 897, 280);

        addObject(new Bet(new int[] {13, 14, 16, 17}, 9), 956, 352);
        addObject(new Bet(new int[] {14, 15, 17, 18}, 9), 956, 280);

        addObject(new Bet(new int[] {16, 17, 19, 20}, 9), 1015, 352);
        addObject(new Bet(new int[] {17, 18, 20, 21}, 9), 1015, 280);

        addObject(new Bet(new int[] {19, 20, 22, 23}, 9), 1074, 352);
        addObject(new Bet(new int[] {20, 21, 23, 24}, 9), 1074, 280);

        addObject(new Bet(new int[] {22, 23, 25, 26}, 9), 1133, 352);
        addObject(new Bet(new int[] {23, 24, 26, 27}, 9), 1133, 280);

        addObject(new Bet(new int[] {25, 26, 28, 29}, 9), 1192, 352);
        addObject(new Bet(new int[] {26, 27, 29, 30}, 9), 1192, 280);

        addObject(new Bet(new int[] {28, 29, 31, 32}, 9), 1251, 352);
        addObject(new Bet(new int[] {29, 30, 32, 33}, 9), 1251, 280);

        addObject(new Bet(new int[] {31, 32, 34, 35}, 9), 1310, 352);
        addObject(new Bet(new int[] {32, 33, 35, 36}, 9), 1310, 280);

        /////////////////////////////  Six's  ////////////////////////////////

        addObject(new Bet(new int[] {1, 2, 3, 4, 5, 6}, 6), 720, 424);
        addObject(new Bet(new int[] {4, 5, 6, 7, 8, 9}, 6), 779, 424);
        addObject(new Bet(new int[] {7, 8, 9, 10, 11, 12}, 6), 838, 424);
        addObject(new Bet(new int[] {10, 11, 12, 13, 14, 15}, 6), 897, 424);
        addObject(new Bet(new int[] {13, 14, 15, 16, 17, 18}, 6), 956, 424);
        addObject(new Bet(new int[] {16, 17, 18, 19, 20, 21}, 6), 1015, 424);
        addObject(new Bet(new int[] {19, 20, 21, 22, 23, 24}, 6), 1074, 424);
        addObject(new Bet(new int[] {22, 23, 24, 25, 26, 27}, 6), 1133, 424);
        addObject(new Bet(new int[] {25, 26, 27, 28, 29, 30}, 6), 1192, 424);
        addObject(new Bet(new int[] {28, 29, 30, 31, 32, 33}, 6), 1251, 424);
        addObject(new Bet(new int[] {31, 32, 33, 34, 35, 36}, 6), 1310, 424);

        ///////////////////////////  Streets  //////////////////////////////////

        addObject(new Bet(new int[] {1, 2, 3}, 12), 690, 424);
        addObject(new Bet(new int[] {4, 5, 6}, 12), 749, 424);
        addObject(new Bet(new int[] {7, 8, 9}, 12), 808, 424);
        addObject(new Bet(new int[] {10, 11, 12}, 12), 867, 424);
        addObject(new Bet(new int[] {13, 14, 15}, 12), 926, 424);
        addObject(new Bet(new int[] {16, 17, 18}, 12), 985, 424);
        addObject(new Bet(new int[] {19, 20, 21}, 12), 1044, 424);
        addObject(new Bet(new int[] {22, 23, 24}, 12), 1103, 424);
        addObject(new Bet(new int[] {25, 26, 27}, 12), 1162, 424);
        addObject(new Bet(new int[] {28, 29, 30}, 12), 1221, 424);
        addObject(new Bet(new int[] {31, 32, 33}, 12), 1280, 424);
        addObject(new Bet(new int[] {34, 35, 36}, 12), 1339, 424);
    }
}
