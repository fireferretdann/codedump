import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.Random;
import java.util.List;

/**
 * Write a description of class MyWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MyWorld extends World
{
    public Random rand = new Random();
    public int speciesCount;
    public Color[] speciesColors;
    public double[][] speciesForces;
    public static int centerX = 850;
    public static int centerY = 450;
    
    /*public MyWorld()
    {
        super(centerX*2, centerY*2, 1);
        speciesCount = 3;
        prepare();
    }*/
    
    public MyWorld(int numSpecies)
    {
        super(centerX*2, centerY*2, 1);
        speciesCount = numSpecies;
        prepare();
    }
    
    public void act()
    {
        List<Particle> particles = getObjects(Particle.class);
        
        for(Particle p : particles)
            p.feelForces(particles);
        for(Particle p : particles)
            p.move();
    }
    
    private void prepare()
    {
        speciesColors = new Color[speciesCount];
        speciesForces = new double[speciesCount][];
        for(int i = 0; i < speciesCount; i ++)
        {
            speciesColors[i] = new Color(255*((i)%2), 255*((i%4)/2), 255*((i%8)/4));
            
            double[] forcesForThisSpecies = new double[speciesCount];
            for(int f = 0; f < speciesCount; f ++)
            {
                forcesForThisSpecies[f] = rand.nextDouble()-.5;
            }
            speciesForces[i] = forcesForThisSpecies;
            
            int countForThisSpecies = rand.nextInt(7) + 3;
            for(int p = 0; p < countForThisSpecies; p ++)
            {
                Particle toAdd = new Particle(i, speciesForces[i], speciesColors[i]);
                addObject(toAdd, rand.nextInt(this.getWidth()), rand.nextInt(this.getHeight()));
            }
        }
        
        
    }
}
