import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;

/**
 * Write a description of class Particle here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Particle extends Actor
{
    private static final int size = 10;
    private static final double dt = 200;
    private static final double friction = .1;
    private static final double repulsion = 2;
    private static final double centerForce = -.000000001;
    private static final double repulsionDist = 2.6;
    
    public double x;
    public double y;
    private double xVel;
    private double yVel;
    
    private double forces[];
    public int species;
    public Particle(int speciesNumber, double myForces[], Color c)
    {
        species = speciesNumber;
        forces = myForces;
        
        doImage(c);
    }
    
    public void setLocation(int xPos, int yPos)
    {
        x = xPos;
        y = yPos;
        super.setLocation(xPos, yPos);
    }
    
    public void feelForces(List<Particle> particles)
    {
        for(Particle p : particles)
        {
            if(!p.equals(this))
            {
                // forces
                double dx = p.x - this.x;
                double dy = p.y - this.y;
                double dist = Math.hypot(dx, dy);
                double force = forces[p.species]/(dist*dist);
                double angle = Math.atan2(dx, dy) - Math.PI;
                xVel += force*Math.sin(angle);
                yVel += force*Math.cos(angle);
                
                // Repulsion
                dist /= repulsionDist;
                force = repulsion/(dist*dist*dist*dist);
                xVel += force*Math.sin(angle);
                yVel += force*Math.cos(angle);
            }
        }
        
        //pull to center
        MyWorld w = (MyWorld)getWorld();
        double dx = w.centerX - this.x;
        double dy = w.centerY - this.y;
        double dist = Math.hypot(dx, dy);
        double distToEdge = w.centerX - dist;
        double force = centerForce*Math.pow(dist, 2.1);
        double angle = Math.atan2(dx, dy) - Math.PI;
        xVel += force*Math.sin(angle);
        yVel += force*Math.cos(angle);
        
        // friction
        xVel -= xVel*friction;
        yVel -= yVel*friction;
    }
    
    public void move()
    {
        x += xVel*dt;
        y += yVel*dt;
        super.setLocation((int)Math.rint(x), (int)Math.rint(y));
    }
    
    private void doImage(Color c)
    {
        GreenfootImage image = new GreenfootImage(10, 10);
        image.setColor(c);
        image.fillOval(0,0,10,10);
        this.setImage(image);
    }
}
