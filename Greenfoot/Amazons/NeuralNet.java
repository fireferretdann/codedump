import java.util.Random;

public class NeuralNet  
{
    public static Random rand = new Random();
    
	double nodes[][];
	double weights[][];
	
	// In the current implementation, the input nodes are:
	// 0-35 white, 36-71 black, 72-107 burned, and 108 bias
    public NeuralNet(int boardSize)
    {
        double shape[][] = new double[4][];
        shape[0] = new double[boardSize*boardSize*3+1];
        shape[1] = new double[shape[0].length/3];
        shape[2] = new double[shape[1].length/3];
        shape[3] = new double[1];
        
        
		nodes = new double[shape.length][];
		for(int i = 0; i < nodes.length; i ++)
		{
			nodes[i] = new double[shape[i].length];
		}
		
		nodes[0][nodes[0].length-1] = rand.nextGaussian();
		
		weights = new double[nodes.length-1][];
		for(int i = 0; i < weights.length; i ++)
		{
			weights[i] = new double[nodes[i].length*nodes[i+1].length];
		}
		
		for(int i = 0; i < weights.length; i ++)
		{
			for(int j = 0; j < weights[i].length; j++)
			{
				weights[i][j] = rand.nextGaussian();
			}
		}
    }
    
    public double evaluate(Board b)
    {
        readPosition(b);
        
		for(int i = 0; i < weights.length; i ++)
		{
			for(int j = 0; j < weights[i].length; j ++)
			{
				nodes[i+1][j/nodes[i].length] += nodes[i][j%nodes[i].length] * weights[i][j];
			}
		}
		
		return nodes[nodes.length-1][0];
    }
    
    private void readPosition(Board b)
    {
        // Clears the input nodes (But not the bias)
        for(int i = 0; i < nodes[0].length-1; i ++)
        {
            nodes[0][i] = 0;
        }
        
        // Reads the board
        for(Amazon a : b.getObjects(Amazon.class))
        {
            if(a.player == 1)
            {
                nodes[0][a.getX() + b.boardSize*a.getY()] = 1;
            }
            else if(a.player == 2)
            {
                nodes[0][36 + a.getX() + b.boardSize*a.getY()] = 1;
            }
        }
        for(BurnedSquare bs : b.getObjects(BurnedSquare.class))
        {
                nodes[0][72 + bs.getX() + b.boardSize*bs.getY()] = 1;
        }
    }
}
