/**
 * Write a description of class Move here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Move  
{
    Amazon amazon;
    int x0, y0, x1, x2, y1, y2;
    
    public Move(int amazonX, int amazonY, int toMoveX, int toMoveY, int toShootX, int toShootY)
    {
        x0 = amazonX;
        y0 = amazonY;
        x1 = toMoveX;
        y1 = toMoveY;
        x2 = toShootX;
        y2 = toShootY;
    }
}
