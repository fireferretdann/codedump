import java.util.ArrayList;

public class AI  
{
    public NeuralNet net;
    public int player = 1;
    
    public AI(int size)
    {
        net = new NeuralNet(size);
    }
    
    public double valuePosition(Board b)
    {
        return net.evaluate(b);
    }
    
    public Move chooseBestMove(Board board)
    {
        board = board.copy();
        ArrayList<Move> legalMoves = board.getLegalMoves();
        
        if(!legalMoves.isEmpty())
        {
            Move bestMove = legalMoves.get(0);
            double bestMoveValue = Double.MIN_VALUE;
            
            for(Move m : legalMoves)
            {
                Board copy = board.copy();
                copy.doMove(m);
                double value = player * valuePosition(copy);
                if(value > bestMoveValue)
                {
                    bestMoveValue = value;
                    bestMove = m;
                }
            }
            
            return bestMove;
        }
        return null;
    }
    
    public void changePlayer()
    {
        player *= -1;
    }
}
