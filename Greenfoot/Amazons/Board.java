import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.ArrayList;

/**
 * Write a description of class MyWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Board extends World
{
    public int whoseTurn = 1;
    public boolean firing;
    public Amazon selected;
    public int boardSize;
    public AI bot = new AI(6);
    
    public void act()
    {
        MouseInfo mouse = Greenfoot.getMouseInfo();
        if(mouse != null && mouse.getButton() == 1)
        {
            Actor actor = mouse.getActor();
            if(actor != null && actor instanceof Amazon && !firing)
            {
                selected = (Amazon)actor;
                if(selected.player != whoseTurn)
                    selected = null;
            }
            else if(actor == null && selected != null)
            {
                if(firing)
                {
                    burn(selected, mouse.getX(), mouse.getY());
                    Move m = bot.chooseBestMove(this);
                    System.out.println("(" + m.x0 + ", " + m.y0 + ")");
                    System.out.println("(" + m.x1 + ", " + m.y1 + ")");
                    System.out.println("(" + m.x2 + ", " + m.y2 + ")");
                    //System.out.println(bot.chooseBestMove(this));
                    //doMove(bot.chooseBestMove(this));
                }
                else
                {
                    moveAmazon(selected, mouse.getX(), mouse.getY());
                }
            }
        }
    }
    
    public ArrayList<Move> getLegalMoves()
    {
        ArrayList<Move> moves = new ArrayList<Move>();
        for(Amazon a : this.getObjects(Amazon.class))
        {
            if(a.player == whoseTurn)
            {
                for(int x1 = 0; x1 < this.getWidth(); x1 ++)
                {
                    for(int y1 = 0; y1 < this.getHeight(); y1 ++)
                    {
                        if(hasValidPath(a.getX(), a.getY(), x1, y1))
                        {
                            for(int x2 = 0; x2 < this.getWidth(); x2 ++)
                            {
                                for(int y2 = 0; y2 < this.getHeight(); y2 ++)
                                {
                                    if(hasValidPath(x1, y1, x2, y2))
                                    {
                                        moves.add(new Move(a.getX(), a.getY(), x1, y1, x2, y2));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return moves;
    }
    
    public boolean burn(Amazon toShoot, int x, int y)
    {
        //Check if legal
        if(!hasValidPath(toShoot.getX(), toShoot.getY(), x, y))
            return false;
        addObject(new BurnedSquare(), x, y);
        firing = false;
        selected = null;
        whoseTurn = whoseTurn%2 + 1;
        
        //System.out.println(bot.valuePosition(this));
        
        return true;
    }
    
    public boolean moveAmazon(Amazon toMove, int x, int y)
    {
        //Check if legal
        if(!hasValidPath(toMove.getX(), toMove.getY(), x, y))
            return false;
        toMove.setLocation(x, y);
        firing = true;
        
        return true;
    }
    
    public boolean doMove(Move m)
    {
        Amazon amazon = this.getObjectsAt(m.x0, m.y0, Amazon.class).get(0);
        return moveAmazon(amazon, m.x1, m.y1)  &&  burn(amazon, m.x2, m.y2);
    }
    
    public boolean hasValidPath(int x1, int y1, int x2, int y2)
    {
        if(x1 == x2 && y1 == y2)
            return false; // The two positions are the same, and for our purposes, that is bad
        
        if(x1 == x2)
        { // Horizontal
            int sign = (int)Math.signum(y2-y1);
            for(int i = 1; i < Math.abs(y2-y1); i ++)
            {
                if(!getObjectsAt(x1, y1+sign*i, Actor.class).isEmpty())
                    return false;
            }
        }
        else if(y1 == y2)
        { // Vertical
            int sign = (int)Math.signum(x2-x1);
            for(int i = 1; i < Math.abs(x2-x1); i ++)
            {
                if(!getObjectsAt(x1+sign*i, y1, Actor.class).isEmpty())
                    return false;
            }
        }
        else if(Math.abs(y1-y2) == Math.abs(x1-x2))
        { // down/right diagonal
            int signX = (int)Math.signum(x2-x1);
            int signY = (int)Math.signum(y2-y1);
            for(int i = 1; i < Math.abs(x2-x1); i ++)
            {
                for(int j = 1; j < Math.abs(y2-y1); j ++)
                {
                    if(!getObjectsAt(x1+signX*i, y1+signY*j, Actor.class).isEmpty())
                        return false;
                }
            }
        }
        else
        {
            return false;
        }
        return true;
    }
    
    public Board(int size)
    {
        super(size, size, 60);
        prep(size);
    }
    
    public Board()
    {
        super(6, 6, 60);
        prep(6);
    }
    
    public void prep(int size)
    {
        boardSize = size;
        if(boardSize == 6)
        {
            addObject(new Amazon(1), 6, 3);
            addObject(new Amazon(1), 0, 2);
            addObject(new Amazon(2), 2, 6);
            addObject(new Amazon(2), 3, 0);
        }
    }
    
    public Board copy()
    {
        Board copy = new Board(boardSize);
        copy.removeObjects(copy.getObjects(null)); // Removes all objects
        for(Actor a : this.getObjects(Actor.class))
        { // Repopulates the copy with copies of all the Actors
            if(a instanceof Amazon)
            {
                copy.addObject(new Amazon(((Amazon)a).player), a.getX(), a.getY());
            }
            else if(a instanceof BurnedSquare)
            {
                copy.addObject(new BurnedSquare(), a.getX(), a.getY());
            }
        }
        return copy;
    }
}
