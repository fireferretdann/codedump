import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Amazon here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Amazon extends Actor
{
    int player;
    
    public Amazon(int owningPlayer)
    {
        player = owningPlayer;
        if(player == 1)
            this.setImage(new GreenfootImage("WhiteQueen.png"));
        else
            this.setImage(new GreenfootImage("BlackQueen.png"));
    }
}
