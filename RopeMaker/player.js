let player = 
{
	activity: "",
	
	collectionRate: 1,
	collectOn: 100,
	lastCollect: 0,
	
	spinRate: 1,
	spinOn: 2000,
	lastSpin: 0,
	
	twistRate: 1,
	twistOn: 2000,
	lastTwist: 0,



	collect(now) 
	{
		if(now - this.lastCollect >= this.collectOn  &&  this.canCollect())
		{
			this.lastCollect = now;
			collectFibers(this.collectionRate);
		}
	},

	spin(now)
	{
		if(now - this.lastSpin >= this.spinOn  &&  this.canSpin())
		{
			this.lastSpin = now;
			spinFibers(this.spinRate);
		}
	},

	twist(now)
	{
		if(now - this.lastTwist >= this.twistOn  &&  this.canTwist())
		{
			this.lastTwist = now;
			twistYarns(this.twistRate);
			console.log("twisted");
		}
	},
	
	canCollect()
	{
		return true;
	},
	
	canSpin()
	{
		return fibers >= fibersToYarn;
	},
	
	canTwist()
	{
		return yarns >= yarnsToStrand;
	}
}