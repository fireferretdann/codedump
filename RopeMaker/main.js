
////////////// CLEAN THIS SHIT UP BEFORE ITS WORSE  ///////////////////

var loaded = false;
var fibers = 0;
var yarns = 0;
var strands = 0;
var interval = 10;
var lastTime = Date.now();
var collectors = 0;

var fibersToYarn = 50;
var yarnsToStrand = 20;

function select(buttonID)
{
	var actionButtons = document.getElementsByClassName("action");
	console.log(actionButtons);
	for(var i = 0; i < actionButtons.length; i++)
	{
		console.log(typeof actionButtons[i]);
		actionButtons[i].setAttribute("style", "color: black");
	};
	document.getElementById(buttonID).setAttribute("style", "color: red");
};

function fiberClick()
{
	player.activity = "fibers";
	player.lastCollect = Date.now();
	select("collect");
};

function yarnClick()
{
	player.activity = "yarns";
	player.lastSpin = Date.now();
	select("spin");
};

function strandClick()
{
	player.activity = "strands";
	player.lastTwist = Date.now();
	select("twist");
};

function collectFibers(number)
{
    fibers = fibers + number;
    document.getElementById("fibers").innerHTML = Math.floor(fibers);
};

function spinFibers(number)
{
	if(fibers >= fibersToYarn*number)
	{
		yarns += number;
		fibers -= fibersToYarn;
        document.getElementById('yarns').innerHTML = yarns;  //updates the number of yarns for the user
        document.getElementById('fibers').innerHTML = Math.floor(fibers);  //updates the number of fibers for the user
	}
};

function twistYarns(number)
{
	if(yarns >= yarnsToStrand*number)
	{
		strands += number;
		yarns -= yarnsToStrand;
        document.getElementById('strands').innerHTML = strands;  //updates the number of yarns for the user
        document.getElementById('yarns').innerHTML = yarns;  //updates the number of fibers for the user
	}
};

function buyCollector()
{
	var collectorCost = getCollectorCost();
    if(yarns >= collectorCost)
	{                                   //checks that the player can afford the collector
        collectors ++;                                   //increases number of collectors
    	yarns -= collectorCost;                          //removes the fibers spent
        document.getElementById('collectors').innerHTML = collectors;  //updates the number of collectors for the user
		document.getElementById('yarns').innerHTML = Math.floor(yarns);  //updates the number of fibers for the user
		document.getElementById("buyCollectors").innerText = "Buy Collector: " + getCollectorCost() + 	" Yarns";
    };
};

function getCollectorCost()
{
	return Math.floor(1 * Math.pow(1.1,collectors));
};

function firstLoad()
{
	document.getElementById("buyCollectors").innerText = "Buy Collector: " + getCollectorCost() + 	" Yarns";
}

window.setInterval(function()
{
	if(!loaded)
	{
		firstLoad();
		loaded = true;
	}
	var now = Date.now();
	var secondsSinceLastUpdate = (now - lastTime)/1000;
	
	collectFibers(collectors*secondsSinceLastUpdate);
	if(player.activity == "fibers")
		player.collect(now);
	if(player.activity == "yarns")
		player.spin(now);
	if(player.activity == "strands")
		player.twist(now);
	
	/*console.clear();
	console.log("secondsSinceLastUpdate: " + secondsSinceLastUpdate);
	console.log("fibers: " + fibers);
	console.log("yarns: " + yarns);
	console.log("collectors: " + collectors);
	console.log(document.getElementsByClassName("selected"));
	console.log();
	/**/
	
	
	lastTime = now;
	
}, interval);