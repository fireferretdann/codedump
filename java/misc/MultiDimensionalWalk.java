import java.util.Scanner;

public class MultiDimensionalWalk
{
	static Scanner input = new Scanner(System.in);
	int[] location;
	long stepCount;
	
	
	public MultiDimensionalWalk(int numDimensions)
	{
		location = new int [numDimensions];
		stepCount = 0;
	}
	
	
	public static void main(String[] args)
	{
		MultiDimensionalWalk walk = new MultiDimensionalWalk(2);
		
		do
		{
			walk.step();
			if(walk.stepCount%Integer.MAX_VALUE == 0)
			{
				walk.printInfo();
				//input.nextLine();
				System.out.println();
			}
		} while(!walk.isAtOrigin()  &&  walk.stepCount < Long.MAX_VALUE);
		
		walk.printInfo();
	}
	
	
	public boolean isAtOrigin()
	{
		for(int distanceInDimension : location)
			if(distanceInDimension != 0)
				return false;
		
		return true;
	}
	
	
	public void step()
	{
		int dimension = (int) (location.length * Math.random());
		
		if(Math.random() < .5)
			location[dimension]++;
		else
			location[dimension]--;
		
		stepCount ++;
	}
	
	public void printInfo()
	{
		System.out.print("[" + location[0]);
		for(int i = 1; i < location.length; i++)
			System.out.print(", " + location[i]);
		System.out.println("]");
		
		
		int stepDist = 0;
		double trueDist = 0;
		
		for(int s : location)
		{
			stepDist += Math.abs(s);
			trueDist += ((double)s)*((double)s);
		}
		trueDist = Math.sqrt(trueDist);
		
		System.out.println("Stepwise Distance:\t" + stepDist);
		System.out.println("True Distance:\t\t" + trueDist);
		
		System.out.println("Steps taken:\t\t" + stepCount);
	}
}
