import java.util.Scanner;
import java.util.HashSet;

public class ParenProblem
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		System.out.println("How many pairs of parens?");
		int n = input.nextInt();
		
		while(n > 0)
		{
			HashSet<String> set = parens(n);
			print(set);
			print("there are " + set.size() + " possible combinations\n\n");
			
			System.out.println("How many pairs of parens?");
			n = input.nextInt();
		}
	}

	public static void print(HashSet<String> set)
	{
		String s = "[";
		for(String e : set)
			s += e + ", ";
		s = s.substring(0, s.length() - 2);
		s += "]\n";
		print(s);
	}
	public static void print(String s)
	{
		System.out.print(s);
	}

	public static HashSet<String> parens(int n)
	{
		HashSet<String> set = new HashSet<String>();
		set.add("");
		return parens(n, set);
	}
	
	public static HashSet<String> parens(int n, HashSet<String> set)
	{
		for(int i = 0; i < n; i++)
		{
			HashSet<String> newSet = new HashSet<String>();
			for(String s : set)
			{
				newSet.add("()" + s);
				newSet.add("(" + s + ")");
				newSet.add(s + "()");
			}
			set = (HashSet<String>)newSet.clone();
		}
		return set;
	}

}
