import java.util.HashMap;

public class Graph
{
	HashMap<Integer, Node> nodes;
	
	public Graph()
	{
		nodes = new HashMap<Integer, Node>();
	}
	
	public void connect(int first, int second)
	{
		Node firstNode = nodes.get(first);
		Node secondNode = nodes.get(second);
		
		connect(firstNode, secondNode);
	}
	
	public void connect(Node nodeA, Node nodeB)
	{
		nodeA.connect(nodeB);
		nodeB.connect(nodeA);
	}
	
	public void add(int toAdd)
	{
		nodes.put(toAdd, new Node(toAdd));
	}
}
