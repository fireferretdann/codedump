import java.util.ArrayList;

public class Node
{
	final int value;
	ArrayList<Node> connections;
	
	public Node(int id)
	{
		value = id;
		connections = new ArrayList<Node>();
	}
	
	public void connect(Node other)
	{
		if(!connections.contains(other))
			connections.add(other);
	}
}
