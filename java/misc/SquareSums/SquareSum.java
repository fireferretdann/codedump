import java.util.HashSet;
import java.util.ArrayList;

public class SquareSum extends Graph
{
	//HashSet<Node> visited = new HashSet<Node>();
	final static int SMALLEST_GRAPH = 1;
	boolean currentGraphHasPath = false;
	
	public SquareSum()
	{
		super();
	}
	
	public SquareSum(int maxNumber)
	{
		super();
		
		for(int i = 1; i <= maxNumber; i ++)
			addAndConnect(i);
	}
	
	public static void main(String[] args)
	{
		SquareSum graph = new SquareSum();
		boolean graphsHavePaths[] = new boolean[501];
		
		long start;
		long end;
		start = System.currentTimeMillis();
		
		for(int i = 1; i < graphsHavePaths.length; i ++)
		{
			graph.addAndConnect(i);
			
			if(i >= SMALLEST_GRAPH)
			{
				graphsHavePaths[i] = graph.hasHamiltonianPath();
				System.out.println(graph.nodes.size() + ": \t" + graphsHavePaths[i]);
			}
		}
		
		for(int i = 0; i < graphsHavePaths.length; i ++)
		{
			if(graphsHavePaths[i] == false)
				System.out.println("Graph " + i + " has no path.");
		}
		end = System.currentTimeMillis();
		System.out.println(end-start);
	}
	
	public boolean hasHamiltonianPath()
	{
		currentGraphHasPath = false;
		
		if(nodes.size() == 0)
			return currentGraphHasPath;
		
		HashSet<Node> visited = new HashSet<Node>();
		
		//for(Node n : nodes.values())
		for(int i = nodes.size(); i > 0; i --)
		{
			Node n = nodes.get(i);
			
			if(hasHamiltonianPath(n, (HashSet<Node>)visited.clone()))
			{
				currentGraphHasPath = true;
				return currentGraphHasPath;
			}
		}
		
		return currentGraphHasPath;
	}
	
	public boolean hasHamiltonianPath(Node currentNode, HashSet<Node> visited)
	{
		visited.add(currentNode);
		
		if(visited.size() == nodes.size())
			return true;
		
		for(Node n : currentNode.connections)
		{
			if(!visited.contains(n))
			{
				if(hasHamiltonianPath(n, (HashSet<Node>)visited.clone()))
					return true;
			}
		}
		
		//visited.remove(currentNode);
		
		return false;
	}
	
	public void addAndConnect(int toAdd)
	{
		Node newNode = new Node(toAdd);
		
		for(Node n : nodes.values())
		{
			if(isSquare(n.value + newNode.value))
				connect(n, newNode);
		}
		
		nodes.put(toAdd, newNode);
	}
	
	public static boolean isSquare(int n)
	{
		double sqrt = Math.sqrt(n);
		return sqrt == (int)sqrt;
	}
}
