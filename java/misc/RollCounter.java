import java.text.DecimalFormat;

public class RollCounter
{
	static int[] resultCount = new int[13];
	
	
	public static void main(String[] args)
	{
		int[][] allResults = new int[13][13];
		
		for(int numDice = 0; numDice < 13; numDice ++)
		{
			//System.out.println("Working on " + numDice + " dice...");
			
			doAllRolls(numDice);
			
			System.arraycopy(resultCount, 0, allResults[numDice], 0, 13);
			resultCount = new int[13];
		}
		
		//DecimalFormat form = new DecimalFormat("0.#####");
		
		//System.out.println("Before\t\t\t\tAfter");
		for(int numDice = 0; numDice < 13; numDice ++)
		{
			double totalWays = (double) pow(6, numDice);
			//System.out.print("\n" + numDice);
			for(int count : allResults[numDice])
				System.out.print("\t" + /*form.format*/((double)count/totalWays));
			System.out.println();
		}
	}
	
	
	public static void doAllRolls(int numDice)
	{
		if(numDice == 0)
		{
			resultCount[0] ++;
		}
		else
		{
			int[] rolls = new int[numDice];
			oneDie(rolls);
		}
	}
	
	
	public static void oneDie(int[] previousRolls)
	{
		int dieIndex = 0;
		while(previousRolls[dieIndex] != 0)
			dieIndex ++;
		
		for(int i = 1; i <= 6; i ++)
		{
			previousRolls[dieIndex] = i;
			
			if(dieIndex == previousRolls.length-1)
			{
				count(previousRolls);
			}
			else
			{
				oneDie(previousRolls);
			}
			previousRolls[dieIndex] = 0;
		}
	}
	
	public static void count(int[] rolls)
	{
		int newDice = 0;
		
		for(int i = 1; i <= 6; i++)
		{
			int countForSide = 0;
			for(int roll : rolls)
				if(roll == i)
					countForSide ++;
			
			if(countForSide == 1)
				newDice += 2;
			else if(countForSide == 2)
				newDice += 1;
		}
		
		resultCount[newDice] ++;
	}
	
	
	public static long pow(long base, long exp)
	{
		long result = 1;
		while(exp > 0)
		{
			if(exp%2 == 1)
				result *= base;
			
			base *= base;
			exp /= 2;
		}
		return result;
	}
}
