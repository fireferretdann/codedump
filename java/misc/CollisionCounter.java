public class CollisionCounter
{
	public static final double digits = 10;
	public static void main(String[] args)
	{
		long collisions = 0;
		
		double v1 = 0;
		double m1 = 1;
		
		double v2 = -1;
		double m2 = Math.pow(100, digits-1);
		
		while(v1 > v2)
		{
			double v1New = ((m1-m2)*v1 + 2*m2*v2)/(m1 + m2);
			double v2New = ((m2-m1)*v2 + 2*m1*v1)/(m2 + m1);
			v1 = v1New;
			v2 = v2New;
			collisions++;
			
			if(v1 < 0)
			{
				v1 = -1*v1;
				collisions++;
			}
		}
		
		System.out.println(collisions);
	}
}