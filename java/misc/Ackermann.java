// Note to self: run with "java -Xss100m Ackermann"
// to increase stack size



import java.util.TreeSet;

public class Ackermann implements Comparable
{
	public static TreeSet<Ackermann> visited = new TreeSet<Ackermann>();
	
	
	public int m, n, value;
	
	public Ackermann(int first, int second)
	{
		m = first;
		n = second;
		value = ack(m, n);
	}
	
	public Ackermann(int first, int second, int ans)
	{
		m = first;
		n = second;
		value = ans;
	}
	
	public static void main(String[] args)
	{
		java.util.Scanner input = new java.util.Scanner(System.in);
		
		String toDo = "again";
		
		while(!toDo.equals("q"))
		{
			if(toDo.equals("again"))
			{
				System.out.print("m: ");
				int m = input.nextInt();
				System.out.print("n: ");
				int n = input.nextInt();
				
				Ackermann max = new Ackermann(m, n);
				visited.add(max);
				System.out.println(max);
			}
			
			if(toDo.equals("list"))
			{
				for(Ackermann ack : visited)
				{
					System.out.println("ack("+ack.m+", "+ack.n+") = "+ack.value);
				}
			}
			
			System.out.println("What to do next?");
			toDo = input.nextLine();
		}
	}
	
	public static int ack(int m, int n)
	{
		int value;
		if(hasVisited(m, n))
		{
			//System.out.println("Trying to find ("+m+", "+n+")");
			value 			= getFromVisited(m, n);
		}
		else
		{
			if(m == 0) value 	= n + 1;
			else if (n == 0) value	= ack(m-1, 1);
			else value 		= ack(m-1, ack(m, n-1));
			
			visited.add(new Ackermann(m, n, value));
			//System.out.println("Added ack("+m+", "+n+") = "+value);
		}
		
		return value;
	}
	
	public static int getFromVisited(int m, int n)
	{
		int value = -1;
		for(Ackermann ack : visited)
			if(ack.m == m  &&  ack.n == n) return ack.value;
		
		System.err.println("Expected node ("+m+", "+n+") not found");
		
		return -1;
	}
	
	public static boolean hasVisited(int m, int n)
	{
		boolean found = false;
		for(Ackermann ack : visited)
			found = found  ||  (ack.m == m  &&  ack.n == n);
		
		return found;
	}
	
	public boolean equals(Ackermann ack)
	{
		return (this.m == ack.m  &&  this.n == ack.n);
	}
	
	public String toString()
	{
		return "" + value;
	}
	
	public int compareTo(Object o)
	{
		Ackermann that = (Ackermann) o;
		if(this.m < that.m) return -1;
		else if(this.m > that.m) return 1;
		else if(this.n < that.n) return -1;
		else if(this.n > that.n) return 1;
		return 0;
	}
}
