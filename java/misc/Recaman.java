import java.util.HashSet;
import java.util.ArrayList;
import java.math.BigInteger;

public class Recaman
{
	HashSet<BigInteger> numsIn;
	//ArrayList<BigInteger> sequence;
	BigInteger lastNumber;
	BigInteger length;
	BigInteger smallestExcluded;
	BigInteger nextPrint;
	
	public Recaman()
	{
		smallestExcluded = BigInteger.ONE;
		numsIn = new HashSet<BigInteger>();
		//sequence = new ArrayList<BigInteger>();
		lastNumber = BigInteger.ZERO;
		length = BigInteger.ONE;
		nextPrint = BigInteger.TEN;
	}
	
	public static void main(String[] args)
	{
		Recaman r = new Recaman();
		while(true)
		{
			r.findNext();
		}
	}
	
	public void findNext()
	{
		length = length.add(BigInteger.ONE);
		
		BigInteger diff = lastNumber.subtract(length);
		if(diff.compareTo(BigInteger.ZERO) < 0 || numsIn.contains(diff))
			add(lastNumber.add(length));
		else
			add(diff);
		shouldPrintLength();
	}
	
	private void add(BigInteger n)
	{
		lastNumber = n;
		numsIn.add(n);
		if(smallestExcluded.equals(n))
			findNewSmallest();
	}
	
	private void findNewSmallest()
	{
		while(true)
		{
			if(numsIn.contains(smallestExcluded))
				smallestExcluded = smallestExcluded.add(BigInteger.ONE);
			else
			{
				System.out.println(smallestExcluded);
				return;
			}
		}
	}
	
	private void shouldPrintLength()
	{
		if(length.equals(nextPrint))
		{
			System.out.println("n = " + length + "\tR(n) = " + lastNumber);
			nextPrint = nextPrint.multiply(BigInteger.TEN);
		}
	}
}
