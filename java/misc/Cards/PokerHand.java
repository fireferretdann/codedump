import java.util.LinkedList;

public class PokerHand extends Hand implements Comparable<PokerHand>
{
	int[][] bestFive;
	
	public static void main(String[] args)
	{
		Deck d = new Deck();
		PokerHand[] phs = new PokerHand[125];
		for(int i = 0; i < phs.length; i ++)
			phs[i] = new PokerHand(new Hand(5));
		
		java.util.Arrays.sort(phs);
		//for(int i = phs.length-1; i >= 0; i--)
		for(int i = 0; i < phs.length; i++)
			System.out.println(phs[i]);
	}
	
	public PokerHand(LinkedList<Card> cs)
	{
		super(cs);
		if(cs.size() >= 5)
			bestFive = calculateBestFive(cs);
		else
		{
			System.err.println("too few cards for a poker hand");
			System.exit(9);
		}
	}
	
	public PokerHand(Hand h)
	{
		super(h);
		LinkedList<Card> cs = h.cards;
		
		if(cs.size() >= 5)
			bestFive = calculateBestFive(cs);
		else
		{
			System.err.println("too few cards for a poker hand");
			System.exit(9);
		}
	}
	
	public int compareTo(PokerHand that)
	{
		//Better hand compared to worse hand -> 1
		
		if(this.equals(that))
			return 0;
		
		return compare(this.bestFive, that.bestFive);
	}
	
	public int[][] calculateBestFive(LinkedList<Card> cs)
	{
		int[][] res = new int[5][2];
		
		if(cs.size() == 5)
		{
			int count = 0;
			for(Card c : cs)
				res[count++] = c.rsPair;
		}
		else
		{
			for(int i = 0; i < 5; i ++)
				res[i] = cs.get(i).rsPair;
			
			for(int i = 0; i < cs.size(); i++)
			{
				LinkedList<Card> temp = (LinkedList<Card>) cs.clone();
				temp.remove(i);
				int[][] tempFive = calculateBestFive(temp);
				if(compare(tempFive, res) > 0)
					res = tempFive;
			}
		}
		return res;
	}
	
	public static int compare(int[][] h1, int[][] h2)
	{
		h1 = sort(h1);
		h2 = sort(h2);
		
		//check for straight flush
		if(hasFlush(h1)&&hasStraight(h1) && !(hasFlush(h2)&&hasStraight(h2)))
			return 1;
		else if(hasFlush(h2)&&hasStraight(h2) && !(hasFlush(h1)&&hasStraight(h1)))
			return -1;
		else if(hasFlush(h2)&&hasStraight(h2) && (hasFlush(h1)&&hasStraight(h1)))
		{
			if(h1[2][0] > h2[2][0])
				return 1;
			else if(h1[2][0] > h2[2][0])
				return -1;
		}
		
		//make histograms
		int[] hist1 = new int[13];
		for(int i[] : h1)
			hist1[i[0]]++;
		hist1 = sort(hist1);
		
		int[] hist2 = new int[13];
		for(int i[] : h2)
			hist2[i[0]]++;
		hist2 = sort(hist2);
		
		//check for 4 of a kind
		if(hist1[0] == 4 && hist2[0] != 4)
			return 1;
		else if(hist2[0] == 4 && hist1[0] != 4)
			return -1;
		else if(hist1[0] == 4 && hist2[0] == 4)
		{
			if(h1[2][0]  > h2[2][0] )
				return 1;
			else if(h2[2][0]  > h1[2][0] )
				return -1;
			else if(h1[0][0]  + h1[4][0]  > h2[0][0]  + h2[4][0] )
				return 1;
			else if(h2[0][0]  + h2[4][0]  > h1[0][0]  + h1[4][0] )
				return -1;
			return 0;
		}
		
		//check for boring sitcom
		if((hist1[0]==3 && hist1[1]==2) && !(hist2[0]==3 && hist2[1]==2))
			return 1;
		else if(!(hist1[0]==3 && hist1[1]==2) && (hist2[0]==3 && hist2[1]==2))
			return -1;
		else if((hist1[0]==3 && hist1[1]==2) && (hist2[0]==3 && hist2[1]==2))
		{
			if(h1[2][0]  > h2[2][0] )
				return 1;
			else if(h2[2][0]  > h1[2][0] )
				return -1;
			else if(h1[0][0]  + h1[4][0]  > h2[0][0]  + h2[4][0] )
				return 1;
			else if(h2[0][0]  + h2[4][0]  > h1[0][0]  + h1[4][0] )
				return -1;
			return 0;
		}
		
		//check for flush
		if(hasFlush(h1) && !hasFlush(h2))
			return 1;
		else if(hasFlush(h2) && !hasFlush(h1))
			return -1;
		
		//check for straight
		if(hasStraight(h1) && !hasStraight(h2))
			return 1;
		else if(hasStraight(h2) && !hasStraight(h1))
			return -1;
		
		//check for 3 of a kind
		if(hist1[0] == 3 && hist2[0] != 3)
			return 1;
		else if(hist2[0] == 3 && hist1[0] != 3)
			return -1;
		else if(hist1[0] == 3 && hist2[0] == 3)
		{
			if(h1[2][0] > h2[2][0])
				return 1;
			else if(h2[2][0] > h1[2][0])
				return -1;
		}
		
		//check for 2 pair
		if((hist1[0]==2 && hist1[1]==2) && !(hist2[0]==2 && hist2[1]==2))
			return 1;
		else if(!(hist1[0]==2 && hist1[1]==2) && (hist2[0]==2 && hist2[1]==2))
			return -1;
		else if((hist1[0]==2 && hist1[1]==2) && (hist2[0]==2 && hist2[1]==2))
		{
			if(h1[1][0] > h2[1][0])
				return 1;
			else if(h2[1][0] > h1[1][0])
				return -1;
			if(h1[3][0] > h2[3][0])
				return 1;
			else if(h2[3][0] > h1[3][0])
				return -1;
		}
		
		//check for 2 of a kind
		if(hist1[0] == 2 && hist2[0] != 2)
			return 1;
		else if(hist2[0] == 2 && hist1[0] != 2)
			return -1;
		else if(hist1[0] == 2 && hist2[0] == 2)
		{
			int h1pair = -1;
			int h2pair = -1;
			for(int i = 0; i < 4; i++)
			{
				if(h1[i][0] == h1[i+1][0])
					h1pair = h1[i][0];
				if(h2[i][0] == h2[i+1][0])
					h2pair = h2[i][0];
			}
			if(h1pair > h2pair)
				return 1;
			else if(h2pair > h1pair)
				return -1;
		}
		
		//check for high card (and high card in straights and flushes)
		for(int i = 0; i < 5; i ++)
		{
			if(h1[i][0] > h2[i][0])
				return 1;
			else if(h2[i][0] > h1[i][0])
				return -1;
		}
		
		return 0;
	}
	
	private static boolean hasStraight(int[][] h)
	{
		if(h[0][0] - h[4][0] == 4)
			return true;
		if(h[0][0] == 12 && h[1][0] == 3 && h[2][0] == 2 && h[3][0] == 1 && h[4][0] == 0)	//Checks for ace-low possible straight
			return true;
		return false;
	}
	
	private static boolean hasFlush(int[][] h)
	{
		int suit = h[0][1];
		for(int i = 1; i < h.length; i++)
			if(suit != h[i][1])
				return false;
		return true;
	}
	
	private static int[][] sort(int[][] h)
	{
		//bubblesort since its only five cards
		for(int i = 0; i < 4; i ++)
		{
			for(int j = i; j < 5; j ++)
			{
				if(h[i][0] < h[j][0])
				{
					int[] temp = h[i];
					h[i] = h[j];
					h[j] = temp;
				}
			}
		}
		//for(int[] i : h) System.out.println(i[0]);
		return h;
	}
	
	private static int[] sort(int[] a)
	{
		//bubblesort since its only a histogram of 13
		for(int i = 0; i < 12; i ++)
		{
			for(int j = i; j < 13; j ++)
			{
				if(a[i] < a[j])
				{
					int temp = a[i];
					a[i] = a[j];
					a[j] = temp;
				}
			}
		}
		return a;
	}
}
