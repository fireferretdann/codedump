public class GoFishProblem
{
	public static long TIME_TRIALS = 12000;
	
	public static void main(String[] args)
	{
		if(args.length > 0)
			TIME_TRIALS = Long.parseLong(args[0]);
		
		double[] averages = new double[53];
		for(int numCards = 2; numCards <= 52; numCards ++)
		{
			int[] pairCounts = new int[numCards/2 + 2];
			
			long start = System.currentTimeMillis();
			while(System.currentTimeMillis()-start < TIME_TRIALS)
			{
				Hand hand = new Hand(numCards);
				pairCounts[countPairs(hand)] ++;
			}
			
			int total = 0;
			for(int i = 0; i < pairCounts.length; i++)
			{
				//System.out.println("Number of times \t" + i + " pairs found:\t" + pairCounts[i]);
				total += pairCounts[i];
			}
			
			//System.out.println("------------------------------------------------------------");
			
			for(int i = 0; i < pairCounts.length; i++)
			{
				//System.out.println("Percent of times \t" + i + " pairs found:\t" + ((double)pairCounts[i]/(double)total));
			}
			
			double average = 0;
			for(int i = 0; i < pairCounts.length; i++)
			{
				average += i*((double)pairCounts[i]/(double)total);
			}
			//System.out.println("Average for " + numCards + " cards: \t" + average);
			averages[numCards] = average;
		}
		
		for(int i = 0; i < averages.length; i++)
		{
			System.out.println("Average for " + i + " cards: \t" + averages[i]);
		}
	}
	
	public static int countPairs(Hand h)
	{
		int count = 0;
		
		for(int i = 0; i < h.cards.size()-1; i ++)
		{
			for(int j = i+1; j < h.cards.size(); j ++)
			{
				Card c1 = h.cards.get(i); 
				Card c2 = h.cards.get(j);
				if(c1.rankEquals(c2))
				{
					h.cards.remove(j);
					h.cards.remove(i);
					count ++;
					i -= 1;
					j -= 2;
					break;
				}
			}
		}
		
		return count;
	}
}