import java.util.Random;

public class Card implements Comparable<Card>
{
	public static void main(String[] args)
	{
		Card c1 = new Card("2h");
		Card c2 = new Card("10h");
		System.out.println(c1);
	}
	
	public final Rank rank;
	public final Suit suit;
	public final int[] rsPair;
	
	public Card(Rank r, Suit s)
	{
		rank = r;
		suit = s;
		rsPair = makePair(rank, suit);
	}
	
	public Card(String rs)
	{
		rs = rs.toLowerCase();
		String r = rs.substring(0, rs.length()-1);
		String s = rs.substring(rs.length()-1, rs.length());
		
		rank = Rank.stringToRank(r);
		suit = Suit.stringToSuit(s);
		rsPair = makePair(rank, suit);
	}
	
	private static int[] makePair(Rank r, Suit s)
	{
		int[] rs = new int[2];
		rs[0] = Rank.rankToNum(r);
		rs[1] = Suit.suitToNum(s);
		return rs;
	}
	
	public String toString()
	{
		return rank.toString() + suit.toString();
	}
	
	public boolean suitEquals(Card that)
	{
		return this.suit == that.suit;
	}
	
	public boolean rankEquals(Card that)
	{
		return this.rank == that.rank;
	}
	
	public boolean equals(Object o)
	{
		if(o != null && o instanceof Card)
		{
			return this.toString().equals(o.toString());
		}
		return false;
	}
	
	public int compareTo(Card that)
	{
		if(this.rsPair[0] < that.rsPair[0])
			return 1;
		if(this.rsPair[0] > that.rsPair[0])
			return -1;
		return 0;
	}
}
