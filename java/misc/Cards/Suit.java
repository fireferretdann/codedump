public enum Suit
{
	CLUBS,
	DIAMONDS,
	HEARTS,
	SPADES;
	
	public static Suit stringToSuit(String s)
	{
		switch(s)
		{
			case("c"):
				return CLUBS;
			case("d"):
				return DIAMONDS;
			case("h"):
				return HEARTS;
			case("s"):
				return SPADES;
			default:
				System.err.println("bad suit string: " + s);
				System.exit(123);
		}
		return CLUBS; //Should not be reached
	}
	
	public static Suit numToSuit(int s)
	{
		switch(s)
		{
			case(0):
				return CLUBS;
			case(1):
				return DIAMONDS;
			case(2):
				return HEARTS;
			case(3):
				return SPADES;
			default:
				System.err.println("bad suit num: " + s);
				System.exit(123);
		}
		return CLUBS; //Should not be reached
	}
	
	public static int suitToNum(Suit s)
	{
		switch(s)
		{
			case CLUBS:
				return 0;
			case DIAMONDS:
				return 1;
			case HEARTS:
				return 2;
			case SPADES:
				return 3;
		}
		return -1; //Should not be reached
	}
	
	public String toString()
	{
		switch(this)
		{
			case CLUBS:
				return "C";
			case DIAMONDS:
				return "D";
			case HEARTS:
				return "H";
			case SPADES:
				return "S";
			/*case CLUBS:
				return "" + (char)0x2663;
			case DIAMONDS:
				return "" + (char)0x2666;
			case HEARTS:
				return "" + (char)0x2665;
			case SPADES:
				return "" + (char)0x2660;*/
		}
		return "SOMETHINGS WRONG";
	}
}
