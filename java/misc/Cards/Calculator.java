import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.text.*;
import java.util.ArrayList;

public class Calculator extends JPanel implements ActionListener
{
	private long counter = 0;
	JTextField playerCard1;
	JTextField playerCard2;
	JTextField flop1;
	JTextField flop2;
	JTextField flop3;
	JTextField turn;
	JTextField river;
	JTextField numPlayers;
	JTextField probability;
	JTextField[] inputs;
	
	public Calculator()
	{
		playerCard1 = new JTextField(3);
		playerCard2 = new JTextField(3);
		flop1 = new JTextField(3);
		flop2 = new JTextField(3);
		flop3 = new JTextField(3);
		turn = new JTextField(3);
		river = new JTextField(3);
		numPlayers = new JTextField(2);
		probability = new JTextField(5);
		inputs = new JTextField[8];
		
		inputs[0] = playerCard1;
		inputs[1] = playerCard2;
		inputs[2] = flop1;
		inputs[3] = flop2;
		inputs[4] = flop3;
		inputs[5] = turn;
		inputs[6] = river;
		inputs[7] = numPlayers;
		
		
		probability.setText("1.0");
		
		for(JTextField inputs : inputs)
		{
			inputs.addActionListener(this);
			this.add(inputs);
		}
		this.add(probability);
	}
 
	public void actionPerformed(ActionEvent evt)
	{
		//Do calculations
		if(!numPlayers.getText().isEmpty())
		{
			
			probability.setText("" + getProbability());
		}
		else
		{
			probability.setText("1.0");
		}
	}
	
	public static void main(String[] args)
	{
		JFrame frame = new JFrame("My Calculator");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 
		//Add contents to the window.
		frame.add(new Calculator());
 
		//Display the window.
		frame.pack();
		frame.setVisible(true);
	}
	
	public double getProbability()
	{
		if(inputs[0].getText().isEmpty() || inputs[1].getText().isEmpty())
			return 0;
		else
		{
			int i = 2;
			Card[] hole = new Card[2];
			hole[0] = new Card(inputs[0].getText());
			hole[1] = new Card(inputs[1].getText());
			
			ArrayList<Card> board = new ArrayList<Card>();
			while(!inputs[i].getText().isEmpty() && i < inputs.length-1)
			{
				board.add(new Card(inputs[i].getText()));
				i++;
			}
			
			int players = Integer.parseInt(inputs[7]);
			
			Deck deck = new Deck();
			for(Card c : hole)
				deck.remove(c);
			for(Card c : board)
				deck.remove(c);
			
			return getProbability(hand, board, deck, players);
		}
		
		return 0;
	}
	
	public double getProbability(Card[] hand, ArrayList<Card> board, Deck deck, int players)
	{
		double prob = 0;
		if(board.size() < 5)
		{
			for(Card c : deck)
			{
				board.add(c);
				deck.remove(c);
				prob += getProbability(hand, board, deck, players);
				deck.add(c);
				board.remove(c);
			}
			prob = prob/counter;
			counter = 0;
		
		}
		else
		{
			
			counter ++;
		}
		
		return 0.0;
	}
}
