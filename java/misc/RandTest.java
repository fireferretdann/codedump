public class RandTest
{
	
	public static int x = 0;
	public static int w = 0;
	public static int s = 1327217885; // Integer.MAX_VALUE/phi (rounded up) for minimum irrationality
	
	public static void main(String[] args)
	{
		//x = (int)System.currentTimeMillis();
		for(int i = 0; i < 10; i ++)
		{
			System.out.println(x);
			nextRand();
		}
		
		for(int i = 0; i < 40; i ++)
		{
			System.out.println(x);
			nextRand();
		}
	}
	
	public static int nextRand()
	{
		for(int i = x%3; i >= -2; i --)
		{
			x *= x;
			w += s;
			x += w;
			x = (x>>32) | (x<<32);
		}
		return x;
	}
}