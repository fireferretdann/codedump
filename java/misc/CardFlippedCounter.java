import java.util.concurrent.ThreadLocalRandom;
import java.util.Random;

public class CardFlippedCounter
{
	public static final double probOfFlippingifFlipped = 1;
	public static final double probOfFlippingifRegular = 1;
	static Random rand = ThreadLocalRandom.current();
	public static final int deviationFromHalf = 5;
	
	public static void main(String[] args)
	{
		for(int flips = 3; flips <= 3; flips ++)
		{
			int totalReps = 3333333;
			double minTot = 0;
			double maxTot = 0;
			double totalVar = 0;
			double totalFF = 0;
			
			for(int reps = 0; reps < totalReps; reps ++)
			{
				boolean[] directions = new boolean[52];
				//flip(directions, 20);
				double min = 1;
				double max = 0;
				for(int i = 0; i < flips; i ++)
				{
					flip(directions, directions.length/2 + rand.nextInt(deviationFromHalf*2+1) - deviationFromHalf);
					//flip(directions, 27);
					//System.out.println(FF);
					/*if(FF < min)
						min = FF;
					if(FF > max)
						max = FF; */
				}
				double FF = fractionFlipped(directions);
				totalFF += FF;
				//System.out.println(min + "\t/ " + max);
				/* minTot += min;
				maxTot += max; */
				
				totalVar += Math.abs(.5 - fractionFlipped(directions));
			}
			double averageVar = totalVar/totalReps;
			double averageFF = totalFF/totalReps;
			/* variance += Math.abs((minTot/totalReps) - .5);
			variance += Math.abs((maxTot/totalReps) - .5);
			variance /= 2; */
			//System.out.println("With " + flips + " flips, the variance is " + averageVar);
			System.out.println(flips + " FF:\t" + averageFF);
			System.out.println(flips + " var:\t" + averageVar);
			
			//System.out.println("Average min: " + (minTot/totalReps));
			//System.out.println("Average max: " + (maxTot/totalReps));
		}
	}
	
	public static void flip(boolean[] a, int toFlip)
	{
		toFlip = toFlip%a.length;
		
		for(int i = 0; i < toFlip; i++)
		{
			if(a[i])
			{
				if(rand.nextDouble() < probOfFlippingifFlipped)
					a[i] = !a[i];
			}
			else
			{
				if(rand.nextDouble() < probOfFlippingifRegular)
					a[i] = !a[i];
			}
		}
		
		shuffleArray(a);
	}
	
	// Implementing Fisher–Yates shuffle
	public static void shuffleArray(boolean[] ar)
	{
		for (int i = ar.length - 1; i > 0; i--)
		{
			int index = rand.nextInt(i + 1);
			// Simple swap
			boolean a = ar[index];
			ar[index] = ar[i];
			ar[i] = a;
		}
	}
	
	public static double fractionFlipped(boolean[] cardDirections)
	{
		int count = 0;
		for(boolean flipped : cardDirections)
		{
			if(flipped)
				count ++;
		}
		
		return (double)count/(double)cardDirections.length;
	}
}