
public class SeiveEfficiency
{
	public static void main(String[] args)
	{
		for(int numPrimes = 1; numPrimes < 15; numPrimes ++)
		{
			int primes[] = new int[numPrimes];
			
			fill(primes);
			System.out.println("filled.");
			
			boolean seive[] = makeSeive(primes);
			System.out.println("seived.");
			
			int count = 0;
			for(boolean b : seive)
				if(b) count ++;
			
			double fraction = (double)count/(double)seive.length;
			
			System.out.println("for the first " + numPrimes + " primes, the fraction checked is " + fraction);
			System.out.println();
		}
	}
	
	public static boolean[] makeSeive(int[] primes)
	{
		boolean s[] = new boolean[prod(primes)];
		
		for(int i = 0; i < s.length; i ++)
		{
			s[i] = true;
			for(int p : primes)
				if(i%p == 0) s[i] = false;
		}
		
		return s;
	}
	
	public static int prod(int[] primes)
	{
		int prod = 1;
		for(int p : primes)
		{
			prod *= p;
		}
		return prod;
	}
	
	public static void fill(int[] primes)
	{
		int currPlace = 2;
		for(int i = 0; i < primes.length; i ++)
		{
			while(!isPrime(currPlace)) currPlace++;
			primes[i] = currPlace++;
		}
	}
	
	public static boolean isPrime(int n)
	{
		for(int i = 2; i*i <= n; i ++)
		{
			if(n%i == 0)
				return false;
		}
		return true;
	}
}
