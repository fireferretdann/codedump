import java.util.Scanner;

public class GoldbachTester
{
	public static Scanner input = new Scanner(System.in);
	
	
	public static void main(String[] args)
	{
		while(true)
		{
			int toTest = getInt();
			for (int i = 2; i <= toTest/2; i ++)
				if(isPrime(i)  &&  isPrime(toTest-i))
				{
					System.out.println("The number worked!\n" + i + " + " + (toTest-i) + " = " + toTest);
					break;
				}
		}
	}
	
	public static boolean isPrime(int num)
	{
		if(num == 2 || num  == 3)
			return true;
		if(num % 2 == 0  ||  num % 3 == 0)
			return false;
		for(int i = 5; i*i <= num; i += 6)
			if(num % i == 0  ||  num % (i+2) == 0)
				return false;
		return true;
	}
	
	
	public static int getInt()
	{
		while(true)
		{
			try
			{
				System.out.print("Please enter an even integer greater than two: ");
				int num = input.nextInt();
				if (num % 2 == 0 && num > 2)
					return num;
			}
			catch(NumberFormatException error) {}
		}
	}
}
