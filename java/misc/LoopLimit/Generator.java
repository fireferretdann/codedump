import java.util.LinkedList;

public class Generator
{
	public static void main(String[] args)
	{
		int numLoops = Integer.parseInt(args[0]);
		LinkedList<String> lines =  new LinkedList<String>();
		lines.add("public class Loops{");
		lines.add("public static void main(String[] args){");
		
		lines.add("long count = 0;");
		
		for(int i = 0; i < numLoops; i ++)
			lines.add("for(int i"+i+" = 0; i"+i+" < 2; i"+i+"++)");
		
		lines.add("count++;");
		
		lines.add("System.out.println(count);");
		lines.add("}");
		lines.add("}");
		
		for(String s : lines)
			System.out.println(s);
	}
}
