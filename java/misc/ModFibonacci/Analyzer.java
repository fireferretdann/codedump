

public class Analyzer
{
	final static int maxSequence = 30000;
	
	public static void main(String[] args)
	{
		int missingCount[] = new int[maxSequence];
		
		for(int length = 2; length <= maxSequence; length ++)
		{
			Sequence blah = new Sequence(length);
			blah.run();
			
			for(int i = 0; i < blah.reachables.length; i ++)
			{
				if(!blah.reachables[i])
					missingCount[i] ++;
			}
		}
		
		System.out.println();
		for(int i = 0; i < 100; i ++)
		{
			System.out.println(i + "\t" + missingCount[i] + "\t" + (double)missingCount[i]/(double)(maxSequence-i));
		}
		System.out.println();
	}
}
