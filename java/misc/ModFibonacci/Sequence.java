import java.util.HashSet;
import java.util.ArrayList;
import java.awt.Point;

public class Sequence
{
	final int MOD;
	HashSet<Point> pairsVisited = new HashSet<Point>();
	int a, b;
	boolean[] reachables;
	
	public Sequence(int modulous)
	{
		MOD = modulous;
		a = 0;
		b = 1;
		reachables = new boolean[MOD];
	}
	
	public int next()
	{
		int temp = (a+b)%MOD;
		a = b;
		b = temp;
		
		pairsVisited.add(new Point(a,b));
		reachables[a] = true;
		
		return a;
	}
	
	public boolean hasNext()
	{
		return !pairsVisited.contains(new Point(b, a+b));
	}
	
	public void run()
	{
		while(this.hasNext())
		{
			//System.out.println(next());
			next();
		}
	}
	
	public void printMissing()
	{
		for(int i = 0; i < MOD; i++)
			if(!reachables[i])
				System.out.println(i);
	}
	
	public ArrayList<Integer> missing()
	{
		ArrayList<Integer> res = new ArrayList<Integer>();
		
		for(int i = 0; i < MOD; i++)
			if(!reachables[i])
				res.add(i);
		
		return res;
	}
	
	public static void main(String[] args)
	{
		Sequence blep = new Sequence(40);
		blep.run();
	}
}
