import java.util.Scanner;
import java.io.PrintWriter;

public class Sumer
{
	static Scanner input = new Scanner(System.in);
	
	public static void main(String args[])
	{
		double a = -2;
		double b = -3;
		double x = 1-2*Math.random();
		System.out.print("Starting x: " + x);
		
		try
		{
			PrintWriter writer = new PrintWriter("SumerOutput.txt", "UTF-8");
		
			for(int i = 0; i < 100000; i ++)
			{
				x = a + b/x;
				writer.println(x);
			}
			
			writer.close();
		} catch(Exception e) {};
	}
	
	/*public static void main(String args[])
	{
		while(true)
		{
			System.out.println("Using xNew = a + b/xOld");
			
			System.out.print("a: ");
			double a = input.nextDouble();
			
			System.out.print("b: ");
			double b = input.nextDouble();
			
			System.out.print("Starting x: ");
			double x = input.nextDouble();
			
			while(input.nextLine().equals(""))
			{
				x = a + b/x;
				System.out.print(x);
			}
		}
	}*/
}