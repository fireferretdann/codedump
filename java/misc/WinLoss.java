import java.util.Scanner;

public class WinLoss
{
	public static int wins = 0;
	public static int losses = 0;
	public static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args)
	{
		String s = "";
		
		while (recordString(s))
			s = input.nextLine();
	}
	
	// Counts wins and losses and quits if invalid string.
	public static boolean recordString(String s)
	{
		s = s.toLowerCase();
		
		if(s.equals(""))
			return true;
		if(s.equals("w")  ||  s.equals("win")  ||  s.equals("+")) 
			wins ++;
		else if(s.equals("l")  ||  s.equals("loss")  ||  s.equals("-")  ||  s.equals("lose")) 
			losses ++;
		else
			return false;
		
		double percent = (double)wins / (double)(losses + wins);
		System.out.println("Current wins: " + wins);
		System.out.println("Current losses: " + losses);
		System.out.println("Current win percentage: " + percent);
		
		if(percent < 1.0/3.0)
			System.out.println("Decrease Difficulty.");
		
		if(percent > 2.0/3.0)
			System.out.println("Improve Difficulty.");
		
		System.out.println();
		return true;
	}
}
