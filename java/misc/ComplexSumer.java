import java.util.Scanner;
import java.io.PrintWriter;

public class ComplexSumer
{
	static Scanner input = new Scanner(System.in);
	
	public static void main(String args[])
	{
		ComplexNumber a = new ComplexNumber(-2, 0, false);
		ComplexNumber b = new ComplexNumber(-3, 0, false);
		ComplexNumber x = new ComplexNumber(-1, 1.414, false);
		System.out.print("Starting x: " + x);
		
		try
		{
			PrintWriter writer = new PrintWriter("SumerOutput.txt", "UTF-8");
		
			for(int i = 0; i < 100000; i ++)
			{
				x = a.add(b.divide(x));
				writer.println(x);
			}
			
			writer.close();
		} catch(Exception e) {};
	}
	
	/*public static void main(String args[])
	{
		while(true)
		{
			System.out.println("Using xNew = a + b/xOld");
			
			System.out.print("a: ");
			double a = input.nextDouble();
			
			System.out.print("b: ");
			double b = input.nextDouble();
			
			System.out.print("Starting x: ");
			double x = input.nextDouble();
			
			while(input.nextLine().equals(""))
			{
				x = a + b/x;
				System.out.print(x);
			}
		}
	}*/
}