public class GeometricMean
{
	public static void main(String args[])
	{
		System.out.println(continuousMean(.3, 10, Integer.MAX_VALUE/5));
	}
	
	public static double mean(double[] data)
	{
		double workingMean = 1;
		
		for(double d : data)
			workingMean *= Math.pow(d, 1.0/data.length);
		
		return workingMean;
	}
	
	public static double continuousMean(double start, double end, int numSamples)
	{
		double data[] = new double[numSamples];
		double interval = (end-start)/(numSamples-1);
		
		for(int i = 0; i < numSamples-1; i ++)
		{
			data[i] = start + i*interval;
		}
		
		data[numSamples-1] = end;
		
		return mean(data);
	}
}