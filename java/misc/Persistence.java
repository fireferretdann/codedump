import java.math.BigInteger;
import java.util.HashMap;

public class Persistence
{
	public static HashMap<BigInteger, Integer> checked = new HashMap<BigInteger, Integer>();
	
	public static void main(String[] args)
	{
		search();
	}
	
	public static void search()
	{
		int maxPers = 0;
		BigInteger n = BigInteger.ONE;
		
		while(maxPers < 12)
		{
			if(persistence(n) > maxPers)
			{
				maxPers = persistence(n);
				System.out.println("Persistence of \t" + maxPers + " \tat \t" + n);
			}
			n = n.add(BigInteger.ONE);
		}
	}
	
	public static int persistence(BigInteger n)
	{
		if(checked.containsKey(n))
			return checked.get(n);
		else if(n.compareTo(BigInteger.TEN) < 0)
		{
			checked.put(n, 0);
			return 0;
		}
		else
		{
			int pers = 1 + persistence(step(n));
			checked.put(n, pers);
			return pers;
		}
	}
	
	public static BigInteger step(BigInteger n)
	{
		BigInteger res = BigInteger.ONE;
		while(n.compareTo(BigInteger.ZERO) > 0)
		{
			BigInteger[] divmod = n.divideAndRemainder(BigInteger.TEN);
			res = res.multiply(divmod[1]);
			n = divmod[0];
		}
		return res;
	}
}
