import java.util.Random;
import java.util.HashSet;
import java.util.Scanner;
import java.nio.file.Files;
import java.nio.file.Paths;

public class PasswordGenerator
{
	static final int MIN_WORD_LENGTH = 4;
	static final int MAX_WORD_LENGTH = 7;
	
	public static void main(String args[])
	{
		String[] words = prepareWords();
		Scanner input = new Scanner(System.in);
		
		System.out.println("Please enter a partial seed (optional): ");
		Random rand = new Random(input.nextLine().hashCode() + (new Random()).nextLong());
		
		System.out.println("Please enter the number of words to include: ");
		int wordCount = input.nextInt();
		
		System.out.println("Please enter the number of digits to include: ");
		int digitCount = input.nextInt();
		
		System.out.println("Please enter the number of passwords to generate: ");
		int passwordCount = input.nextInt();
		
		for(int n = 0; n < passwordCount; n ++)
		{
			String password = "";
			for(int i = 0; i < wordCount; i ++)
			{
				password += words[rand.nextInt(words.length)];
			}
			for(int i = 0; i < digitCount; i++)
			{
				int digit = rand.nextInt(10);
				int place = rand.nextInt(password.length());
				
				password = password.substring(0, place) + digit + password.substring(place);
			}
			System.out.println(password);
		}
	}
	
	public static String[] prepareWords()
	{
		HashSet<String> allWords = new HashSet<String>();
		HashSet<String> wordsSet = new HashSet<String>();
		
		try
		{
			allWords.addAll(Files.readAllLines(Paths.get("D:\\Other Documents\\codedump\\java\\misc\\Password Generator\\words.txt")));
			//allWords.addAll(Files.readAllLines(Paths.get("D:\\Other Documents\\codedump\\java\\misc\\Password Generator\\words_alpha\\words_alpha.txt")));
		}
		catch(Exception e)
		{
			System.err.println("Failed to read words file");
		}
		
		for(String word : allWords)
		{
			if(word.length() >= MIN_WORD_LENGTH  &&  word.length() <= MAX_WORD_LENGTH)
				wordsSet.add(word);
		}
		
		return wordsSet.toArray(new String[0]);
	}
}