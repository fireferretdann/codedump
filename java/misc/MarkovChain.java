import java.util.Scanner;
import java.util.Arrays;
import java.io.File;
import java.text.DecimalFormat;

public class MarkovChain
{
	public static DecimalFormat form = new DecimalFormat("0.00000");
	public static double margin = .000000000000001;
	public static long multCount = 0;
	
	public static void main(String[] args)
	{
		/*double[][]a = {{1,2,3},{0,0,0},{5,5,5}};
		double[][]b = {{-1,0,2},{1,1,4},{3,2,1}};
		print(multiply(a,b));*/
		
		Scanner input = new Scanner(System.in);
		double[][] probMatrix = new double[13][13];
		double[][] resultMatrix = new double[1][13];
		
		try
		{
			Scanner file = new Scanner(new File("/home/mclark/Documents/outputMatrix.txt"));
			for(int i = 0; i < 13; i ++)
			{
				for(int j = 0; j < 13; j ++)
				{
					probMatrix[i][j] = file.nextDouble();
				}
			}
			probMatrix[0][0] = 0;
			probMatrix[0][1] = 1;
			resultMatrix[0][1] = 1;
			
		}catch(java.io.FileNotFoundException e)
		{
			System.err.println("Did not find file");
			return;
		}
		/*
		while(input.nextLine().equals(""))
		{
			print(resultMatrix);
			resultMatrix = multiply(resultMatrix, probMatrix);
		}*/
		print(probMatrix);
		input.nextLine();
		
		double[][] oldResult = new double[resultMatrix.length][resultMatrix[0].length];
		System.out.println("Flowing...");
		long count = 0;
		while(!areNearlyEqual(oldResult, resultMatrix))
		{
			count++;
			oldResult = copy(resultMatrix);
			resultMatrix = multiply(resultMatrix, probMatrix);
			print(resultMatrix);
			input.nextLine();
		}
		print(resultMatrix);
		System.out.println(count);
		System.out.println(multCount);
	}
	
	
	public static void print(double[][] m)
	{
		for(double[] v : m)
		{
			for(double d : v)
				System.out.print(form.format(d) + "  ");
			System.out.println();
		}
	}
	
	
	public static double[][] multiply(double[][] a, double[][] b)
	{
		if(a[0].length != b.length)
			return null;
		
		double[][] result = new double[a.length][b[0].length];
		
		for(int row = 0; row < result.length; row ++)
		{
			for(int col = 0; col < result[0].length; col ++)
			{
				for(int i = 0; i < b.length; i ++)
				{
					result[row][col] += a[row][i]*b[i][col];
					multCount++;
				}
			}
		}
		
		return result;
	}
	
	
	public static boolean areNearlyEqual(double[][] a, double[][] b)
	{
		if(a.length != b.length)
			return false;
		for(int i = 0; i < a.length; i++)
		{
			if(a[i].length != b[i].length)
				return false;
			for(int j = 0; j < a[i].length; j++)
			{
				if(a[i][j] <= b[i][j] - margin)
					return false;
				if(a[i][j] >= b[i][j] + margin)
					return false;
			}
		}
		
		return true;
	}
	
	
	public static boolean areEqual(double[][] a, double[][] b)
	{
		return Arrays.deepEquals(a,b);
	}
	
	
	public static double[][] copy(double[][] src)
	{
		double[][] result = new double[src.length][src[0].length];
		for(int i = 0; i < result.length; i ++)
		{
			System.arraycopy(src[i], 0, result[i], 0, result[i].length);
		}
		
		return result;
	}
}
