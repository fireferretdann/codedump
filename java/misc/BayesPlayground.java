import java.util.Scanner;

public class BayesPlayground
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		double guess = .5;
		
		while(guess != -1)
		{
			System.out.println("\nPlease enter the pop %, true + %, then true - %");
			guess = input.nextDouble(); // Probability of positive (Starts with population percentage)
			double sensitivity = input.nextDouble(); // Rate of true positives
			double specificity = input.nextDouble(); // Rate of true negatives
			int i = 0;
			
			System.out.println("After " + i++ + " positive tests, the probability is " + guess);
			
			while (guess > 0 && guess < 1)
			{
				boolean result = true;
				guess = doFormula(guess, sensitivity, specificity, result);
				
				System.out.println("After " + i++ + " positive tests, the probability is " + guess);
			}
		}
	}
	
	public static double doFormula(double guess, double sensitivity, double specificity, boolean isPositive)
	{
		if(isPositive)
		{
			double pba = sensitivity;
			double pa  = guess;
			double pb  = pba*pa + (1-specificity)*(1-pa);
			double pab = pba*pa/pb;
			return pab;
		}
		return guess;
	}
}
