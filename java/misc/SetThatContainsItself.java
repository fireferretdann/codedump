import java.util.HashSet;
import java.util.Iterator;

public class SetThatContainsItself
{
	public static void main(String[] args)
	{
		HashSet<HashSet> set = new HashSet<HashSet>();
		
		System.out.println("Made");
		
		set.add(set);
		
		System.out.println("Added");
		System.out.println(set);
		
		System.out.println(set.size());
		
		System.out.println(set.isEmpty());
		
		Iterator i = set.iterator();
		
		System.out.println("Iterator made");
		
		System.out.println(i);
		System.out.println(i.hasNext());
		System.out.println(i.next());
		
		System.out.println(set.contains(set));
	}
}
