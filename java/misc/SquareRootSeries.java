
public class SquareRootSeries
{
	public static void main(String[] args)
	{
		double result = 1;
		long balls = 0;
		do
		{
			balls += 2;
			result *= balls-1;
			result /= balls;
			balls--;
			if(balls % 1000000 == 0)
				System.out.println(result + "\t*\t" + balls + "\t=\t" + result*balls);
		}while (result*balls > .1);
		System.out.println(result + "\t*\t" + balls + "\t=\t" + (long)(result*balls));
		
		/*for(long i = 1; i <= 20000; i ++) 
		{
			double result = 1;
			int balls = 0;
			for(int n = 1; n < i; n ++)
			{
				balls += 10;
				result *= balls-1;
				result /= balls;
				balls--;
			}
			System.out.println(result);
		}*/
	}
}
