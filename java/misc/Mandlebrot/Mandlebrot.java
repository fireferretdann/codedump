import java.util.HashSet;

public class Mandlebrot
{
	static HashSet<ComplexNumber> outNumbers = new HashSet<ComplexNumber>();
	static HashSet<ComplexNumber> inNumbers = new HashSet<ComplexNumber>();
	
	public static void main(String[] args)
	{
		java.util.Scanner input = new java.util.Scanner(System.in);
		String in = input.nextLine();
		while(!in.equals("quit"))
		{
			ComplexNumber toTest = new ComplexNumber(in);
			if(isInSet(toTest))
				System.out.println("This number is in the Mandlebrot set.");
			else
				System.out.println("This number is NOT in the Mandlebrot set.");
			in = input.nextLine();
		}
	}
	
	public static boolean isInSet(ComplexNumber c)
	{
		if(outNumbers.contains(c))
			return false;
		else if (inNumbers.contains(c))
			return true;
		HashSet<ComplexNumber> tested = new HashSet<ComplexNumber>();
		ComplexNumber z = c.clone();
		while(z.absDouble() < 2)
		{
			tested.add(z);
			
			System.out.println("Tested " + z);
			(new java.util.Scanner(System.in)).nextLine();
			
			z = next(z, c);
			if(tested.contains(z))
			{
				inNumbers.add(c);
				return true;
			}
		}
		
		outNumbers.add(c);
		
		return false;
	}
	
	public static ComplexNumber next(ComplexNumber z, ComplexNumber c)
	{
		return (z.multiply(z)).add(c);
	}
}
