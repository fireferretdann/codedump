

public class MyBinTreeNode
{
	private int value;
	private MyBinTreeNode left;
	private MyBinTreeNode right;
	
	public MyBinTreeNode()
	{
		value = 0;
	}
	
	public MyBinTreeNode(int i)
	{
		value = i;
	}
	
	public MyBinTreeNode getLeft()
	{
		return left;
	}

	public MyBinTreeNode getRight()
	{
		return right;
	}
	
	// Tries to add a child to left, then right, then whichever has less children.
	public void addChild(int i)
	{
		if(left == null)
			left = new MyBinTreeNode(i);
		else if(right == null)
			right = new MyBinTreeNode(i);
		else if(left.getChildrenCount() <= right.getChildrenCount())
			left.addChild(i);
		else
			right.addChild(i);
	}
	
	public void addLeft(MyBinTreeNode tree)
	{
		left = tree;
	}
	
	public void addRight(MyBinTreeNode tree)
	{
		right = tree;
	}
	
	public int getChildrenCount()
	{
		int c = 1;
		if (left != null)
		{
			c += left.getChildrenCount();
			if (right != null)
				c += right.getChildrenCount();
		}
		return c;
	}
	
	public void print()
	{
		System.out.println(this.toString());
	}
	
	public String toString()
	{
		String s = "" + value;
		
		if(left == null  &&  right == null)
			return s;
		
		s += " [";
		
		if(left == null)
			s += "n";
		else
			s += left.toString();
		
		s += ", ";
		
		if(right == null)
			s += "n";
		else
			s += right.toString();
		
		s += "]";
		
		return s;
	}
}
