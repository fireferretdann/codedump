public class ChangeOptimizer
{
	// Solution found: 2 pennies, 1 nickel, 1 dime, 1 quarter
	
	// Change stored as int[] with 0 being pennies, 1 nickels, 2 dimes, 3 quarters
	public static void main(String[] args)
	{
		int minCoinsLeftTotal = 100000;
		
		// Testing that "countOptimumChange" method is correct:
		/*for(int cost = 0; cost < 100; cost ++)
		{
			System.out.println("cost: " + cost);
			System.out.println(countOptimumChange(cost, new int[]{2,1,1,1}));
		}*/
		
		for(int pennies = 0; pennies < 5; pennies ++)
		{
			for(int nickels = 0; nickels < 2; nickels ++)
			{
				for(int dimes = 0; dimes < 4; dimes ++) // Try three dimes as well to be sure
				{
					for(int quarters = 0; quarters < 4; quarters ++)
					{
						//System.out.println(pennies + ", " + nickels + ", " + dimes + ", " + quarters);
						//System.out.println(countCoins(new int[]{pennies, nickels, dimes, quarters}));
						
						int totalCoinsLeft = 0;
						for(int cost = 0; cost < 100; cost ++)
						{
							totalCoinsLeft += countOptimumChange(cost, new int[]{pennies, nickels, dimes, quarters});
						}
						
						if(totalCoinsLeft < minCoinsLeftTotal)
						{
							minCoinsLeftTotal = totalCoinsLeft;
							System.out.println("New minimum: " + totalCoinsLeft);
							System.out.print("Found with the coins: ");
							System.out.println(pennies + ", " + nickels + ", " + dimes + ", " + quarters);
							System.out.println();
						}
					}
				}
			}
		}
		
		System.out.println("Average of " + ((double)minCoinsLeftTotal/100.0) + " coins left after buying");
	}
	
	public static int returnChange(int cost)
	{
		int[] change = new int[4];
		
		while(cost >= 25)
		{
			change[3] ++;
			cost -= 25;
		}
		while(cost >= 10)
		{
			change[2] ++;
			cost -= 10;
		}
		while(cost >= 5)
		{
			change[1] ++;
			cost -= 5;
		}
		while(cost >= 1)
		{
			change[0] ++;
			cost -= 1;
		}
		
		return countCoins(change);
	}
	
	public static int countOptimumChange(int cost, int[] changeOnHand)
	{
		int min = 100;
		int handCount = countCoins(changeOnHand);
		//System.out.println(handCount);
		
		for(int pennies = 0; pennies <= changeOnHand[0]; pennies ++)
		{
			for(int nickels = 0; nickels <= changeOnHand[1]; nickels ++)
			{
				for(int dimes = 0; dimes <= changeOnHand[2]; dimes ++) // Try three dimes as well to be sure
				{
					for(int quarters = 0; quarters <= changeOnHand[3]; quarters ++)
					{
						int changeGiven = pennies + 5*nickels + 10*dimes + 25*quarters;
						int coinCount = returnChange((cost + 200 - changeGiven)%100) + handCount - pennies - nickels - dimes - quarters;
						
						/*System.out.println("changeGiven " + changeGiven);
						System.out.println("returnChange(" + ((cost + 200 - changeGiven)%100) + ") = " + returnChange(((cost + 200 - changeGiven)%100)));
						System.out.println("coinCount " + coinCount);
						System.out.println();*/
						
						if(coinCount < min)
						{
							min = coinCount;
						}
					}
				}
			}
		}
		
		return min;
		
		/*// This may not be the best way to do it. if something costs 0.06 and you have 2 pennies, this will use both pennies.
		while(cost >= 25 && changeOnHand[3] > 0)
		{
			changeOnHand[3] --;
			cost -= 25;
		}
		while(cost >= 10 && changeOnHand[2] > 0)
		{
			changeOnHand[2] --;
			cost -= 10;
		}
		while(cost >= 5 && changeOnHand[1] > 0)
		{
			changeOnHand[1] --;
			cost -= 5;
		}
		while(cost >= 1 && changeOnHand[0] > 0)
		{
			changeOnHand[0] --;
			cost -= 1;
		}
		
		return returnChange(cost) + countCoins(changeOnHand);*/
	}
	
	public static int countCoins(int[] change)
	{
		int count = 0;
		for(int i = 0; i < change.length; i ++)
			count += change[i];
		return count;
	}
}