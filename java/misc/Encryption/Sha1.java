import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
 
public class Sha1
{
	/**
	 * @param args
	 * @throws NoSuchAlgorithmException 
	 */
	static String sha1(String input) throws NoSuchAlgorithmException
	{
		MessageDigest mDigest = MessageDigest.getInstance("SHA1");
		byte[] result = mDigest.digest(input.getBytes());
		
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < result.length; i++)
		{
			sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
		}
		 
		return sb.toString();
	}
	
	static String myHash(String input)
	{
		int h0 = 0x67452301;
		int h1 = 0xEFCDAB89;
		int h2 = 0x98BADCFE;
		int h3 = 0x10325476;
		int h4 = 0xC3D2E1F0;
		
		byte[] message = input.getBytes();
		
		long ml = message.length*8;
		message = add(message, (byte)0x80);
		
		while((message.length*8)%512 != 448)
		{
			message = add(message, (byte)0);
		}
		
		long toAppend = Long.reverseBytes(ml);
		for(int i = 0; i < 8; i++)
		{
			message = add(message, (byte)toAppend);
			toAppend = toAppend >> 8;
		}
		
		//for(byte b : message)
			//System.out.print(b + "\t");
		int a = h0;
		int b = h1;
		int c = h2;
		int d = h3;
		int e = h4;
		
		//for each chunk:
		for(int firstByteOfChunk = 0; firstByteOfChunk < message.length; firstByteOfChunk += 64)
		{
			int[] words = new int[80];
			for(int word = 0; word < 16; word ++)
			{
				for(int i = 0; i < 4; i ++)
				{
					words[word] = words[word] << 8;
					words[word] += message[firstByteOfChunk + word*4 + i];
				}
				//System.out.println(Integer.toHexString(words[word]));
				//(new java.util.Scanner(System.in)).nextLine();
			}
			for(int i = 16; i < 80; i ++)
			{
				words[i] = Integer.rotateLeft(words[i-3] ^ words[i-8] ^ words[i-14] ^ words[i-16], 1);
			}
			
			a = h0;
			b = h1;
			c = h2;
			d = h3;
			e = h4;
			
			for(int i = 0; i < 79; i ++)
			{
				int f;
				int k;
				if(i < 20)
				{
					f = (b&c) | ((~b)&d);
					k = 0x5A827999;
				}
				else if(i < 40)
				{
					f = b^c^d;
					k = 0x6ED9EBA1;
				}
				else if(i < 60)
				{
					f = (b&c) | (b&d) | (c&d);
					k = 0x8F1BBCDC;
				}
				else
				{
					f = b^c^d;
					k = 0xCA62C1D6;
				}
				
				int temp = Integer.rotateLeft(a, 5) + f + e + k + words[i];
				e = d;
				d = c;
				c = Integer.rotateLeft(b,30);
				b = a;
				a = temp;
			}
			
			h0 += a;
			h1 += b;
			h2 += c;
			h3 += d;
			h4 += e;
		}
		
		String result = "";
		
		String[] hs = {Integer.toHexString(h0),Integer.toHexString(h1),Integer.toHexString(h2),Integer.toHexString(h3),Integer.toHexString(h4)};
		for(String h : hs)
		{
			while(h.length() < 8)
				h = "0" + h;
			result += h;
		}
		
		return result;
	}
	
	private static byte[] add(byte[] array, byte toAdd)
	{
		byte[]temp = new byte[array.length+1];
		System.arraycopy(array, 0, temp, 0, array.length);
		temp[array.length] = toAdd;
		return temp;
	}
}
