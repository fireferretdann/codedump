
public class Tester
{
	public static void main(String[] args)
	{
		String s = (new java.util.Scanner(System.in)).nextLine();
		try
		{
			System.out.println(Sha1.sha1(s));
		}
		catch(Exception e)
		{
			System.err.println(e);
		}
		System.out.println(Sha1.myHash(s));
	}
}
