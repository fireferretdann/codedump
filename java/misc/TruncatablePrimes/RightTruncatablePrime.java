import java.util.ArrayList;

public class RightTruncatablePrime
{
	long data;
	
	public static void main(String args[])
	{
		Tree<RightTruncatablePrime> tree = new Tree<RightTruncatablePrime>(new RightTruncatablePrime(0l));
		
		int oldLength = 0;
		while(oldLength != tree.makeArrayList().size())
		{
			oldLength = tree.makeArrayList().size();
			generatePrimeTreeLayer(tree);
		}
			tree.printAsTree();
			printOrderedTree(tree);
	}
	
	public static void generatePrimeTreeLayer(Tree<RightTruncatablePrime> tree)
	{
		for(Tree<RightTruncatablePrime> leaf : tree.getLeaves())
		{
			for(int i = 0; i < 10; i ++)
			{
				long candidate = leaf.getData().data*10 + i;
				if(isPrime(candidate))
				{
					RightTruncatablePrime newPrime = new RightTruncatablePrime(candidate);
					leaf.add(newPrime);
					//generatePrimeTreeLayer(tree.getNode(newPrime));
				}
			}
		}
	}
	
	public RightTruncatablePrime(long thisPrime)
	{
		data = thisPrime;
	}
	
	private static boolean isPrime(long candidate)
	{
		if(candidate <= 1)
			return false;
		if(candidate == 2)
			return true;
		if(candidate%2 == 0)
			return false;
		for(long i = 3; i*i <= candidate; i += 2)
		{
			if(candidate%i == 0)
				return false;
		}
		
		return true;
	}
	
	public String toString()
	{
		return ""+data;
	}
	
	public boolean equals(Object o)
	{
		if(o instanceof RightTruncatablePrime)
			return data == ((RightTruncatablePrime)o).data;
		return false;
	}
	
	public static void printOrderedTree(Tree tree)
	{
		ArrayList<RightTruncatablePrime> list = (ArrayList<RightTruncatablePrime>)tree.makeArrayList();
		mergeSort(list);
		System.out.println(list + "\n");
	}
	
	private static void mergeSort(ArrayList<RightTruncatablePrime> list)
	{
		if(list.size() <= 1)
			return;
		ArrayList<RightTruncatablePrime> firstHalf = new ArrayList<RightTruncatablePrime>(list.subList(0, list.size()/2));
		ArrayList<RightTruncatablePrime> secondHalf = new ArrayList<RightTruncatablePrime>(list.subList(list.size()/2, list.size()));
		mergeSort(firstHalf);
		mergeSort(secondHalf);
		list = merge(firstHalf, secondHalf);
	}
	
	private static ArrayList<RightTruncatablePrime> merge(ArrayList<RightTruncatablePrime> list1, ArrayList<RightTruncatablePrime> list2)
	{
		ArrayList<RightTruncatablePrime> list = new ArrayList<RightTruncatablePrime>();
		int i1 = 0;
		int i2 = 0;
		
		while(i1 < list1.size()  &&  i2 < list2.size())
		{
			if(list1.get(i1).data <= list2.get(i2).data)
				list.add(list1.get(i1++));
			else
				list.add(list2.get(i2++));
		}
		
		for(int i = i1; i < list1.size(); i++)
			list.add(list1.get(i1));
		for(int i = i2; i < list2.size(); i++)
			list.add(list2.get(i2));
		
		return list;
	}
}