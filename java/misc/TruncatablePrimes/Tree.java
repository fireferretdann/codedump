import java.util.ArrayList;

public class Tree<T>
{
	private ArrayList<Tree<T>> children;
	private T data;
	private Tree<T> parent;
	
	public Tree(T rootData)
	{
		children = new ArrayList<Tree<T>>();
		data = rootData;
	}
	
	public void add(Tree<T> branch)
	{
		if(!contains(branch))
			children.add(branch);
	}
	
	public void add(T element)
	{
		add(new Tree<T>(element));
	}
	
	public ArrayList<Tree<T>> getLeaves()
	{
		ArrayList<Tree<T>> leaves = new ArrayList<Tree<T>>();
		ArrayList<Tree<T>> nodes = makeNodeList();
		for(Tree<T> node : nodes)
		{
			if(node.children.size() == 0)
				leaves.add(node);
		}
		return leaves;
	}
	
	public Tree<T> getNode(T element)
	{
		if(this.data.equals(element))
			return this;
		
		for(Tree<T> branch : children)
		{
			Tree t = branch.getNode(element);
			if(t != null)
				return t;
		}
		
		return null;
	}
	
	public ArrayList<Tree<T>> makeNodeList()
	{
		ArrayList<Tree<T>> list = new ArrayList<Tree<T>>();
		
		list.add(this);
		
		for(Tree branch : children)
		{
			list.addAll(branch.makeNodeList());
		}
		
		return list;
	}
	
	public ArrayList<T> makeArrayList()
	{
		ArrayList<T> list = new ArrayList<T>();
		
		list.add(data);
		
		for(Tree branch : children)
		{
			list.addAll(branch.makeArrayList());
		}
		
		return list;
	}
	
	public boolean equals(Object o)
	{
		if(o instanceof Tree)
		{
			return ((Tree)o).getData().equals(this.getData());
		}
		return false;
	}
	
	public void printAsTree()
	{
		printAsTree(0);
	}
	
	public void printAsTree(int depth)
	{
		for(int i = 0; i < depth; i ++)
			System.out.print(".");
		
		System.out.println(data);
		
		for(Tree branch : children)
		{
			branch.printAsTree(1+depth);
		}
	}
	
	public T getData()
	{
		return data;
	}
	
	public String toString()
	{
		return this.makeArrayList().toString();
	}
	
	public boolean contains(Tree<T> node)
	{
		if(this.equals(node))
			return true;
		for(Tree<T> branch : children)
			if(branch.contains(node))
				return true;
		
		return false;
	}
}