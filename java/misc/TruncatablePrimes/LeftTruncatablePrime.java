import java.util.ArrayList;
import java.math.BigInteger;

public class LeftTruncatablePrime
{
	private final static BigInteger TWO = new BigInteger("2");
	BigInteger data;
	
	public static void main(String args[])
	{
		Tree<LeftTruncatablePrime> tree = new Tree<LeftTruncatablePrime>(new LeftTruncatablePrime(BigInteger.ZERO));
		
		int oldLength = 0;
		while(oldLength != tree.makeArrayList().size())
		{
			oldLength = tree.makeArrayList().size();
			generatePrimeTreeLayer(tree);
		}
			tree.printAsTree();
			printOrderedTree(tree);
	}
///////////////////////////////////////////////////////////  TODO: REPLACE LONG WITH BIGINTEGER  ///////////////////////////////////////////////////////////
	public static void generatePrimeTreeLayer(Tree<LeftTruncatablePrime> tree)
	{
		for(Tree<LeftTruncatablePrime> leaf : tree.getLeaves())
		{
			for(int i = 0; i < 10; i ++)
			{
				BigInteger leafData = leaf.getData().data;
				BigInteger candidate = new BigInteger(""+i);
				if(leafData.compareTo(BigInteger.ZERO) > 0)
				{
					int power = (int)Math.ceil(Math.log10(leafData.doubleValue()));
					/*System.out.println("leafData: " + leafData);
					System.out.println("Power: " + power);
					System.out.println();*/
					candidate = leafData.add((new BigInteger("" + i)).multiply(BigInteger.TEN.pow(power)));
					//candidate = leafData + i*(long)Math.pow(10, power);
				}
				if(isPrime(candidate))
				{
					LeftTruncatablePrime newPrime = new LeftTruncatablePrime(candidate);
					leaf.add(newPrime);
					//generatePrimeTreeLayer(tree.getNode(newPrime));
				}
			}
		}
	}
	
	public LeftTruncatablePrime(BigInteger thisPrime)
	{
		data = thisPrime;
	}
	
	private static boolean isPrime(BigInteger candidate)
	{
		if(candidate.doubleValue() <= 1)
			return false;
		if(candidate.doubleValue() == 2)
			return true;
		if(candidate.mod(TWO).equals(BigInteger.ZERO))
			return false;
		for(BigInteger i = new BigInteger("3"); i.multiply(i).compareTo(candidate) <= 0; i = i.add(TWO))
		{
			if(candidate.mod(i).equals(0))
				return false;
		}
		
		return true;
	}
	
	public String toString()
	{
		return ""+data;
	}
	
	public boolean equals(Object o)
	{
		if(o instanceof LeftTruncatablePrime)
			return data.equals(((LeftTruncatablePrime)o).data);
		return false;
	}
	
	public static void printOrderedTree(Tree tree)
	{
		ArrayList<LeftTruncatablePrime> list = (ArrayList<LeftTruncatablePrime>)tree.makeArrayList();
		mergeSort(list);
		System.out.println(list + "\n");
	}
	
	private static void mergeSort(ArrayList<LeftTruncatablePrime> list)
	{
		if(list.size() <= 1)
			return;
		ArrayList<LeftTruncatablePrime> firstHalf = new ArrayList<LeftTruncatablePrime>(list.subList(0, list.size()/2));
		ArrayList<LeftTruncatablePrime> secondHalf = new ArrayList<LeftTruncatablePrime>(list.subList(list.size()/2, list.size()));
		mergeSort(firstHalf);
		mergeSort(secondHalf);
		list = merge(firstHalf, secondHalf);
	}
	
	private static ArrayList<LeftTruncatablePrime> merge(ArrayList<LeftTruncatablePrime> list1, ArrayList<LeftTruncatablePrime> list2)
	{
		ArrayList<LeftTruncatablePrime> list = new ArrayList<LeftTruncatablePrime>();
		int i1 = 0;
		int i2 = 0;
		
		while(i1 < list1.size()  &&  i2 < list2.size())
		{
			if(list1.get(i1).data.compareTo(list2.get(i2).data) <= 0)
				list.add(list1.get(i1++));
			else
				list.add(list2.get(i2++));
		}
		
		for(int i = i1; i < list1.size(); i++)
			list.add(list1.get(i1));
		for(int i = i2; i < list2.size(); i++)
			list.add(list2.get(i2));
		
		return list;
	}
}