import java.util.Scanner;

public class RootFinder
{
	public static void main(String args[])
	{
		//Scanner input = new Scanner(System.in);
		for(int N = 1; N < 100; N ++)
		{
			System.out.println("Root for N = " + N + ": " + findMyFunctionRoot(N));
			//System.out.println(findMyFunctionRoot(N));
			//input.nextLine();
		}
	}
	
	public static double findMyFunctionRoot(int n)
	{
		//using Newton's method for approximating roots
		double backTwoGuess = 0; // accounts for back and forths caused by double's innacuracies
		double lastGuess = 0;
		double bestGuess = 2;
		while(bestGuess != lastGuess  &&  bestGuess != backTwoGuess)
		{
			backTwoGuess = lastGuess;
			lastGuess = bestGuess;
			bestGuess = bestGuess - myFuntion(n, bestGuess)/myFunctionDerivative(n, bestGuess);
		}
		
		return bestGuess;
	}
	
	public static double myFuntion(int n, double x)
	{
		double result = Math.pow(x, n);
		for(int i = n-1; i >= 0; i --)
		{
			result += -Math.pow(x, i);
		}
		return result;
	}
	
	public static double myFunctionDerivative(int n, double x)
	{
		double result = n*Math.pow(x, n-1);
		for(int i = n-1; i >= 0; i --)
		{
			result += -i*Math.pow(x, i-1);
		}
		return result;
	}
}