import java.util.ArrayList;
import java.util.Scanner;
import java.math.BigDecimal;

public class Genbonacci
{
	static final BigDecimal TWO = new BigDecimal(2);
	
	static Scanner input = new Scanner(System.in);
	static ArrayList<BigDecimal> sequence;
	static int N;
	
	public static void main(String[] args)
	{
		for(int n = 1; n < 400; n ++)
		{
			sequence = new ArrayList<BigDecimal>();
			
			//System.out.println("Please input N (fibonacci=2, tribonacci=3, etc.)");
			//N = input.nextInt();
			N = n;
			
			for(int i = 0; i < N-1; i ++)
				sequence.add(BigDecimal.ZERO);
			
			sequence.add(BigDecimal.ONE);
			
			do
			{
				for(int i = 0; i < 20+2*N; i ++)
				{
					makeNext();
					//System.out.println(last());
					//System.out.println(lastRatio());
					//System.out.println(lastRatio().compareTo(TWO));
				}
			} while(false);
			//while(!input.nextLine().equals("q"));
			
			System.out.println(n + ":\t" + lastRatio());
		}
	}
	
	public static void makeNext()
	{
		BigDecimal sum = new BigDecimal("0");
		
		for(int i = 1; i <= N; i ++)
			sum = sum.add(last(i));
		
		sequence.add(sum);
	}
	
	public static BigDecimal lastRatio()
	{
		return last(1).divide(last(2), 25+N/2, BigDecimal.ROUND_HALF_EVEN);
	}
	
	public static BigDecimal last(int i)
	{
		return sequence.get(sequence.size()-i);
	}
	
	public static BigDecimal last()
	{
		return sequence.get(sequence.size()-1);
	}
}
