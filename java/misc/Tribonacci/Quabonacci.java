import java.util.ArrayList;

public class Quabonacci
{
	static ArrayList<Long> sequence;
	
	public static void main(String[] args)
	{
		sequence = new ArrayList<Long>();
		sequence.add(0l);
		sequence.add(0l);
		sequence.add(0l);
		sequence.add(1l);
		
		for(int i = 0; i < 20; i ++)
		{
			makeNext();
			System.out.println(last());
			System.out.println(lastRatio());
		}
	}
	
	public static void makeNext()
	{
		long sum = 0l;
		
		sum += last(1);
		sum += last(2);
		sum += last(3);
		sum += last(4);
		
		sequence.add(sum);
	}
	
	public static double lastRatio()
	{
		return (double)last(1)/(double)last(2);
	}
	
	public static long last(int i)
	{
		return sequence.get(sequence.size()-i);
	}
	
	public static long last()
	{
		return sequence.get(sequence.size()-1);
	}
}
