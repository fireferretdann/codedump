import java.io.File;
import java.util.Scanner;

public class HuffmanEncoding
{
	public static void main(String[] args)
	{
		for(String file : args)
		{
			test(file);
		}
	}
	
	public static void test(String fileName)
	{
		File file = new File(fileName);
		encode(file);
		
		File encoded = new File(fileName + ".huff");
		
		decode(encoded, fileName + ".temp");
		
		File forTesting = new File(fileName + ".temp");
		forTesting.deleteOnExit();
		
		//Scanner orig = new Scanner(file);
		//Scanner shouldEqualOrig = new Scanner(forTesting);
		
	}

	public static void encode(File file, String newFileName)
	{
		File encoded = new File(newFileName);
		//Do encoding
		
		Scanner input = new Scanner(encoded);
		input.useDelimiter("");
		Counter counter = new Counter();
		
		while(input.hasNext())
		{
			counter.count(input.next());
		}
	}
	
	public static void encode(File file)
	{
		encode(file, file.getName() + ".huff");
	}
		
	public static void decode(File file, String newFileName)
	{
		File decoded = new File(newFileName);
		//Do decoding
	}
	
	public static void decode(File file)
	{
		if(file.getName().endsWith(".huff"))
		{
			String dest = file.getName();
			dest = dest.substring(0, dest.length() - 5);
			decode(file, dest);
		}
		else
		{
			System.err.println("File \"" + file.getName() + "\" was not a \".huff\" file.");
			System.exit(9);
		}
	}
}
