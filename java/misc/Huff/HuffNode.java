
public class HuffNode
{
	HuffNode left;
	HuffNode right;
	char data;
	boolean isLeaf;
	
	public HuffNode(char character)
	{
		data = character;
		isLeaf = true;
	}
	
	public HuffNode(HuffNode leftNode, HuffNode rightNode)
	{
		left = leftNode;
		right = rightNode;
	}
	
	public String toString()
	{
		if(isLeaf)
			return ""+data;
		else
			return left.toString() + right.toString();
	}
	
	public static void main(String[] args)
	{
		HuffNode[] nodes = new HuffNode[26];
		for(int i = 0; i < 26; i ++)
		{
			 nodes[i] = new HuffNode((char)('a' + i));
		}
		
		for(HuffNode n : nodes)
		{
			System.out.println(n);
		}
		
		HuffNode branch = new HuffNode(nodes[0], nodes[1]);
		HuffNode root = new HuffNode(branch, nodes[5]);
		System.out.println(root);
	}
}
