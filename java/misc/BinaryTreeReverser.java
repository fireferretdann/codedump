public class BinaryTreeReverser
{
	public static void main(String[] args)
	{
		MyBinTreeNode tree = new MyBinTreeNode(0);
		
		for (int i = 1; i < 5; i++)
			tree.addChild(i);
		
		tree.print();
		
		System.out.println("Reversing...");
		
		reverse(tree);
		
		tree.print();
	}
	
	// This function recursively swaps the left and right branches of a tree
	public static void reverse(MyBinTreeNode tree)
	{
		if(tree.getLeft() != null)
			reverse(tree.getLeft());
		
		if(tree.getRight() != null)
			reverse(tree.getRight());
		
		MyBinTreeNode newLeft = tree.getRight();
		tree.addRight(tree.getLeft());
		tree.addLeft(newLeft);
	}
}
