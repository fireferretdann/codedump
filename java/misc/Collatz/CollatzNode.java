public class CollatzNode
{
	public final int NUMBER;
	public CollatzNode doubleChild;
	public CollatzNode thirdChild;
	
	public CollatzNode(int collatzNumber)
	{
		NUMBER = collatzNumber;
	}
	
	public void generateChildren()
	{
		if(doubleChild == null)
		{
			doubleChild = new CollatzNode(NUMBER*2);
			
			if(NUMBER%3 == 1) // if this has a number x such that 3*x + 1 = this
			{
				int possibleChild = (NUMBER-1)/3;
				
				if(possibleChild%2 == 1) // if possibleChild is odd
				{
					if(possibleChild != 1) // Prevents the known loop
						thirdChild = new  CollatzNode(possibleChild);
				}
			}
		}
		else
		{
			doubleChild.generateChildren();
			if(thirdChild != null)
				thirdChild.generateChildren();
		}
	}
	
	public String toString()
	{
		String returnString = "";
		
		return returnString;
	}
	
	public int countNodes()
	{
		int count = 1;
		if(doubleChild != null)
			count += doubleChild.countNodes() + 1;
		if(thirdChild != null)
			count += thirdChild.countNodes() + 1;
		
		return count;
	}
	
	public int countLayers()
	{
		int layers = 1;
		if(doubleChild != null)
			layers += doubleChild.countLayers();
		return layers;
	}
	
	public int width()
	{
		int width = 1;
		
		if(doubleChild != null)
		{
			width += doubleChild.width();
			width += doubleChild.width();
			
		/*if(thirdChild != null)
			width += thirdChild.width();
		*/
		}
		
		return width;
	}
}