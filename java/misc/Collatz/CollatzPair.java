public class CollatzPair
{
	int number;
	String sequence;
	
	public CollatzPair(int num)
	{
		number = num;
		sequence = getSequence(number);
	}
	
	public static String getSequence(int num)
	{
		String output = "";
		while(num > 1)
		{
			if(num%2 == 0)
			{
				output = "H" + output;
				num = num/2;
			}
			else
			{
				output = "T" + output;
				num = num*3 + 1;
			}
		}
		
		return output;	
	}
	
	public String toString()
	{
		return sequence;
	}
}