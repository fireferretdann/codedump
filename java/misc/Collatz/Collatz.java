import java.util.ArrayList;

public class Collatz
{
	public static void main(String args[])
	{
		ArrayList<CollatzPair> nums = new ArrayList<CollatzPair>();
		
		for(int i = 1; i <= 1000; i++)
		{
			nums.add(new CollatzPair(i));
		}
		/*for(int i = 1; i < 1000; i++)
		{
			System.out.println(nums.get(i).number + ":\t" + nums.get(i));
		}*/
		nums = sort(nums);
		for(int i = 1; i < 1000; i++)
		{
			System.out.println(nums.get(i).number + ":\t" + nums.get(i));
		}
		
	}
	
	public static ArrayList<CollatzPair> sort(ArrayList<CollatzPair> nums)
	{
		//Bad but easy way
		ArrayList<CollatzPair> newNums = new ArrayList<CollatzPair>();
		while(nums.size() > 0)
		{
			int minElement = 0;
			for(int i = 1; i < nums.size()-1; i++)
			{
				if(nums.get(i).sequence.compareTo(nums.get(minElement).sequence) < 0)
					minElement = i;
			}
			
			newNums.add(nums.get(minElement));
			nums.remove(minElement);
		}
		
		return newNums;
	}
	
	public static String reverseString(int num)
	{
		String output = "";
		while(num > 1)
		{
			if(num%2 == 0)
			{
				output = "H" + output;
				num = num/2;
			}
			else
			{
				output = "T" + output;
				num = num*3 + 1;
			}
		}
		
		return output;	
	}
}