public class CollatzTree
{
	public CollatzNode root;
	
	public static void main(String args[])
	{
		CollatzTree tree = new CollatzTree();
		
		System.out.println(tree);
		
		for(int i = 0; i < 6; i ++)
		{
			tree.newLayer();
			System.out.println(tree);
			System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
		}
	}
	
	public CollatzTree()
	{
		root = new CollatzNode(1);
	}
	
	public void newLayer()
	{
		root.generateChildren();
	}
	
	public String toString()
	{
		int nodes = root.countNodes();
		return "";
	}
}