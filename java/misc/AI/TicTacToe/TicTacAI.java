import java.util.Random;
import java.awt.geom.Point2D;

public class TicTacAI
{
	double nodes[][];
	double weights[][];
	boolean isX;
	
	static Random rand = new Random();
	
	public TicTacAI(double[][] shape, boolean x)
	{
		nodes = new double[shape.length][];
		for(int i = 0; i < nodes.length; i ++)
		{
			nodes[i] = new double[shape[i].length];
		}
		
		nodes[0][nodes[0].length-1] = rand.nextGaussian();
		
		weights = new double[nodes.length-1][];
		for(int i = 0; i < weights.length; i ++)
		{
			weights[i] = new double[nodes[i].length*nodes[i+1].length];
		}
		
		for(int i = 0; i < weights.length; i ++)
		{
			for(int j = 0; j < weights[i].length; j++)
			{
				weights[i][j] = rand.nextGaussian();
			}
		}
		
		isX = x;
	}
	
	public int bestLegalMove(TicTacToe game)
	{
		readBoard(game.board);
		for(int i = 0; i < weights.length; i ++)
		{
			for(int j = 0; j < weights[i].length; j ++)
			{
				nodes[i+1][j/nodes[i].length] += nodes[i][j%nodes[i].length] * weights[i][j];
			}
		}
		
		Point2D[] moves = new Point2D[9];
		for(int i = 0; i < 9; i ++)
		{
			moves[i] = new Point2D.Double(i, nodes[nodes.length-1][i]);
		}
		sort(moves);
		for(int i = 0; i < 9; i ++)
		{
			if(game.isLegalMove((int)moves[i].getX()))
				return (int)moves[i].getX();
		}
		
		return -1;
	}
	
	public void sort(Point2D[] moves)
	{
		for(int i = 0; i < moves.length-1; i ++)
		{
			for(int j = i+1; j < moves.length; j ++)
			{
				if(moves[j].getY() < moves[i].getY())
				{
					Point2D temp = moves[i];
					moves[i] = moves[j];
					moves[j] = temp;
				}
			}
		}
	}
	
	public void readBoard(int[][] board)
	{
		for(int i = 0; i < 9; i ++)
		{
			int symbol = board[i%3][i/3];
			if(isX)
				nodes[0][i] = (double)((5*symbol - 3*symbol*symbol)/2);
			else
				nodes[0][i] = (double)((3*symbol*symbol - 5*symbol)/2);
		}
	}
}