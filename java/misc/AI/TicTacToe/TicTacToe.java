import java.util.Scanner;

public class TicTacToe
{
	public static Scanner input = new Scanner(System.in);
	public int[][] board;
	static TicTacAI ais[];
	
	public TicTacToe()
	{
		board = new int[3][];
		
		for(int i = 0; i < 3; i ++)
		{
			board[i] = new int[3];
		}
	}
	
	public static void main(String args[])
	{
		double[][] shape = new double[3][];
		shape[0] = new double[9];
		shape[1] = new double[16]; //ways to win * number of players
		shape[2] = new double[9];
		
		ais = new TicTacAI[2];
		ais[0] = new TicTacAI(shape, true);
		ais[1] = new TicTacAI(shape, false);
		
		TicTacToe game = new TicTacToe();
		game.play(true);
	}
	
	public int play(boolean print)
	{
		while(this.keepPlaying())
		{
			if(print)
			{
				System.out.println(this);
				System.out.println("X's turn");
				System.out.println(ais[0].bestLegalMove(this));
				xMove(ais[0].bestLegalMove(this));
				if(this.keepPlaying())
				{
					System.out.println(this);
					System.out.println("O's turn");
				System.out.println(ais[1].bestLegalMove(this));
					oMove(ais[1].bestLegalMove(this));
				}
			}
			else
			{
				// TODO
			}
		}
		int winner = this.getWinner();
		System.out.println(this);
		if(print)
		{
			System.out.print("The winner is ");
			if(winner == 1)
				System.out.println("X");
			else if(winner == 2)
				System.out.println("O");
			else
				System.out.println("no one");
		}
		return winner;
	}
	
	public void xMove(String location)
	{
		board[Integer.parseInt(location.substring(0,1))][Integer.parseInt(location.substring(location.length()-1, location.length()))] = 1;
	}
	
	public void oMove(String location)
	{
		board[Integer.parseInt(location.substring(0,1))][Integer.parseInt(location.substring(location.length()-1, location.length()))] = 2;
	}
	
	public void xMove(int location)
	{
		board[location%3][location/3] = 1;
	}
	
	public void oMove(int location)
	{
		board[location%3][location/3] = 2;
	}
	
	public boolean isLegalMove(int location)
	{
		return 0 == board[location%3][location/3];
	}
	
	public boolean keepPlaying()
	{
		boolean full = true;
		for(int i = 0; i < 3*3; i ++)
		{
			if(board[i%3][i/3] == 0)
			{
				full = false;
				break;
			}
		}
		if(full)
			return false;
		
		if(getWinner() == 0)
			return true;
		else
			return false;
	}
	
	public int getWinner()
	{
		// Diagonals
		if(board[0][0] == board [1][1] && board[1][1] == board[2][2])
			return board[1][1];
		if(board[0][2] == board [1][1] && board[1][1] == board[2][0])
			return board[1][1];
		
		for(int i = 0; i < 3; i ++)
		{
			//verticals
			if(board[i][0] == board [i][1] && board[i][1] == board[i][2])
				return board[i][1];
			//horizontals
			if(board[0][i] == board [1][i] && board[1][i] == board[2][i])
				return board[1][i];
		}
		
		return 0;
	}
	
	public String toString()
	{
		String s = "";
		
		s += blankXO(board[0][0]) + "|";
		s += blankXO(board[1][0]) + "|";
		s += blankXO(board[2][0]) + "\n-----\n";
		s += blankXO(board[0][1]) + "|";
		s += blankXO(board[1][1]) + "|";
		s += blankXO(board[2][1]) + "\n-----\n";
		s += blankXO(board[0][2]) + "|";
		s += blankXO(board[1][2]) + "|";
		s += blankXO(board[2][2]) + "\n";
		
		return s;
	}
	
	private static String blankXO(int i)
	{
		if(i == 1)
			return "X";
		if(i == 2)
			return "O";
		return " ";
	}
	
	
	
}