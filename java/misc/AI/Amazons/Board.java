public class Board
{
	// Representations: open: 0, white: 1, black: 2, burned: 3
	int[][] positions;
	
	public static void main(String args[])
	{
		System.out.println(new Board(6));
	}
	
	public Board(int size)
	{
		if(size == 6)
		{
			positions = new int[6][6];
			positions[0][3] = 1;
			positions[5][2] = 1;
			positions[2][0] = 2;
			positions[3][5] = 2;
		}
		else
		{
			System.err.println("That board size is not yet implemented");
			System.exit(1);
		}
	}
	
	// makes move and returns true if the move is legal, otherwise returns false
	public boolean move(int player, int fromX, int fromY, int toX, int toY, int shootX, int shootY)
	{
		if(positions[fromX][fromY] != player)
			return false; // Not that player's piece
		// Do more legallity checks
		
		positions[fromX][fromY] = 0;
		positions[toX][toY] = player;
		positions[shootX][shootY] = 3;
		
		
		return true;
	}
	
	public String toString()
	{
		String s = "";
		for(int j = 0; j < positions[0].length*2+1; j ++)
			s += "-";
		s += "\n";
		for(int i = 0; i < positions.length; i ++)
		{
			s += "|";
			for(int j = 0; j < positions[0].length; j ++)
			{
				switch(positions[i][j])
				{
					case 0:	s += " ";
							break;
					case 1:	s += "W";
							break;
					case 2:	s += "B";
							break;
					case 3:	s += "X";
							break;
				}
				s += "|";
			}
			s += "\n";
			for(int j = 0; j < positions[0].length*2+1; j ++)
				s += "-";
			s += "\n";
		}
		return s;
	}
}