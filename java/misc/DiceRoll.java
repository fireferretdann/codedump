import java.util.Scanner;

public class DiceRoll
{
	int sides;
	int dice;
	//??? Rules?
	
	
	public int singleDieRoll()
	{
		int total = 0;
		int side = 1 + (int)(Math.random()*sides);
		
		total += side;
		/*while(side == sides)
		{
			side = 1 + (int)(Math.random()*sides);
			total += side;
		}
		while(side == 1)
		{
			total --;
			side = 1 + (int)(Math.random()*sides);
			total += side;
		}*/
		//System.out.print(side + ", ");
		return total;
	}
	
	
	public int roll()
	{
		int total = 0;
		boolean matching = true;
		
		total = 0;
		
		while(matching && dice > 1)
		{
			int match = -1;
			for(int i = 0; i < dice; i++)
			{
				int side = singleDieRoll();
				//System.out.println("Rolled: " + side);
				//(new Scanner(System.in)).nextLine();
				if(match == -1)
					match = side;
				else if(side != match)
				{
					matching = false;
				}
				total += side;
			}
			//System.out.println("\n");
		}
		
		return total;
	}
	
	
	public DiceRoll()
	{
		sides = 6;
		dice = 1;
	}
	
	
	public DiceRoll(int numSides)
	{
		sides = numSides;
		dice = 1;
	}
	
	
	public DiceRoll(int numSides, int numDice)
	{
		sides = numSides;
		dice = numDice;
	}
	
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		System.out.println("How many sides?");
		int s = input.nextInt();
		
		System.out.println("How many dice?");
		int d = input.nextInt();
		
		DiceRoll roll = new DiceRoll(s, d);
		
		System.out.println("Rolling examples...\n");
		for(int i = 0; i < 15; i ++)
			System.out.println(roll.roll());
		
		int n = 400000;
		int total = 0;
		for (int i= 0; i < n; i++)
		{
			total += roll.roll();
		}
		
		System.out.println("The average over " + n + " rolls was: " + ((double)total/(double)n));
		System.out.println("The average of a normal roll is: " + ((double)(1 + roll.sides)/2.0)*(double)(roll.dice));
		
		System.out.println("The average per original die roll was: " + ((double)total/(double)n)/(double)roll.dice);
		System.out.println("The average of a normal roll is: " + ((double)(1 + roll.sides)/2.0));
	}
}
