
public class RandomPi
{
	public static void main(String[] args)
	{
		int a;
		int b;
		int ans;
		
		int trials = Integer.MAX_VALUE/2;
		int coprimeCount = 0;
		
		print(trials);
		
		for(int i = 0; i < trials; i ++)
		{
			a = 1 + (int)(Math.random() * (Integer.MAX_VALUE-1));
			b = 1 + (int)(Math.random() * (Integer.MAX_VALUE-1));
			
			ans = gcd(a, b);
			if(ans == 1)
				coprimeCount ++;
			/*print(a);
			print(b);
			print(ans);
			double percent = (double) coprimeCount/((double)i+1);
			print("Current Percentage: " + percent);
			print("Current Pi Estimate: " + Math.sqrt(6.0/percent));
			print();//*/
		}
		double percent = (double) coprimeCount/((double)trials);
		print("Final Percentage: " + percent);
		print("Final Pi Estimate: " + Math.sqrt(6.0/percent));
	}
	
	public static int gcd(int a, int b)
	{
		if( b == 0 )
			return a;
		return gcd( b, a%b);
	}
	
	public static void print()
	{
		System.out.println();
	}
	
	public static void print(Object thing)
	{
		System.out.println(thing);
	}
}
