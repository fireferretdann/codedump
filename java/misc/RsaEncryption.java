import java.util.Scanner;
import java.math.BigInteger;
import java.util.Random;

public class RsaEncryption
{
	private static Random rand = new Random();
	BigInteger publicKey;
	BigInteger privateKey;
	BigInteger mod;
	
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		RsaEncryption crypt = new RsaEncryption(); 
		//System.out.println("public: " + crypt.publicKey);
		//System.out.println("private: " + crypt.privateKey);
		//System.out.println("mod: " + crypt.mod);
		//System.out.println("bit length: " + crypt.mod.bitLength());
		//System.out.println();
		
		System.out.println("Ready.");
		String message;
		for(int i = 0; i < 5000; i ++)
		{
			//message = new BigInteger(crypt.mod.bitLength()-1, rand);
			//message = new BigInteger(1, input.nextLine().getBytes());
			message = input.nextLine();
			/*if(!crypt.decrypt(crypt.encrypt(message)).equals(message))
			{
				System.out.println(crypt.decrypt(crypt.encrypt(message)) + " \twas gotten from: \t" + crypt.encrypt(message));
				System.exit(91112);
			}*/
			System.out.println("Cypher:");
			//System.out.println(new String(crypt.encrypt(message).toByteArray()));
			System.out.println(crypt.encrypt(message));
			System.out.println("Message: ");
			System.out.println(crypt.decrypt(crypt.encrypt(message)));
		}
	}
	
	public RsaEncryption()
	{
		BigInteger prime1 = new BigInteger(300, 1000, rand);
		BigInteger prime2 = new BigInteger(350, 1000, rand);
		mod = prime1.multiply(prime2);
		BigInteger phi = phi(prime1, prime2);
		publicKey = new BigInteger("60");
		while(!isCoPrime(publicKey, phi))
		{
			publicKey = publicKey.add(BigInteger.ONE);
		}
		
		privateKey = publicKey.modInverse(phi);
	}
	
	public RsaEncryption(int p1, int p2)
	{
		BigInteger prime1 = new BigInteger("" + p1);
		BigInteger prime2 = new BigInteger("" + p2);
		mod = prime1.multiply(prime2);
		BigInteger phi = phi(prime1, prime2);
		
		publicKey = new BigInteger("2");
		while(!isCoPrime(publicKey, phi))
		{
			publicKey = publicKey.add(BigInteger.ONE);
		}
		
		privateKey = publicKey.modInverse(phi);
	}
	
	public RsaEncryption(BigInteger prime1, BigInteger prime2)
	{
		mod = prime1.multiply(prime2);
		BigInteger phi = phi(prime1, prime2);
		
		publicKey = new BigInteger("2");
		while(!isCoPrime(publicKey, phi))
		{
			publicKey = publicKey.add(BigInteger.ONE);
		}
		
		privateKey = publicKey.modInverse(phi);
	}
	
	public String decrypt(String cypher)
	{
		return new String(decrypt(new BigInteger(1, cypher.getBytes())).toByteArray());
	}
	
	public BigInteger decrypt(BigInteger cypher)
	{
		return cypher.modPow(privateKey, mod);
	}
	
	public String encrypt(String plain)
	{
		return new String(encrypt(new BigInteger(1, plain.getBytes())).toByteArray());
	}
	
	public BigInteger encrypt(BigInteger plain)
	{
		return plain.modPow(publicKey, mod);
	}
	
	public BigInteger getPublicKey()
	{
		return publicKey.abs();
	}
	
	private BigInteger phi(BigInteger prime1, BigInteger prime2)
	{
		BigInteger a = prime1.subtract(BigInteger.ONE);
		BigInteger b = prime2.subtract(BigInteger.ONE);
		BigInteger gcd = a.gcd(b);
		return a.multiply(b).divide(gcd);
	}
	
	private boolean isCoPrime(BigInteger a, BigInteger b)
	{
		return a.gcd(b).equals(BigInteger.ONE);
	}
}
