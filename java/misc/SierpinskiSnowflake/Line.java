import java.awt.Graphics2D;
import java.util.LinkedList;

public class Line
{
	final static double SIZE_LIMIT = .5;
    double x1, y1, x2, y2, midX, midY, length, theta;
    boolean draw;
    
    public Line(double xStart, double yStart, double xEnd, double yEnd, boolean shouldDraw)
    {
        x1 = xStart;
        y1 = yStart;
        x2 = xEnd;
        y2 = yEnd;
        
        midX = (x1+x2)/2;
        midY = (y1+y2)/2;
        theta = Math.atan2(y2-y1, x2-x1);
        length = Math.hypot(x2-x1, y2-y1);
        
        draw = shouldDraw;
    }
    
    private void drawSelf(Graphics2D graphics)
    {
        if(draw)
        {
			graphics.draw((java.awt.Shape)(new java.awt.geom.Line2D.Double(x1, y1, x2, y2)));
        }
    }
    
    public void act(Graphics2D graphics, LinkedList lines)
    {
		drawSelf(graphics);
		if(length > SIZE_LIMIT)
		{
			iterate(lines);
		}
    }
    
    public void iterate(LinkedList lines)
    {
        double newX = midX + (length/2.0)*Math.cos(theta + Math.PI/3.0);
        double newY = midY + (length/2.0)*Math.sin(theta + Math.PI/3.0);
        
        lines.add(new Line(newX, newY, midX, midY, true));
        lines.add(new Line(x1, y1, midX, midY, false));
        lines.add(new Line(midX, midY, x2, y2, false));
    }
}
