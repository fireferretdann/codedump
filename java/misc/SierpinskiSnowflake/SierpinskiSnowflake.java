import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.Graphics2D;
import java.util.LinkedList;
import java.io.*;
import javax.imageio.ImageIO;

public class SierpinskiSnowflake
{
	final static int IMAGE_WIDTH = 10800;
	
	public static void main(String args[])
	{
		BufferedImage image = new BufferedImage(IMAGE_WIDTH, IMAGE_WIDTH, BufferedImage.TYPE_BYTE_BINARY);
		Graphics2D graphics = image.createGraphics();
		
		LinkedList<Line> lines = new LinkedList<Line>();
		
        double length = 3*IMAGE_WIDTH/4;
        double theta0 = -Math.PI/12;
        double x0 = IMAGE_WIDTH/16;
        double y0 = 9*IMAGE_WIDTH/24;
        double x1 = x0 + length * Math.cos(theta0);
        double y1 = y0 + length * Math.sin(theta0);
        double x2 = x1 + length * Math.cos(theta0 + 2*Math.PI/3);
        double y2 = y1 + length * Math.sin(theta0 + 2*Math.PI/3);
		
        lines.add(new OuterLine(x0, y0, x1, y1, true));
        lines.add(new OuterLine(x1, y1, x2, y2, true));
        lines.add(new OuterLine(x2, y2, x0, y0, true));
		
		while(!lines.isEmpty())
		{
			lines.poll().act(graphics, lines);
		}
		
		try
		{
			File outputfile = new File("saved.png");
			ImageIO.write(image, "png", outputfile);
		} catch (Exception e) { System.err.println("Whoopsie Poopsie");}
	}
}