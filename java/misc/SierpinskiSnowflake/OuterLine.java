import java.util.LinkedList;

public class OuterLine extends Line
{
    boolean newTriangle;
    
    public OuterLine(double xStart, double yStart, double xEnd, double yEnd, boolean isNewTri)
    {
        super(xStart, yStart, xEnd, yEnd, isNewTri);
        newTriangle = isNewTri;
    }
    
    public void iterate(LinkedList lines)
    {
        double newX = midX + (length/2.0)*Math.cos(theta + Math.PI/3.0);
        double newY = midY + (length/2.0)*Math.sin(theta + Math.PI/3.0);
        
        if(newTriangle)
        {
            lines.add(new Line(newX, newY, midX, midY, true));
            lines.add(new Line(x1, y1, midX, midY, false));
            lines.add(new Line(midX, midY, x2, y2, false));
        }
        
        double thirdX = (x1+x1+x2)/3;
        double thirdY = (y1+y1+y2)/3;
        newX = thirdX + (length/3)*Math.cos(theta - Math.PI/3);
        newY = thirdY + (length/3)*Math.sin(theta - Math.PI/3);
        double third2X = (x1+x2+x2)/3;
        double third2Y = (y1+y2+y2)/3;
        
        lines.add(new OuterLine(x1, y1, thirdX, thirdY, false));
        lines.add(new OuterLine(thirdX, thirdY, newX, newY, true));
        lines.add(new OuterLine(newX, newY, third2X, third2Y, true));
        lines.add(new OuterLine(third2X, third2Y, x2, y2, false));
        lines.add(new Line(third2X, third2Y, thirdX, thirdY, false));
    }
}
