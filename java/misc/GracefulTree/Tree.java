import java.util.LinkedList;

public class Tree
{
	private int value;
	private LinkedList<Tree> children;
	
	public static void main(String[] args)
	{
		Tree t = new Tree();
		for(int i = 1; i < 10; i ++)
		{
			t.addNodeRandomly(new Tree(i));
		}
		System.out.println(t);
	}
	
	public Tree()
	{
		value = 0;
		children = new LinkedList<Tree>();
	}
	
	public Tree(int v)
	{
		value = v;
		children = new LinkedList<Tree>();
	}
	
	public void addNode(Tree toAdd)
	{
		this.children.add(toAdd);
	}
	
	public int size()
	{
		int total = 1;
		for(Tree t : children)
			total += t.size();
		
		return total;
	}
	
	public int getValue()
	{
		return value;
	}
	
	public void addNodeRandomly(Tree toAdd)
	{
		// Probability of being uniformly chosen to be a child of this node
		double prob = 1.0/(this.size());
		double r = Math.random();
		double probsPassed = 0;
		
		if(r < prob)
		{
			addNode(toAdd);
		}
		else
		{
			probsPassed += prob;
			for(Tree t : children)
			{
				if(r - probsPassed < prob*t.size())
				{
					t.addNodeRandomly(toAdd);
					break;
				}
				else
				{
					probsPassed += prob*t.size();
				}
			}
		}
	}
	
	public String toString()
	{
		String s = "" + this.value;
		
		if(children.size() > 0)
		{
			s += "(";
			for(int i = 1; i < children.size(); i ++)
			{
				s += children.get(i).toString();
				s += ", ";
			}
			
			s += children.get(0);
			s += ")";
		}
		
		return s;
	}
}