import java.util.LinkedList;

public class GracefulTree extends Tree
{
	boolean isGraceful = false;
	
	public GracefulTree()
	{
		super();
	}
	
	public GracefulTree(int size)
	{
		super();
		
		for(int i = 1; i < size; i ++)
		{
			this.addNodeRandomly(new GracefulTree());
		}
	}
	
	public static void main(String[] args)
	{
		GracefulTree t = new GracefulTree(3);
		
		boolean grace = t.becomeGraceful();
	}
	
	public boolean isGraceful()
	{
		
	}
	
	public boolean becomeGraceful(GracefulTree root, LinkedList<Integer> odds)
	{
		if(odds.size() < this.size())
		{
			System.err.println("Not enough odds to go around");
			return false;
		}
		if(odds.size() == 1)
		{
			this.value = odds.get(0);
			
			return root.isGraceful();
		}
		else
		{
			for(int i = 0; i < odds.size(); i ++)
			{
				this.value = odds.remove(i);
				
				/////////////////////////////////////////////////////////////////////////////////////////////////////?/////////////
				
				odds.add(i, this.value);
			}
		}
	}
	
	public boolean becomeGraceful()
	{
		if(this.size() == 1)
		{
			this.value = 1;
			return true;
		}
		else
		{
			LinkedList<Integer> oddsRemaining = new LinkedList<Integer>();
			for(int i = 0; i < this.size(); i ++)
			{
				oddsRemaining.add(1 + 2*i);
			}
			
			return this.becomeGraceful(this, oddsRemaining);
		}
	}
}