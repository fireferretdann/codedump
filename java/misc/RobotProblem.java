import java.util.Scanner;

public class RobotProblem
{
	public static double probList[][] = {{1}, {.25, .25, .25, .25}};
	public static int maxRobots = 5000;
	
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		double currentProbs[] = new double[2];
		currentProbs[1] = 1;
		print(currentProbs);
		
		while(true)
		{
			currentProbs = iterate(currentProbs);
			print(currentProbs);
			
			//input.nextLine();
		}
	}
	
	
	public static double[] iterate(double[] oldProbs)
	{
		double[] newProbs = new double[(oldProbs.length-1)*3+1];
		if(newProbs.length > maxRobots)
			newProbs = new double[maxRobots];
		for(int numRobots = 0; numRobots < oldProbs.length; numRobots ++)
		{
			for(int nextCount = 0; nextCount < newProbs.length; nextCount ++)
			{
				newProbs[nextCount] += oldProbs[numRobots]*diceProb(numRobots, nextCount);
			}
		}
		
		return newProbs;
	}
	
	
	public static double diceProb(int currDice, int nextDice)
	{
		if(currDice < probList.length)
		{
			if(nextDice < probList[currDice].length)
				return probList[currDice][nextDice];
			else return 0;
		}
		else
		{
			generateRow();
			if(nextDice < probList[currDice].length)
				return probList[currDice][nextDice];
			else return 0;
		}
	}
	
	
	public static void generateRow()
	{
		double[][] newList = new double[probList.length+1][];
		
		for(int i = 0; i < probList.length; i ++)
			newList[i] = probList[i];
		
		double lastRow[] = probList[probList.length-1];
		double temp[] = new double[lastRow.length+3];
		
		for(int i = 0; i < temp.length; i++)
		{
			int b = i;
			if(b >= 0  &&  b < lastRow.length)
				temp[i] += .25*lastRow[b];
			
			b--;
			if(b >= 0  &&  b < lastRow.length)
				temp[i] += .25*lastRow[b];
			
			b--;
			if(b >= 0  &&  b < lastRow.length)
				temp[i] += .25*lastRow[b];
			
			b--;
			if(b >= 0  &&  b < lastRow.length)
				temp[i] += .25*lastRow[b];
			
		}
		//System.out.println("Printing temp...");
		//print(temp);

		newList[newList.length-1] = temp;
		probList = newList;
	}
	
	
	public static void print(double[] a)
	{
		/*for(int i = 0; i < a.length; i++)
		{
			System.out.println(i + ":\t" + a[i]);
		}*/
		
		System.out.println("Probability of extinction: " + a[0]);
	}
}
