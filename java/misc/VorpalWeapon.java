import java.util.Scanner;
import java.util.Random;

public class VorpalWeapon
{
	public static Random rand = new Random();
	
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		System.out.println("How many sides?");
		int sides = Integer.parseInt(input.nextLine());
		
		System.out.println("samples(s) or recursion(r)?");
		boolean isSamples = input.nextLine().contains("s");
		
		if(isSamples)
		{
			System.out.println("How many samples?");
			int samples = Integer.parseInt(input.nextLine());
			int total = 0;
			
			for(int i = 0; i < samples; i++)
			{
				double roll = doRoll(sides);
				total += roll;
				//System.out.println("You got a: " + roll);
				//System.out.println("The current average is: " + (double)total/(double)(i+1));
			}
			
			System.out.println("The final average is: " + (double)total/(double)samples);
		}
		else
		{
			double oldGuess = 0;
			double newGuess = triangleNumber(sides)/sides;
			
			while(oldGuess != newGuess)
			{
				oldGuess = newGuess;
				newGuess += newGuess/(double)sides;
				System.out.println(newGuess);
				input.nextLine();
			}
		}
		
		System.out.println("This was for " + sides + " sides.");
	}
	
	
	public static double triangleNumber(double n)
	{
		return n*(n+1)/2.0;
	}
	
	
	public static double doRoll(int sides)
	{
		int total = 0;
		int roll = sides; 
		
		//return rand.nextInt(sides) + 1;/*
		
		while(roll == sides)
		{
			roll = rand.nextInt(sides) + 1;
			total += roll;
		}
		
		return total;//*/
	}
}
