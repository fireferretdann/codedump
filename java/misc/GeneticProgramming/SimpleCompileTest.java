import javax.tools.*;

public class SimpleCompileTest
{
	public static void main(String[] args)
	{
		String fileToCompile = "MyClass.java";
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		int compilationResult =	compiler.run(null, null, null, fileToCompile);
		
		if(compilationResult == 0)
		{
			System.out.println("Compilation is successful");
			
			MyClass.myMethodThree();
		}
		else
		{
			System.out.println("Compilation Failed");
		}
	}
}
