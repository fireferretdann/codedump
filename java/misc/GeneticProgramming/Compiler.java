import javax.tools.*;

public class Compiler
{
	private static JavaCompiler internalCompiler = ToolProvider.getSystemJavaCompiler();
	
	public static void main(String[] args)
	{
		String fileToCompile = "MyClass.java";
		
		Compiler.compile(fileToCompile);
	}
	
	public static boolean compile(String fileToCompile)
	{
		if(internalCompiler.run(null, null, null, fileToCompile)  ==  0)
			return true;
		else
			System.err.println("Compilation of file \"" + fileToCompile + "\" failed.");
		return false;
	}
}
