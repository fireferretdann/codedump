import java.util.Scanner;

public class RandomWalk
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		while(true)
		{
			System.out.print("Please enter the number of walks to do: ");
			int m = input.nextInt();
			
			System.out.print("Please enter the number of steps to do: ");
			int n = input.nextInt();
			
			double total = 0;
			for(int i = 0; i < m; i++)
			{
				total += doWalk(n);
			}
			
			System.out.println("The average length was " + (total/m));
			System.out.println();
		}
	}
	
	public static double doWalk(int n)
	{
		int x = 0;
		int y = 0;
		for(int i = 0; i < n; i ++)
		{
			int dir = (int)(Math.random()*4);
			if(dir == 0)
				x ++;
			if(dir == 1)
				y ++;
			if(dir == 2)
				x --;
			if(dir == 3)
				y --;
		}
		
		return Math.sqrt(x*x + y*y);
	}
}
