import java.util.ArrayList;

public class PrimePrimeHarmonic
{
	public static ArrayList<Long> primes = startPrimes();
	
	public static void main(String[] args)
	{
		int termCount;
		if(args.length == 0)
		{
			termCount = 15;
		}
		else
		{
			termCount = Integer.parseInt(args[0]);
		}
		
		double sum = 0;
		for(int i = 1; i <= termCount; i++)
		{
			//System.out.println(i + ":\t" + p(p(i)));
			sum += 1.0/(double)p(i);
			if(i%(termCount/100) == 0)
			{
				System.out.println("After " + i + " terms: \t" + (sum - Math.log(Math.log(i))));
				System.out.println("Last prime: \t\t" + p(i));
				System.out.println("One over Last prime: \t" + (1.0/p(i)));
				System.out.println("Sum: \t\t\t" + sum);
				System.out.println("Log: \t\t\t" + Math.log(Math.log(i)));
				System.out.println("After " + i + " terms: \t" + (sum - Math.log(Math.log(i))));
				System.out.println();
			}
		}
		
		System.out.println((sum - Math.log(Math.log(termCount))));
		
		/*double sum = 0;
		for(int i = 1; i <= termCount; i++)
		{
			//System.out.println(i + ":\t" + p(p(i)));
			sum += 1.0/(double)p(p(i));
			if(i%(termCount/100) == 0)
			{
				System.out.println("After " + i + " terms: \t" + (sum - Math.log(Math.log(Math.log(i)))));
				System.out.println("Last prime: \t\t" + p(p(i)));
				System.out.println("One over Last prime: \t" + (1.0/p(p(i))));
				System.out.println("Sum: \t\t\t" + sum);
				System.out.println();
			}
		}
		
		System.out.println((sum - Math.log(Math.log(Math.log(termCount)))));*/
	}
	
	public static long p(long whichPrime)
	{
		whichPrime --; // Accounts for 0 based ArrayList
		
		if(primes.size() > whichPrime)
			return primes.get((int)whichPrime);
		
		long possPrime = primes.get(primes.size()-1);
		possPrime = possPrime + 6 - (1+possPrime)%6; // Gets next number such that  possPrime%6 == 5
		
		//System.out.println("The next Possible prime after " + primes.get(primes.size()-1) + " is " + possPrime);
		
		while(primes.size() <= whichPrime)
		{
			PrimeCheck p = new PrimeCheck();
			PrimeCheck p2 = new PrimeCheck();
			PrimeCheck p6 = new PrimeCheck();
			PrimeCheck p8 = new PrimeCheck();
			p.start(possPrime);
			p2.start(possPrime+2);
			p6.start(possPrime+6);
			p8.start(possPrime+8);
			
			while(p.isAlive()  ||  p2.isAlive()  ||  p6.isAlive()  ||  p8.isAlive());
			if(p.isPrime)
				addPrime(possPrime);
			if(p2.isPrime)
				addPrime(possPrime+2);
			if(p6.isPrime)
				addPrime(possPrime+6);
			if(p8.isPrime)
				addPrime(possPrime+8);
			
			possPrime += 12;
		}
		
		return primes.get((int)whichPrime);
	}
	
	public static void addPrime(long p)
	{
		if(!primes.contains(p))
		{
			if(p <= 0)
			{
				System.err.println("Daddy, can I add " + p + "?");
				System.err.println("Last valid prime: " + primes.get(primes.size()-1));
			}
			primes.add(p);
			//System.out.println("Added " + p);
		}
		else
			System.err.println("Tried to add " + p + " but already existed");
	}
	
	public static boolean isPrime(long possiblePrime)
	{
		for(long prime : primes)
		{
			//System.out.println("Checking " + possiblePrime + " % " + prime);
			if(possiblePrime % prime == 0)
				return false;
			
			if(prime * prime > possiblePrime)
				return true;
		}
		return true;
	}
	
	public static ArrayList<Long> startPrimes()
	{
		ArrayList<Long> p = new ArrayList<Long>();
		p.add(2l);
		p.add(3l);
		p.add(5l);
		p.add(7l);
		p.add(11l);
		p.add(13l);
		
		return p;
	}
}