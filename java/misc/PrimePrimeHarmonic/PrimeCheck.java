public class PrimeCheck extends Thread
{
	public long possPrime;
	public boolean isDone = false;
	public boolean isPrime = false;
	
	public void run()
	{
		try
		{
			for(long prime : PrimePrimeHarmonicBig.primes)
			{
					if(possPrime%prime == 0)
						break;
					else if(prime*prime > possPrime)
					{
						isPrime = true;
						break;
					}
			}
		}
		catch(java.util.ConcurrentModificationException e)
		{
			try{ sleep(100); } catch(Exception secondError) { System.err.println(secondError);}
			this.run();
		}
		
		isDone = true;
	}
	
	public void start(long toCheck)
	{
		possPrime = toCheck;
		super.start();	
	}
}