import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

public class PresidentialCountdown
{
	public static void main(String[] args)
	{
		// Inauguration time in seconds
		long inauguration = (new GregorianCalendar(2021, GregorianCalendar.JANUARY, 20, 12, 0)).getTimeInMillis()/1000;
		
		// Time in seconds (minus 1 so it updates immediately)
		long now = System.currentTimeMillis()/1000 - 1;
		//long oldnow = now;
		for(long n = Long.MIN_VALUE; n < Long.MAX_VALUE; n ++)
		{
			if(System.currentTimeMillis()/1000 > now)
			{
				now = System.currentTimeMillis()/1000;
				long diff = inauguration - now;
				
				long secs = diff%60;
				diff /= 60;
				long mins = diff%60;
				diff /= 60;
				long hours = diff%24;
				diff /= 24;
				long days = diff%365;
				diff /= 365;
				long years = diff;
				
				System.out.println("Freedom in " + years + " years, " + days + " days, " + hours + " hours, " + mins + " minutes, " + secs + " seconds.");
				//if(oldnow < now-1)
				//	System.out.println("\nSkipped a second there");
				//oldnow = now;
				
				try { TimeUnit.MILLISECONDS.sleep(999); } catch(Exception e) {}
			}
		}
	}
}