
public class SieveOfEratosthenes
{
	// All numbers up to limit marked with 0 as prime, 1 as composite, -1 as neither
	private int limit;
	private int[] numbers;
	private int perRow;
	
	
	public SieveOfEratosthenes(int upperLimit)
	{
		limit = upperLimit;
		numbers = new int[upperLimit+1];
		fillSieve();
		
		perRow = 5;
	}
	
	
	public SieveOfEratosthenes(int upperLimit, int elementsPerRow)
	{
		limit = upperLimit;
		numbers = new int[upperLimit+1];
		fillSieve();
		
		perRow = elementsPerRow;
	}
	
	public static void main(String[] args)
	{
		System.out.print("Please enter the upper bound for the sieve: ");
		int max = (new java.util.Scanner(System.in)).nextInt();
		
		System.out.print("Please enter how many numbers per row you want: ");
		int rows = (new java.util.Scanner(System.in)).nextInt();
		
		SieveOfEratosthenes sieve = new SieveOfEratosthenes(max, rows);
		
		System.out.println(sieve);
		
		System.out.println("\nOr just the primes:\n");
		
		sieve.printPrimes();
	}
	
	
	public void printPrimes()
	{
		String result = "";
		int count = 0;
		for(int i = 0; i < numbers.length; i++)
		{
			if(numbers[i] == 0)
			{
				result += i + "\t";
				count ++;
				if(count%perRow == 0)
					result += "\n";
			}
		}
		
		System.out.println(result);
	}
	
	
	public boolean isComposite(int toTest)
	{
		if(toTest < 0)
			return false;
		if(toTest >= numbers.length)
			return false;
		if(numbers[toTest] == 1)
			return true;
		return false;
	}
	
	
	public boolean isPrime(int toTest)
	{
		if(toTest < 0)
			return false;
		if(toTest >= numbers.length)
			return false;
		if(numbers[toTest] == 0)
			return true;
		return false;
	}
	
	
	private void fillSieve()
	{
		if(numbers.length > 0)
			numbers[0] = 1;
		if(numbers.length > 1)
			numbers[1] = -1;
		for(int current = 2; current < numbers.length; current ++)
		{
			if(numbers[current] == 0)
			{
				for(int composite = 2*current; composite < numbers.length; composite += current)
				{
					numbers[composite] = 1;
				}
			}
		}
	}
	
	public String toString()
	{
		String result = "";
		for(int i = 0; i < numbers.length; i++)
		{
			if(i%perRow == 0)
				result += "\n";
			String status = "n";
			if(numbers[i] == 0)
				status = "p";
			else if(numbers[i] == 1)
				status = "c";
			
			result += i + status + "\t";
		}
		return result;
	}
}
