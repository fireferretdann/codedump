
public class BotProbability
{
	//public static final double CHANGE = 1.5;
	public static final double CHANGE = 1.01;
	double[] moveWeights;
	int lastMove;
	
	public BotProbability(ConnectFour game)
	{
		moveWeights = new double[7];
		for(int i = 0; i < moveWeights.length; i ++)
			moveWeights[i] = 1;
		lastMove = -1;
		legalMoves(game);
		balance();
	}
	
	public BotProbability()
	{
		moveWeights = new double[7];
		for(int i = 0; i < moveWeights.length; i ++)
			moveWeights[i] = 1;
		balance();
		lastMove = -1;
	}
	
	public void legalMoves(ConnectFour game)
	{
		for(int i = 0; i < moveWeights.length; i++)
		{
			if(!game.isLegalMove(i))
				moveWeights[i] = 0;
		}
	}
	
	public int getMove()
	{
		double total = 0;
		double rand = Math.random();
		
		for(int i = 0; i < moveWeights.length; i ++)
		{
			total += moveWeights[i];
			if(rand < total)
			{
				lastMove = i;
				return i;
			}
		}
		
		//Something went wrong, but let's try again
		System.out.println("getMove broke");
		ConnectFour.input.nextLine();
		lastMove = getMove();
		return lastMove;
	}
	
	public void win()
	{
		if(lastMove != -1)
		{
			moveWeights[lastMove] *= CHANGE;
			balance();
		}
	}
	
	public void lose()
	{
		if(lastMove != -1)
		{
			moveWeights[lastMove] /= CHANGE;
			balance();
		}
	}
	
	public void balance()
	{
		double total = 0;
		for(double mw : moveWeights)
			total += mw;
		for(int i = 0; i < moveWeights.length; i ++)
			moveWeights[i] = moveWeights[i]/total;
	}
	
	public String toString()
	{
		String res = "";
		for(double prob : moveWeights)
		{
			res += prob + "\n";
		}
		return res;
	}
}
