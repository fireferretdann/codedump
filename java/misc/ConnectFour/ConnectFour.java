import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;

public class ConnectFour
{
	public LinkedList<Integer>[] board;
	public static Random rand = new Random();
	public static Scanner input = new Scanner(System.in);
	public int winner = -1;
	
	static long bot1Wins = 0;
	static long bot2Wins = 0;
	static boolean printing = false;
	
	public ConnectFour()
	{
		board = (LinkedList<Integer>[])(new LinkedList[7]);
		for(int i = 0; i < board.length; i++)
			board[i] = new LinkedList<Integer>();
	}
	
	public ConnectFour(ConnectFour toCopy)
	{
		board = (LinkedList<Integer>[])(new LinkedList[7]);
		for(int i = 0; i < board.length; i++)
			board[i] = (LinkedList<Integer>)toCopy.board[i].clone();
	}
	
	public ConnectFour(ConnectFour toCopy, boolean reverse)
	{
		if(reverse)
		{
			board = (LinkedList<Integer>[])(new LinkedList[7]);
			for(int i = 0; i < board.length; i++)
				board[6-i] = (LinkedList<Integer>)toCopy.board[i].clone();
		}
		else	
		{
			board = (LinkedList<Integer>[])(new LinkedList[7]);
			for(int i = 0; i < board.length; i++)
				board[i] = (LinkedList<Integer>)toCopy.board[i].clone();
		}
	}
	
	public static void main(String[] args)
	{
		Bot bot1 = new Bot();
		Bot bot2 = new Bot();
		bot2.playerID = 2;
		long gamesPlayed = 0;
		printing = true;
		
		while(true)
		{
			System.out.println("I've played " + gamesPlayed + " games so far");
			System.out.println("Ready to play");
			playHumanGame(bot1, false);
			gamesPlayed ++;
			playHumanGame(bot2, true);
			gamesPlayed ++;
			
			gamesPlayed += trainForTime(bot1, bot2);
			
			/*if(gamesPlayed%50000 == 0)
			{
				printing = true;
				System.out.println("Ready to play");
				playHumanGame(bot1, false);
				gamesPlayed ++;
				playHumanGame(bot2, true);
				gamesPlayed ++;
				System.out.println("Training...");
				printing = false;
			}
			else
			{
				botGame(bot1, bot2);
				
				gamesPlayed ++;
			}*/
		}
	}
	
	public static long trainForTime(Bot bot1, Bot bot2)
	{
		boolean printingWas = printing;
		printing = false;
		System.out.println("How long should I train for, in seconds?");
		long time = 1000*input.nextLong();
		System.out.println("Training...");
		
		long gameCount = 0;
		long start = System.currentTimeMillis();
		while(System.currentTimeMillis() < start + time)
		{
			botGame(bot1, bot2);
			botGame(bot2, bot1);
			gameCount += 4; // One per bot, per game
		}
		
		printing = printingWas;
		return gameCount;
	}
	
	public static void botGame(Bot bot1, Bot bot2)
	{
		ConnectFour game = new ConnectFour();
		bot1.newGame(game);
		bot2.newGame(game);
		
		while(!(game.isGameWon() || game.isGameDraw()))
		{
			if(printing)
			{
				System.out.println(game);
				System.out.println(bot1.positions.get(game.toString()));
			}
			game.move(bot1.move(), 1);
			
			if(!(game.isGameWon() || game.isGameDraw()))
			{
				if(printing)
				{
					System.out.println(game);
					System.out.println(bot2.positions.get(game.toString()));
				}
				game.move(bot2.move(), 2);
			}
			
		}
		
		//System.out.println(game);
		
		if(game.winner == 1)
		{
			bot1.win();
			bot2.lose();
			bot1Wins++;
			if(printing)
				System.out.println("Bot1 wins! Ratio = " + bot1Wins/((double)bot1Wins+bot2Wins));
		}
		else if(game.winner == 2)
		{
			bot1.lose();
			bot2.win();
			bot2Wins++;
			if(printing)
				System.out.println("Bot2 wins! Ratio = " + bot2Wins/((double)bot1Wins+bot2Wins));
		}
		//System.out.println("--------------\n");
	}
	
	public static void playHumanGame(Bot bot, boolean humanFirst)
	{
		ConnectFour game = new ConnectFour();
		bot.newGame(game);
		System.out.println(game);
		int h = 2;
		int b = 1;
		
		if(humanFirst)
		{
			h = 1;
			b = 2;
			game.move(humanMove(), h);
		}
		
		while(!(game.isGameWon() || game.isGameDraw()))
		{
			game.move(bot.move(), b);
			
			System.out.println(game);
			
			if(!(game.isGameWon() || game.isGameDraw()))
			{
				game.move(humanMove(), h);
			}
		}
		
		if(game.winner == b)
		{
			for(int i = 0; i < 10; i ++)
				bot.win();
			System.out.println("Computer wins! :P");
		}
		else if(game.winner == h)
		{
			for(int i = 0; i < 10; i ++)
				bot.lose();
			System.out.println("Human wins! :(");
		}
	}
	
	public static int humanMove()
	{
		return input.nextInt();
		//return Integer.parseInt(input.nextLine());
	}
	
	public boolean isGameWon()
	{
		//Check up-down wins
		for(int col = 0; col < 7; col ++)
		{
			LinkedList temp = board[col];
			for(int row = 0; row < temp.size()-3; row ++)
			{
				if(temp.get(row) == temp.get(row+1) && temp.get(row) == temp.get(row+2) && temp.get(row) == temp.get(row+3))
				{
					//System.out.println("Player " + temp.get(row) + " wins!");
					winner = (Integer)temp.get(row);
					return true;
				}
			}
		}
		//Check left-right wins
		for(int row = 0; row < 6; row ++)
		{
			for(int col = 0; col < 4; col ++)
			{
				if(board[col].size() > row)
				{
					if(board[col+1].size() > row && board[col+1].get(row) == board[col].get(row))
					{
						if(board[col+2].size() > row && board[col+2].get(row) == board[col].get(row))
						{
							if(board[col+3].size() > row && board[col+3].get(row) == board[col].get(row))
							{
								//System.out.println("Player " + board[col].get(row) + " wins!");
								winner = board[col].get(row);
								return true;
							} 
						} 
					} 
				}
			}
		}
		//Check up-right wins
		for(int row = 0; row < 6; row ++)
		{
			for(int col = 0; col < 4; col ++)
			{
				if(board[col].size() > row)
				{
					if(board[col+1].size() > row+1 && board[col+1].get(row+1) == board[col].get(row))
					{
						if(board[col+2].size() > row+2 && board[col+2].get(row+2) == board[col].get(row))
						{
							if(board[col+3].size() > row+3 && board[col+3].get(row+3) == board[col].get(row))
							{
								//System.out.println("Player " + board[col].get(row) + " wins!");
								winner = board[col].get(row);
								return true;
							} 
						} 
					} 
				}
			}
		}
		//Check up-left wins
		for(int row = 0; row < 6; row ++)
		{
			for(int col = 3; col < 7; col ++)
			{
				if(board[col].size() > row)
				{
					if(board[col-1].size() > row+1 && board[col-1].get(row+1) == board[col].get(row))
					{
						if(board[col-2].size() > row+2 && board[col-2].get(row+2) == board[col].get(row))
						{
							if(board[col-3].size() > row+3 && board[col-3].get(row+3) == board[col].get(row))
							{
								//System.out.println("Player " + board[col].get(row) + " wins!");
								winner = board[col].get(row);
								return true;
							} 
						} 
					} 
				}
			}
		}
		return false;
	}
	
	public boolean isGameDraw()
	{
		for(LinkedList<Integer> col : board)
		{
			if(col.size() < 6)
				return false;
		}
		//System.out.println("Game is a draw!");
		return true;
	}
	
	// True if game should continue, false if game is over
	public boolean move(int col, int player)
	{
		board[col].add(player);
		
		if(board[col].size() > 6)
			return false;
		
		if(isGameWon())
			return false;
		
		return true;
	}
	
	public boolean isLegalMove(int col)
	{
		return col >= 0  &&  col < board.length  &&  board[col].size()<6;
	}
	
	public void print()
	{
		System.out.println(this);
	}
	
	public String toString()
	{
		String s = "";
		for(int row = 5; row >= 0; row --)
		{
			s += "|";
			for(int col = 0; col < 7; col ++)
			{
				if(row >= board[col].size())
					s += " ";
				else
					s += board[col].get(row);
				s += "|";
			}
			s += '\n';
		}
		
		s += "===============\n";
		s += "|0|1|2|3|4|5|6|\n";
		
		return s;
	}
	
	public String toReverseString()
	{
		String s = "";
		for(int row = 5; row >= 0; row --)
		{
			s += "|";
			for(int col = 6; col >=0; col --)
			{
				if(row >= board[col].size())
					s += " ";
				else
					s += board[col].get(row);
				s += "|";
			}
			s += '\n';
		}
		
		s += "===============\n";
		s += "|0|1|2|3|4|5|6|\n";
		
		return s;
	}
}
