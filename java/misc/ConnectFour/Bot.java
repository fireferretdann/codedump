import java.util.LinkedList;
import java.util.HashMap;

public class Bot
{
	static HashMap<String,BotProbability> positions = new HashMap<String,BotProbability>();
	int playerID;
	ConnectFour game;
	LinkedList<String> positionsThisGame;
	LinkedList<String> positionsThisGameReverse;
	final static boolean CAN_WIN_ENABLED = false;
	
	public Bot()
	{
		playerID = 1;
		game = null;
		positionsThisGame = null;
		positionsThisGameReverse = null;
	}
	
	public void newGame(ConnectFour g)
	{
		game = g;
		positionsThisGame = new LinkedList<String>();
		positionsThisGameReverse = new LinkedList<String>();
	}
	
	public int winningMove()
	{
		for(int col = 0; col < 7; col++)
		{
			if(game.isLegalMove(col))
			{
				ConnectFour test = new ConnectFour(game);
				test.move(col, playerID);
				if(test.isGameWon())
				return col;
			}
		}
		
		return -1;
	}
	
	public void win()
	{
		for(int i = 0; i < positionsThisGame.size(); i ++)
		{
			String pos = positionsThisGame.get(i);
			String posR = positionsThisGameReverse.get(i);
			
			positions.get(pos).win();
			positions.get(posR).lastMove = 6-positions.get(pos).lastMove;
			positions.get(posR).win();
		}
	}
	
	public void lose()
	{
		for(int i = 0; i < positionsThisGame.size(); i ++)
		{
			String pos = positionsThisGame.get(i);
			String posR = positionsThisGameReverse.get(i);
			
			positions.get(pos).lose();
			positions.get(posR).lastMove = 6-positions.get(pos).lastMove;
			positions.get(posR).lose();
		}
	}
	
	public int move()
	{
		if(CAN_WIN_ENABLED  &&  canWin())
		{
			return winningMove();
		}
		else
		{
			if(!positions.containsKey(game.toString()))
			{
				positions.put(game.toString(), new BotProbability(game));
				positions.put(game.toReverseString(), new BotProbability(new ConnectFour(game, true)));
			}
			
			positionsThisGame.add(game.toString());
			positionsThisGameReverse.add(game.toReverseString());
			
			int col = positions.get(game.toString()).getMove();
			
			while(!game.isLegalMove(col))
			{
				col = positions.get(game.toString()).getMove();
				//System.out.println(game);
				//System.out.println(col + " is an illegal move");
				ConnectFour.input.nextLine();
			}
			
			//System.out.println("Computer Move: " + col);
			return col;
		}
	}
	
	public boolean canWin()
	{
		return winningMove() != -1;
	}
	
	public static void main(String[] args)
	{
		
	}
}
