import java.util.Random;

public class TrueRandom
{
	public static void main(String[] args)
	{
		long start;
		long end;
		start = System.currentTimeMillis();
		
		Random rand = new Random();
		int i = rand.nextInt();
		for(long l = 0; l < 1000000000; l++)
			i = rand.nextInt();
			//System.out.write(rand.nextInt());
		
		end = System.currentTimeMillis();
		System.out.println("It took " + (end-start) + " ms");
	}
}
