// Use -Xmx8g flag to increase heap size

import java.util.HashSet;

public class Randomness
{
	long value;
	long prev1;
	long prev2;
	long prev3;
	long prev4;
	
	public Randomness(long seed)
	{
		value = seed;
		prev1 = 0;
		prev2 = 0;
		prev3 = 0;
		prev4 = 0;
		for(int i = 0; i < 5; i ++)
			run();
	}
	public Randomness()
	{
		value = System.currentTimeMillis();
		prev1 = 0;
		prev2 = 0;
		prev3 = 0;
		prev4 = 0;
		for(int i = 0; i < 5; i ++)
			run();
	}
	
	public static void main(String[] args)
	{
		long endTime;
		long startTime;
		startTime = System.currentTimeMillis();
		
		Randomness rng = new Randomness();
		
		long end = 1000000000;
		long count = 0;
		int curr = rng.getInt();
		
		while(count < end)
		{
			count ++;
			curr = rng.getInt();
			//System.out.write(curr);
		}
		
		endTime = System.currentTimeMillis();
		System.out.println("It took " + (endTime-startTime) + " ms");
	}
	
	
	public long calc()
	{
		long seed = value;
		seed = ((seed<<32)>>32) | ((seed>>32)<<32);
		long temp1 = seed^8675309;
		temp1 ^= prev1;
		temp1 ^= prev2;
		temp1 ^= prev3;
		temp1 ^= prev4;
		
		long temp2 = 1234567891+seed<<4;
		temp2 *= 1+temp2;
		temp2 *= (1-prev3)+prev2*prev4;
		
		long temp3 = temp1^temp2;
		temp3 += 314159;
		temp3 |= temp3-prev1-temp2-seed;
		temp3 = temp3>>>prev3;
		
		long temp4 = 9001 + prev4>>3;
		temp4 += prev3>>4;
		temp4 -= 5;
		
		seed += temp1;
		seed -= temp2;
		seed ^= temp3;
		seed += temp4;
		return seed;
	}
	
	
	public void run()
	{
		prev4 = prev3;
		prev3 = prev2;
		prev2 = prev1;
		prev1 = value;
		value = calc();
		
	}
	
	public long getLong()
	{
		run();
		return value;
	}
	
	public int getInt()
	{
		run();
		return (int)value;
	}
	
	public long getLong(long max)
	{
		run();
		return Math.abs(value)%max;
	}
	
	public int getInt(int max)
	{
		run();
		return Math.abs((int)value)%max;
	}
}

