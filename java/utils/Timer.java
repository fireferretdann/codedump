import java.util.Scanner;
import java.text.DateFormat;
import java.util.Date;
import java.text.SimpleDateFormat;

public class Timer
{
	static Scanner input = new Scanner(System.in);
	
	long start;
	long end;
	
	public Timer()
	{
		start = 0;
		end = 0;
	}
	
	public static void main(String[] args)
	{
		while(true)
		{
			Timer t = new Timer();
			System.out.print("Ready! Press enter to start!");
			input.nextLine();
			t.start = now();
			input.nextLine();
			t.end = now();
			
			System.out.println(t.formattedTime() + '\n');
		}
	}
	
	public String formattedTime()
	{
		long time = time();
		DateFormat formatter = new SimpleDateFormat("HH:mm:ss.SSS");
		
		if(time/1000 == 0)
			formatter = new SimpleDateFormat(".SSS");
		else if(time/1000/60 == 0)
			formatter = new SimpleDateFormat("ss.SSS");
		else if(time/1000/60/60 == 0)
			formatter = new SimpleDateFormat("mm:ss.SSS");
		
		Date date = new Date(time() + 5*60*60*1000);
		
		String out = formatter.format(date);
		
		time = time/1000/60/60/24;
		if(time != 0)
		{
			out = time + ":" + out;
		}
		
		return out;
	}
	
	public long time()
	{
		return end - start;
	}
	
	private static long now()
	{
		return System.currentTimeMillis();
	}
}
