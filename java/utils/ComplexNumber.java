import java.lang.Math;

/**
* This class handles complex numbers and their interactions.
* It can use a + b*i notation or r*e^(i*theta) notation with double precision.
*/
public class ComplexNumber
{
	private double a;
	private double b;
	private double r;
	private double theta;
	private boolean lastUsedIsPolar;
	public static boolean POLAR = true;
	public static boolean CARTESIAN = false;
	public static ComplexNumber I = new ComplexNumber(0, 1, false);
	public static ComplexNumber E = new ComplexNumber(Math.E, 0, false);
	public static ComplexNumber PI = new ComplexNumber(Math.PI, 0, false);
	
	/**
	* This constructs a complex number of 0.
	*/
	public ComplexNumber()
	{
		a = 0;
		b = 0;
		r = 0;
		theta = 0;
		lastUsedIsPolar = false;
	}
	
	public ComplexNumber(double AorR, double BOrtheta, boolean isPolar)
	{
		lastUsedIsPolar = isPolar;
		if(lastUsedIsPolar == POLAR)
		{
			r = AorR;
			theta = BOrtheta;
		}
		else
		{
			a = AorR;
			b = BOrtheta;
		}
		update();
	}
	
	public ComplexNumber(String number)
	{
		ComplexNumber me = new ComplexNumber();
		number = number.replaceAll("\\s", "");
		char[] chars = number.toCharArray();
		
		if(number.equals("i"))
			me.b = 1;
		else if(number.equals("e"))
			me.a = Math.E;
		else if(number.equals("pi"))
			me.a = Math.PI;
		else if(number.contains("+"))
		{
			ComplexNumber before = new ComplexNumber(number.substring(0, number.indexOf("+")));
			ComplexNumber after = new ComplexNumber(number.substring(number.indexOf("+")+1));
			me = before.add(after);
		}
		else if(number.contains("-"))
		{
			ComplexNumber before = new ComplexNumber(number.substring(0, number.indexOf("-")));
			ComplexNumber after = new ComplexNumber(number.substring(number.indexOf("-")+1));
			me = before.subtract(after);
		}
		else if(number.contains("*"))
		{
			ComplexNumber before = new ComplexNumber(number.substring(0, number.indexOf("*")));
			ComplexNumber after = new ComplexNumber(number.substring(number.indexOf("*")+1));
			me = before.multiply(after);
		}
		else if(number.contains("/"))
		{
			ComplexNumber before = new ComplexNumber(number.substring(0, number.indexOf("/")));
			ComplexNumber after = new ComplexNumber(number.substring(number.indexOf("/")+1));
			me = before.divide(after);
		}
		else if(number.contains("^"))
		{
			ComplexNumber before = new ComplexNumber(number.substring(0, number.indexOf("^")));
			ComplexNumber after = new ComplexNumber(number.substring(number.indexOf("^")+1));
			me = before.power(after);
		}
		else if(number.matches(".+pi$"))
			me = PI.multiply(new ComplexNumber(number.substring(0, number.length()-2)));
		else if(number.matches("^pi.+"))
			me =PI.multiply(new ComplexNumber((number.substring(2))));
		else if(number.matches(".+i$"))
			me = I.multiply(new ComplexNumber(number.substring(0, number.length()-1)));
		else if(number.matches("^i.+"))
			me =I.multiply(new ComplexNumber((number.substring(1))));
		else if(number.matches(".+e$"))
			me = E.multiply(new ComplexNumber(number.substring(0, number.length()-1)));
		else if(number.matches("^e.+"))
			me =E.multiply(new ComplexNumber((number.substring(1))));
		else if(number.contains("pi"))
		{
			ComplexNumber before = new ComplexNumber(number.substring(0, number.indexOf("pi")));
			ComplexNumber after = new ComplexNumber(number.substring(number.indexOf("pi")+2));
			me = before.multiply(after).multiply(PI);
		}
		else if(number.contains("i"))
		{
			ComplexNumber before = new ComplexNumber(number.substring(0, number.indexOf("i")));
			ComplexNumber after = new ComplexNumber(number.substring(number.indexOf("i")+1));
			me = before.multiply(after).multiply(I);
		}
		else if(number.contains("e"))
		{
			ComplexNumber before = new ComplexNumber(number.substring(0, number.indexOf("i")));
			ComplexNumber after = new ComplexNumber(number.substring(number.indexOf("i")+1));
			me = before.multiply(after).multiply(E);
		}
		else
		{
			if(number.length() > 0)
				me.a = Double.parseDouble(number);
		}
		
		a = me.a;
		b = me.b;
		lastUsedIsPolar = CARTESIAN;
		update();
	}
	
	private void update()
	{
		if(lastUsedIsPolar)
		{
			a = r * Math.cos(theta);
			b = r * Math.sin(theta);
		}
		else
		{
			r = Math.hypot(a, b);
			theta = Math.atan2(b, a);
		}
		
		theta += 2*Math.PI;
		theta %= 2*Math.PI;
	}
	
	public ComplexNumber add(ComplexNumber other)
	{
		return new ComplexNumber(a + other.a, b + other.b, CARTESIAN);
	}
	
	public ComplexNumber subtract(ComplexNumber other)
	{
		return new ComplexNumber(a - other.a, b - other.b, CARTESIAN);
	}
	
	public ComplexNumber multiply(ComplexNumber other)
	{
		return new ComplexNumber(r * other.r, theta + other.theta, POLAR);
	}
	
	public ComplexNumber divide(ComplexNumber other)
	{
		return new ComplexNumber(r / other.r, theta - other.theta, POLAR);
	}
	
	public ComplexNumber power(ComplexNumber other)
	{
		return new ComplexNumber(Math.pow(r, other.a) / (Math.pow(Math.E, other.b*theta)), other.b*Math.log(r) + theta*other.a, POLAR);
	}
	
	public double absDouble()
	{
		return Math.abs(r);
	}
	
	/**
	@override
	*/
	public String toString()
	{
		if(lastUsedIsPolar)
		{
			return "" + r + " e^ (i" + theta + ")";
		}
		else
		{
			return "" + a + " + " + b + "i";
		}
	}
	
	public boolean equals(Object other)
	{
		if(other instanceof ComplexNumber)
			return (this==null  ?  other==null  :  (this.a == ((ComplexNumber)other).a && this.b == ((ComplexNumber)other).b));
		else
			return false;
	}
	
	public int hashCode()
	{
		return 0;
	}
	
	public ComplexNumber clone()
	{
		if(this.lastUsedIsPolar)
			return new ComplexNumber(this.r, this.theta, this.lastUsedIsPolar);
		else
			return new ComplexNumber(this.a, this.b, this.lastUsedIsPolar);
	}
	
	public static void main(String[] args)
	{
		ComplexNumber a = new ComplexNumber();
		ComplexNumber b = new ComplexNumber();
		System.out.println(a.equals(b));
		System.out.println(a == b);
		
		System.out.println(a.hashCode());
		System.out.println(b.hashCode());
		
		java.util.HashSet<ComplexNumber> set = new java.util.HashSet<ComplexNumber>();
		
		set.add(a);
		System.out.println(set.contains(b));
		
		System.out.println(((Object)a).equals((Object)b));
	}
}
