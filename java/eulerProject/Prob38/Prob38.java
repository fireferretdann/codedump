public class Prob38
{
	public static void main(String[] args)
	{
		for(int toTest = 987654321; toTest >= 123456789; toTest --)
		{
			if(isPandigital(toTest))
			{
				if(isConcatProduct(toTest))
				{
					System.out.println(toTest);
					System.exit(0);
				}
				System.out.println("Tried the panDigital: " + toTest);
			}
		}
	}
	
	public static boolean isConcatProduct(int n)
	{
		String nString = "" + n;
		for(int initialDigits = 1; initialDigits <= 4; initialDigits ++)
		{
			int mutiplicand = Integer.parseInt(nString.substring(0, initialDigits));
			
			int concatProduct = 0;
			int numConcats = 0;
			while(concatProduct < n)
			{
				numConcats ++;
				try
				{
					concatProduct = Integer.parseInt(concatProduct + "" + (numConcats*mutiplicand));
				}
				catch(NumberFormatException e)
				{
					concatProduct = n+1; // escapes while loop
				}
				
				if(concatProduct == n)
					return true;
			}
		}
		
		return false;
	}
	
	public static boolean isPandigital(int n)
	{
		if(n < 123456789  ||  n > 987654321)
			return false;
		
		boolean[] digitsPresent = new boolean[9];
		for(int p = 8; p >= 0; p --)
		{
			int digitMinusOne = n / (int)Math.pow(10, p) - 1;
			if(digitMinusOne == -1)
			{
				return false;
			}
			else if(digitsPresent[digitMinusOne])
				return false;
			else
				digitsPresent[digitMinusOne] = true;
			n %= Math.pow(10, p)*(digitMinusOne+1);
		}
		return true;
	}
}