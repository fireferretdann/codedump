import java.math.BigInteger;

public class Prob48
{
	public static void main(String[] args)
	{
		BigInteger sum = new BigInteger("0");
		BigInteger tenDigits = new BigInteger("10000000000");
		BigInteger n;
		for(int i = 1; i <= 1000; i ++)
		{
			n = new BigInteger("" + i);
			
			sum = sum.add(n.modPow(n, tenDigits));
		}
		
		System.out.println(sum);
	}
}