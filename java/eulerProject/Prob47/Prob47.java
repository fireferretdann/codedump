import java.util.ArrayList;

public class Prob47
{
	public static ArrayList<Integer> primes = new ArrayList<Integer>();
	
	public static void main(String[] args)
	{
		int i = 643; // Start at solution for three consecutive numbers
		
		boolean cont = true;
		while(cont)
		{
			i ++;
			if(countPrimeFactors(i) == 4
							&& countPrimeFactors(i+1) == 4
							&& countPrimeFactors(i+2) == 4
							&& countPrimeFactors(i+3) == 4)
				cont = false;
		}
		
		System.out.println(i);
	}
	
	public static int countPrimeFactors(int n)
	{
		
		int count = 0;
		
		if(n%2 == 0)
		{
			count ++;
			while(n%2 == 0)
				n /= 2;
		}
		if(n%3 == 0)
		{
			count ++;
			while(n%3 == 0)
				n /= 3;
		}
		
		for(int p = 5; p*p <= n; p += 6)
		{
			if(n%p == 0)
			{
				count ++;
				while(n%p == 0)
				{
					n /= p;
				}
			}
			if(n%(p+2) == 0)
			{
				count ++;
				while(n%(p+2) == 0)
				{
					n /= p+2;
				}
			}
		}
		
		if(n > 1)
			count ++;
		
		return count;
	}
}