//import java.math.BigInteger;

public class Prob53
{
	public static void main(String[] args)
	{
		int count = 0;
		
		for(int n = 1; n <= 100; n ++)
		{
			for(int r = 1; r < n; r++)
			{
				if(choose(n, r) > 1000000)
					count ++;
			}
		}
		
		System.out.println(count);
	}
	
	public static double choose(int n, int r)
	{
		if(r == 0  ||  r == n)
			return 1.0;
		
		double result = 1;
		
		for(int i = 2; i <= n; i ++)
		{
			result *= i;
			if(i <= r)
				result /= i;
			if(i <= n-r)
				result /= i;
		}
		
		return result;
	}
}