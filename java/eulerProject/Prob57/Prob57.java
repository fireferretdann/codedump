import java.math.BigInteger;

public class Prob57
{
	public static void main(String[] args)
	{
		BigInteger denom = new BigInteger("1");
		BigInteger numer = new BigInteger("1");
		
		int count = 0;
		for(int i = 0; i < 1000; i ++)
		{	// Using the facts that D(n) = D(n-1) + N(n-1) and N(n) = D(n) + D(n-1)
			BigInteger lastDenom = new BigInteger(denom.toString());
			denom = lastDenom.add(numer);
			numer = denom.add(lastDenom);
			
			System.out.println(numer + "/" + denom);
			
			if(numer.toString().toCharArray().length > denom.toString().toCharArray().length)
				count ++;
		}
		
		System.out.println(count);
	}
}