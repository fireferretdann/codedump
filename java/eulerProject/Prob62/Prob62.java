//import java.math.BigInteger;
import java.util.Arrays;
import java.util.TreeSet;

// Alternative method: generate long list of cubes, look for 5 permutations in that list, removing all visited as you search
// Search can stop after the numbers reach anohter digit
public class Prob62
{
	public static int GOAL = 1;
	public static boolean found = false;
	public static TreeSet<Long> cubes = new TreeSet<Long>();
	
	public static void main(String[] args)
	{
		generateCubes();
		
		while(!cubes.isEmpty())
		{
			long toCheck = cubes.pollFirst();
			int permsCounted = 1;
			
			long[] toRemove = new long[2*GOAL];
			int pos = 0;
			
			long stopper = (long)Math.pow(10, 1+(int)Math.log10(toCheck));
			for(long possiblePerm : cubes)
			{
				if(possiblePerm >= stopper)
					break;
				
				if(arePerms(toCheck, possiblePerm))
				{
					toRemove[pos++] = possiblePerm;
					permsCounted ++;
				}
			}
			
			if(permsCounted >= GOAL)
			{
				System.out.println(permsCounted + ": \t" + toCheck);
				GOAL = permsCounted+1;
			}
			
			for(long removeMe : toRemove)
			{
				cubes.remove(removeMe);
			}
		}
	}
	
	public static boolean arePerms(long a, long b)
	{
		char[] aArray = ("" + a).toCharArray();
		char[] bArray = ("" + b).toCharArray();
		Arrays.sort(aArray);
		Arrays.sort(bArray);
		return Arrays.equals(aArray, bArray);
	}
	
	public static void generateCubes()
	{
		for(int i = 1; i < 2097151; i++)
		{
			cubes.add(cube(i));
		}
		System.out.println("Cubes generated\n");
	}
	
	public static long cube(long n)
	{
		return n*n*n;
	}
	
	public static void countUniqueCubes(TreeSet<Long> perms, long min)
	{
		
		long cubes[] = new long[5];
		//for(int i = 0; i < cubes.length; i ++)
		//	cubes[i] = 0;
		
		int pos = 0;
		
		for(long i : perms)
		{
			if(i >= min  &&  /*!contains(cubes, i) &&*/  isCube(i)) //Compare removes permutations starting with 0
			{
				//System.out.println("Adding cube: " + i);
				cubes[pos++] = i;
			}
		}
		
		String s = "Found Them!\n\n";
		for(long i : cubes)
		{
			if(i == 0)
				return;
			s += i + "\n";
		}
		found = true;
		System.out.println(s + "\n\n");
	}
	
	public static boolean contains(long[] a, long e)
	{
		for(long i : a)
			if(i == e)
				return true;
		return false;
	}
	
	public static boolean contains(TreeSet<Long> set, long e)
	{
		return set.contains(e);
	}
	
	public static boolean isCube(long n)
	{
		return Math.cbrt(n)%1.0 == 0;
		/*cubeRoot = Math.floor(cubeRoot);
		String formattedRoot = "" + cubeRoot;
		formattedRoot = formattedRoot.substring(0, formattedRoot.length()-2);
		long rootGuess = new long("" + formattedRoot);
		return rootGuess.pow(3).equals(n);*/
	}
	
	public static TreeSet<Long> permutations(long n)
	{
		String s = "" + n;
		int digitCount = s.length();
		String[] digits = new String[digitCount];
		for(int i = 0; i < digitCount; i ++)
			digits[i] = "" + s.charAt(i);
		
		TreeSet<Long> perms = new TreeSet<Long>();
		
		// Heap's algorithm adapted from wikipedia's pseudocode:
		int[] c = new int[digitCount];

		perms.add(n);
		
		int i = 0;
		while(i < digitCount)
		{
			if(c[i] < i)
			{
				if(i%2 == 0)
					swap(digits, 0, i);
				else
					swap(digits, c[i], i);
				perms.add(getLong(digits));
				c[i] += 1;
				i = 0;
			}
			else
			{
				c[i] = 0;
				i += 1;
			}
		}
		// End of Heap's algorithm
		
		return perms;
	}
	
	public static int factorial(int n)
	{
		int result = 1;
		for(int i = 2; i <= n; i ++)
			result *= i;
		return result;
	}
	
	public static void swap(String[] a, int i, int j)
	{
		String temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}
	
	public static long getLong(String[] digits)
	{
		String n = "";
		for(String d : digits)
			n += d;
		
		return Long.parseLong(n);
	}
}