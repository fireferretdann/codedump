public enum Rank
{
	TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE;
	
	public static Rank numToRank(int r)
	{
		switch(r)
		{
			case(0):
				return TWO;
			case(1):
				return THREE;
			case(2):
				return FOUR;
			case(3):
				return FIVE;
			case(4):
				return SIX;
			case(5):
				return SEVEN;
			case(6):
				return EIGHT;
			case(7):
				return NINE;
			case(8):
				return TEN;
			case(9):
				return JACK;
			case(10):
				return QUEEN;
			case(11):
				return KING;
			case(12):
				return ACE;
			default:
				System.err.println("bad rank num: " + r);
				System.exit(123);
		}
		return TWO; //Should not be reached
	}
	
	public static int rankToNum(Rank r)
	{
		switch(r)
		{
			case TWO:
				return 0;
			case THREE:
				return 1;
			case FOUR:
				return 2;
			case FIVE:
				return 3;
			case SIX:
				return 4;
			case SEVEN:
				return 5;
			case NINE:
				return 6;
			case EIGHT:
				return 7;
			case TEN:
				return 8;
			case JACK:
				return 9;
			case QUEEN:
				return 10;
			case KING:
				return 11;
			case ACE:
				return 12;
		}
		return -1; //Should not be reached
	}
	
	public static Rank stringToRank(String r)
	{
		r = r.toLowerCase();
		
		switch (r)
		{
			case("2"):
				return TWO;
			case("3"):
				return THREE;
			case("4"):
				return FOUR;
			case("5"):
				return FIVE;
			case("6"):
				return SIX;
			case("7"):
				return SEVEN;
			case("8"):
				return EIGHT;
			case("9"):
				return NINE;
			case("10"):
				return TEN;
			case("t"):
				return TEN;
			case("j"):
				return JACK;
			case("q"):
				return QUEEN;
			case("k"):
				return KING;
			case("a"):
				return ACE;
			default:
				System.err.println("invalid rank: " + r);
				System.exit(9001);
		}
		return TWO;
	}
	
	public String toString()
	{
		switch(this)
		{
			case TWO:
				return "2";
			case THREE:
				return "3";
			case FOUR:
				return "4";
			case FIVE:
				return "5";
			case SIX:
				return "6";
			case SEVEN:
				return "7";
			case EIGHT:
				return "8";
			case NINE:
				return "9";
			case TEN:
				return "10";
			case JACK:
				return "J";
			case QUEEN:
				return "Q";
			case KING:
				return "K";
			case ACE:
				return "A";
		}
		return "-1"; //Should not be reached
	}
}
