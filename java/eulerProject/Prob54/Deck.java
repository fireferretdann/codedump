import java.util.LinkedList;
import java.util.Random;

public class Deck
{
	final private static Random rand = new Random();
	LinkedList<Card> cards = new LinkedList<Card>();
	
	public Deck()
	{
		for(Suit s : Suit.values())
			for(Rank r : Rank.values())
				cards.add(new Card(r, s));
		
		cards = shuffle(cards);
	}
	
	public Deck(Deck toCopy)
	{
		cards = (LinkedList<Card>)toCopy.cards.clone();
	}
	
	public static void main(String[] args)
	{
		Deck d = new Deck();

		for(Card c : d.cards)
			System.out.print(c + "\t ");		
		System.out.println('\n');
		
		d.shuffle();
		for(Card c : d.cards)
			System.out.print(c + "\t ");		
		System.out.println('\n');
		
		d.shuffle();
		for(Card c : d.cards)
			System.out.print(c + "\t ");		
		System.out.println('\n');
		
		d.shuffle();
		for(Card c : d.cards)
			System.out.print(c + "\t ");		
		System.out.println('\n');
		
		d.shuffle();
		for(Card c : d.cards)
			System.out.print(c + "\t ");		
		System.out.println('\n');
	}
	
	public void shuffle()
	{
		cards = Deck.shuffle(cards);
	}
	
	public boolean add(Card c)
	{
		return cards.add(c);
	}
	
	public boolean remove(Card c)
	{
		return cards.remove(c);
	}
	
	private static LinkedList<Card> shuffle(LinkedList<Card> l)
	{
		if(l.size() <= 1)
			return l;
		
		LinkedList<Card> l1 = new LinkedList<Card>();
		LinkedList<Card> l2 = new LinkedList<Card>();
		
		int size = l.size();
		for(int i = 0; i < size; i ++)
		{
			if(i%2 == 0)
				l1.add(l.pop());
			else
				l2.add(l.pop());
		}
		
		if(l1.size() > 1)
			l1 = shuffle(l1);
		if(l2.size() > 1)
			l2 = shuffle(l2);
		
		l = randMerge(l1, l2);
		return l;
	}
	
	private static LinkedList<Card> randMerge(LinkedList<Card> l1, LinkedList<Card> l2) 
	{
		if(l1.size() == 0)
			return l2;
		if(l2.size() == 0)
			return l1;
		
		LinkedList<Card> res = new LinkedList<Card>();
		
		while(l1.size() > 0 && l2.size() > 0)
		{
			if(rand.nextBoolean())
				res.add(l1.pop());
			else
				res.add(l2.pop());
		}
		res.addAll(l1);
		res.addAll(l2);
		
		return res;
	}
}
