import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

public class Prob54
{
	public static void main(String[] args)
	{
		ArrayList<PokerHand[]> hands = getHands();
		
		int count = 0;
		for(PokerHand[] pairOfHands : hands)
		{
			System.out.println(pairOfHands[0]);
			System.out.println(pairOfHands[1]);
			System.out.println(pairOfHands[0].compareTo(pairOfHands[1]));
			if(pairOfHands[0].compareTo(pairOfHands[1]) > 0)
				count ++;
		}
		
		System.out.println(count);
		
		
		/*Hand h1 = new Hand();
		h1.add("ah");
		h1.add("qc");
		h1.add("7s");
		h1.add("8d");
		h1.add("9s");
		Hand h2 = new Hand();
		h2.add("8h");
		h2.add("8h");
		h2.add("4h");
		h2.add("5h");
		h2.add("6d");
		System.out.println((new PokerHand(h1)).beats(new PokerHand(h2)));*/
	}
	
	public static ArrayList<PokerHand[]> getHands()
	{
		ArrayList<PokerHand[]> handPairs = new ArrayList<PokerHand[]>();
		try
		{
			BufferedReader in = new BufferedReader(new FileReader("p054_poker.txt"));
			for(int line = 0; line < 1000; line ++)
			{
				String twoHands = in.readLine();
				String[] tenCards = twoHands.split(" ");
				if(tenCards.length != 10)
					System.err.println("WRONG NUMBER OF CARDS ON LINE " + line);
				
				Hand[] pairOfHands = new Hand[2];
				pairOfHands[0] = new Hand();
				pairOfHands[1] = new Hand();
				
				for(int i = 0; i < 10; i ++)
				{
					Card c = new Card(tenCards[i]);
					pairOfHands[i/5].add(c);
				}
				
				PokerHand[] pairOfPHands = new PokerHand[2];
				pairOfPHands[0] = new PokerHand(pairOfHands[0]);
				pairOfPHands[1] = new PokerHand(pairOfHands[1]);
				
				handPairs.add(pairOfPHands);
			}
			in.close();
		}
		catch(java.io.IOException e){System.err.println(e);}
		
		return handPairs; 
	}
}