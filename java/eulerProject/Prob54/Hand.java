import java.util.LinkedList;

public class Hand
{
	LinkedList<Card> cards = new LinkedList<Card>();
	int numCards;
	
	public Hand()
	{
		cards = new LinkedList<Card>();
		numCards = cards.size();
	}
	
	public Hand(int count)
	{
		cards = new LinkedList<Card>();
		Deck d = new Deck();
		for(int i = 0; i < count; i ++)
			cards.add(d.cards.pop());
		numCards = cards.size();
	}
	
	public Hand(Deck d, int count)
	{
		cards = new LinkedList<Card>();
		for(int i = 0; i < count; i ++)
			cards.add(d.cards.pop());
		numCards = cards.size();
	}
	
	public Hand(LinkedList<Card> cs)
	{
		cards = cs;
		numCards = cards.size();
	}
	
	public Hand(Hand h)
	{
		cards = (LinkedList<Card>) h.cards.clone();
		numCards = cards.size();
	}
	
	public static void main(String[] args)
	{
		//Deck d = new Deck();
		Hand h1 = new Hand();
		Hand h2 = new Hand();
		
		h1.add(new Card("3s"));
		h1.add(new Card("JH"));
		h1.add(new Card("Qd"));
		
		h2.add(new Card("jh"));
		h2.add(new Card("qD"));
		h2.add(new Card("2S"));
		
		System.out.println(h1.equals(h2));
	}
	
	public void add(String c)
	{
		this.add(new Card(c));
	}
	
	public void add(Card c)
	{
		cards.add(c);
		numCards ++;
	}
	
	public String toString()
	{
		java.util.Collections.sort(cards);
		String s = "(";
		for(Card c : cards)
			s += c + ", ";
		
		s = s.substring(0, s.length()-2);
		s += ")";
		
		return s;
	}
	
	public boolean equals(Object o)
	{
		if(o != null && o instanceof Hand)
		{
			Hand that = (Hand)o;
			if(that.numCards == this.numCards)
			{
				LinkedList<Card> copy = (LinkedList<Card>)this.cards.clone();
				for(Card c : that.cards)
				{
					if(!copy.remove(c))
						return false;
				}
				
				return copy.isEmpty();
			}
		}
		
		return false;
	}
}
