import java.math.BigDecimal;
import java.io.BufferedReader;
import java.io.FileReader;


public class Prob25
{
	public static BigDecimal phiPlus;
	public static BigDecimal phiMinus;
	public static BigDecimal root5;
	public static BigDecimal thousandDigits;
	 
	public static void main(String[] args)
	{
		String phiDigits = readPhi();
		phiPlus = new BigDecimal(phiDigits);
		phiMinus = (phiPlus.subtract(BigDecimal.ONE)).negate();
		root5 = phiPlus.subtract(phiMinus);
		thousandDigits = BigDecimal.TEN.pow(999);
		 
		///* This code was used to find a reasonable range
		// Was originally tried with index = 100 to find under/over of 3200/6400
		// Reasonable range is currently: 4000/5000
		int index = 1000;
		BigDecimal num = fib(index);
		while(num.compareTo(thousandDigits) < 0)
		{
			System.out.println("Too low: " + index);
			index *= 2;
			num = fib(index);
		}//*/
		
		int i = index/2;
		int j = index;
		while(i + 1 < j)
		{
			index = (i + j)/2; 
			System.out.println("Trying index: " + index);
			num = fib(index);
			int comparator = num.compareTo(thousandDigits);
			if(comparator == 0)
				System.out.println("The exact answer is: " + index); // Just in case!
			else if(comparator < 0)
			{
				i = index;
				System.out.println("It was too small.");
			}
			else
			{
				j = index;
				System.out.println("It was larger or equal.");
			}
			System.out.println();
		}
		
	}
	
	public static BigDecimal fib(int n)
	{
		return ((phiPlus.pow(n)).subtract(phiMinus.pow(n))).divide(root5);
	}
	
	public static String readPhi()
	{
		try
		{
			BufferedReader in = new BufferedReader(new FileReader("phi.txt"));
			return in.readLine();
		}
		catch(java.io.IOException e){System.err.println(e);}
		return "Problem";
	}
}
