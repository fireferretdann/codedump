import java.util.ArrayList;

public class Prob42
{
	public static void main(String[] args)
	{
		ArrayList<String> words = WordReader.getWords();
		System.out.println("Words copied");
		
		int count = 0;
		for(String word : words)
		{
			if(isTriangleNumber(getValue(word)))
				count ++;
		}
		
		System.out.println(count);
	}
	
	public static boolean isTriangleNumber(int t)
	{ // Testing using the assumption that n < sqrt(2t) < n+1	(tested)
		int n = (int)Math.sqrt(2*t);
		return n*(n+1)/2 == t;
	}
	
	public static int getValue(String word)
	{
		int total = 0;
		for(char c : word.toCharArray())
		{
			total += c - ('A' - 1);
		}
		return total;
	}
}