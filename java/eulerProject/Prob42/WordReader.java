import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

public class WordReader
{
	public static ArrayList<String> getWords()
	{
		ArrayList<String> wordList = new ArrayList<String>();
		String words = "";
		try
		{
			BufferedReader in = new BufferedReader(new FileReader("words.txt"));
			words = in.readLine();
		}
		catch(java.io.IOException e){System.err.println(e);}
		
		int lastComma = -1;
		for(int i = 0; i < words.length(); i ++)
		{
			if(words.charAt(i) == ',')
			{
				wordList.add(words.substring(lastComma+2, i-1));
				lastComma = i;
			}
		}
		wordList.add(words.substring(lastComma+2, words.length()-1));
		
		return wordList; 
	}
}
