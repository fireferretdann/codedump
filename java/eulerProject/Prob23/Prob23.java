import java.util.ArrayList;
import java.util.List;

public class Prob23
{
	public static int[] abs;
	public static void main(String[] args)
	{
		ArrayList<Integer> abundants = new ArrayList<Integer>();
		for(int i = 0; i < 28123; i ++)
			if(naiveSumDivisors(i) > i)
				abundants.add(i);
		//print(abundants.subList(0, 15));
		abs = new int[abundants.size()];
		for(int i = 0; i < abs.length; i ++)
			abs[i] = abundants.get(i);
		
		int total = 0;
		for(int i = 0; i < 28123; i++)
			if(!canSum(i))
			{
				total += i;
				System.out.print(i + ",\t");
			}
		System.out.println("\n\n" + total);
	}
	
	public static boolean canSum(int n)
	{
		int i = 0;
		int j = abs.length-1;
		while(i <= j)
		{
			int sum = abs[i] + abs[j];
			if(sum == n)
				return true;
			else if(sum < n)
				i++;
			else
				j--;
		}
		return false;
	}
	
	public static int naiveSumDivisors(int n)
	{
		int total = 0;
		for(int i = 1; i <= n/2; i ++)
			if(n%i == 0)
				total += i;
		return total;
	}

	public static void print(List list)
	{
		String s = "[";
		for(Object i : list)
			s += i + ", ";
		s = s.substring(0, s.length()-2) + "]";
		System.out.println(s);
	}
}
