import java.io.BufferedReader;
import java.io.FileReader;
import java.util.TreeSet;

public class PrimeGeneratorInt
{
	public static TreeSet<Integer> primes = readPrimes();
	
	public static void main(String[] args)
	{
		System.out.println();
		primes.add(2);
		primes.add(3);
		int suspect = primes.last();
		suspect = suspect - (suspect%6) + 5; // Next possible prime(ish)
		while(true)
		{
			if(isPrime(suspect))
			{
				System.out.println(suspect);
				primes.add(suspect);
			}
			if(suspect+2 > 0  &&  isPrime(suspect+2))
			{
				System.out.println(suspect+2);
				primes.add(suspect+2);
			}
			
			suspect += 6;
			if(suspect < 0)
				break;
		}
	}
	
	public static boolean isPrime(int n)
	{
		for(int p : primes)
		{
			if(n%p == 0)
				return false;
			if(p*p > n)
				return true;
		}
		return true;
	}
	
	public static boolean isPrime(int n, TreeSet<Integer> ps)
	{
		for(int p : ps)
		{
			if(n%p == 0)
				return false;
			if(p*p > n)
				return true;
		}
		return true;
	}
	
	public static TreeSet<Integer> readPrimes()
	{
		TreeSet<Integer> ps = new TreeSet<Integer>();
		try
		{
			BufferedReader in = new BufferedReader(new FileReader("intPrimes.txt"));
			
			String line = in.readLine();
			while(line != null)
			{
				ps.add(Integer.parseInt(line));
				line = in.readLine();
			}
			
			in.close();
		}
		catch(java.io.IOException e){System.err.println(e);} // Add creating a file functionality for completeness.
		
		ps.pollLast();
		
		return ps;
	}
}