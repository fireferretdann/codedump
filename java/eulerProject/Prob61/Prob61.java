import java.util.LinkedList;

public class Prob61
{
	public static LinkedList<Integer> numbersInOrder = new LinkedList<Integer>();
	
	public static void main(String[] args)
	{
		for(int i = 1010; i < 10000; i ++)
		{
			if(isOctagonal(i))
			{
				int found[] = new int[9];
				found[0] = 1; // Used later for ease of coding when recieving input from getPolyNumber()
				
				found[8] = i;
				numbersInOrder.add(i);
				
				checkRest(i, found);
				
				numbersInOrder.removeLast();
				found[8] = 0;
			}
		}
		
		// When they print out, manually add them you lazy bum.
	}
	
	public static void checkRest(int lastNumber, int[] f)
	{
		boolean keepChecking = false;
		for(int i = 3; i < 8; i ++)
		{
			if(f[i] == 0)
				keepChecking = true;
		}
		
		if(keepChecking)
		{
			int[] found = new int[f.length];
			System.arraycopy(f, 0, found, 0, found.length);
			
			int lastTwo = lastNumber%100;
			for(int i = 10; i < 100; i ++)
			{
				int newNumber = 100*lastTwo + i;
				
				if(found[7] == 0  &&  isHeptagonal(newNumber))
				{
					found[7] = newNumber;
					numbersInOrder.add(newNumber);
					
					checkRest(newNumber, found);
					
					numbersInOrder.removeLast();
					found[7] = 0;
				}
				if(found[6] == 0  &&  isHexagonal(newNumber))
				{
					found[6] = newNumber;
					numbersInOrder.add(newNumber);
					
					checkRest(newNumber, found);
					
					numbersInOrder.removeLast();
					found[6] = 0;
				}
				if(found[5] == 0  &&  isPentagonal(newNumber))
				{
					found[5] = newNumber;
					numbersInOrder.add(newNumber);
					
					checkRest(newNumber, found);
					
					numbersInOrder.removeLast();
					found[5] = 0;
				}
				if(found[4] == 0  &&  isSquare(newNumber))
				{
					found[4] = newNumber;
					numbersInOrder.add(newNumber);
					
					checkRest(newNumber, found);
					
					numbersInOrder.removeLast();
					found[4] = 0;
				}
				if(found[3] == 0  &&  isTriangular(newNumber))
				{
					found[3] = newNumber;
					numbersInOrder.add(newNumber);
					
					checkRest(newNumber, found);
					
					numbersInOrder.removeLast();
					found[3] = 0;
				}
			}
		}
		else
		{
			if(numbersInOrder.getFirst()/100 == numbersInOrder.getLast()%100)
			{
				for(int p : numbersInOrder)
					System.out.println(p);
				System.out.println();
			}
		}
	}
	
	public static int getPolyNumber(int possiblePoly)
	{
		if(isHeptagonal(possiblePoly))
			return 7;
		if(isHexagonal(possiblePoly))
			return 6;
		if(isPentagonal(possiblePoly))
			return 5;
		if(isSquare(possiblePoly))
			return 4;
		if(isTriangular(possiblePoly))
			return 3;
		
		return 0;
	}
	
	public static boolean isOctagonal(int possibleOct)
	{
		// Uses some algebra based on P(n) = n(3n−2)
		double toCheck = Math.sqrt(4 + (12*possibleOct));
		return toCheck%6 == 4;
	}
	
	public static boolean isHeptagonal(int possibleHept)
	{
		// Uses some algebra based on P(n) = n(5n−3)/2 	
		double toCheck = Math.sqrt(9 + (40*possibleHept));
		return toCheck%10 == 7;
	}
	
	public static boolean isHexagonal(int possibleHex)
	{
		// Uses some algebra based on P(n) = n(2n−1)
		double toCheck = Math.sqrt(1 + (8*possibleHex));
		return toCheck%4 == 3;
	}
	
	public static boolean isPentagonal(int possiblePent)
	{
		// Using some algebra based on P(n) = n(3n-1)/2
		double toCheck = Math.sqrt(1 + (24*possiblePent));
		return toCheck%6 == 5;
	}
	
	public static boolean isSquare(int possibleSqr)
	{
		return Math.sqrt(possibleSqr)%1 == 0  &&  possibleSqr != 0;
	}
	
	public static boolean isTriangular(int possibleTri)
	{
		// Uses some algebra based on P(n) = n(n+1)/2
		double toCheck = Math.sqrt(1 + (8*possibleTri));
		return toCheck%2 == 1  &&  possibleTri != 0;
	}
}