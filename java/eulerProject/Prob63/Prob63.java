import java.math.BigInteger;
import java.util.LinkedList;
import java.util.ListIterator;
import java.io.BufferedReader;
import java.io.FileReader;

public class Prob63
{
	//public static LinkedList<Integer> primes = readPrimes();
	
	public static void main(String[] args)
	{
		long count = 0;
		for(int i = 1; i < 1234; i ++)
		{
			double endCondition = 1.0 + 1.0/(double)i;
			
			//ListIterator<Integer> ks = primes.listIterator();
			int k = 1;
			boolean keepLooking = true;
			BigInteger exp = pow(k, i);
			while(keepLooking  &&  exp.toString().length() < i + 1)
			{
				if(exp.toString().length() == i)
				{
					count++;
					//keepLooking = false;
					System.out.println(k + "^" + i + " has " + i + " digits");
				}
				k ++;
				exp = pow(k, i);
			}
			
			//if(keepLooking)
				//System.out.println("Didn't find one for " + i + " digits :(");
		}
		
		System.out.println(count);
	}
	
	public static boolean isPrime(long n)
	{
		for(int i = 2; i*i <= n; i ++)
		{
			if(n%i == 0)
				return false;
		}
		return true;
	}
	
	public static BigInteger pow(int base, int exponent)
	{
		BigInteger b = new BigInteger("" + base);
		return b.pow(exponent);
	}
	
	public static LinkedList<Integer> readPrimes()
	{
		int primeCount = 12345;
		LinkedList<Integer> ps = new LinkedList<Integer>();
		try
		{
			BufferedReader in = new BufferedReader(new FileReader("..\\intPrimes.txt"));
			
			String line = in.readLine();
			while(line != null  &&  line != ""  &&  ps.size() < primeCount)
			{
				int lastPrime = Integer.parseInt(line);
				ps.add(lastPrime);
				line = in.readLine();
			}
			
			in.close();
		}
		catch(java.io.IOException e){System.err.println(e);} // Add creating a file functionality for completeness.
		
		return ps;
	}
}