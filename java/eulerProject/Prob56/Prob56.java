import java.math.BigInteger;

public class Prob56
{
	public static void main(String[] args)
	{
		int max = 0;
		for(int a = 0; a < 100; a ++)
		{
			for(int b = 0; b < 100; b ++)
			{
				int sum = sumPowerDigits(a, b);
				if(sum > max)
				{
					max = sum;
				}
			}
		}
		
		System.out.println(max);
	}
	
	public static int sumPowerDigits(int a, int b)
	{
		BigInteger bigA = new BigInteger("" + a);
		return sumDigits(bigA.pow(b));
	}
	
	public static int sumDigits(BigInteger n)
	{
		int sum = 0;
		while(n.compareTo(BigInteger.ZERO) > 0)
		{
			BigInteger[] nByTen = n.divideAndRemainder(BigInteger.TEN);
			n = nByTen[0];
			sum += nByTen[1].intValue();
		}
		return sum;
	}
}