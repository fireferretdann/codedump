import java.util.ArrayList;

public class Prob46
{
	public static ArrayList<Integer> primes = new ArrayList<Integer>();
	
	public static void main(String[] args)
	{
		primes.add(2);
		
		int toCheck = 2;
		boolean hasFailed = false;
		
		while(testConjecture(++toCheck)) // Checks every odd number
		{
			toCheck ++;
		}
		
		System.out.println(toCheck);
		// 5777
		// I was expecting a much bigger answer
	}
	
	// Only odd numbers dingus!
	public static boolean testConjecture(int n) // Returns true if the conjecture is upheld, and false if it is broken
	{
		if(isPrime(n))
			return true;
		
		// is composite now
		
		for(int p : primes)
		{
			int diff = n - p;
			if(isTwiceSquare(diff))
			{
				return true;
			}
		}
		
		return false;
	}
	
	public static boolean isTwiceSquare(int n)
	{
		int halfRoot = (int)Math.sqrt(n/2);
		
		return halfRoot*halfRoot*2 == n;
	}
	
	public static boolean isPrime(int n)
	{
		if(primes.contains(n))
			return true;
		else
			for(int p : primes)
				if(n%p == 0)
					return false;
				else if(p*p > n)
				{
					primes.add(n);
					return true;
				}
		
		primes.add(n);
		return true;
	}
}