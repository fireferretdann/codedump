import java.util.HashSet;

public class Prob32
{
	public static void main(String[] args)
	{
		int total = 0;
		HashSet<Integer> products = new HashSet<Integer>();
		
		for(int i = 0; i < 10000; i ++)
		{
			for(int j = i; j < 10000; j ++)
			{
				if(isPandigital("" + i + j + (i*j)))
				{
					System.out.println("" + i + " * " + j + " = " + (i*j));
					products.add(i*j);
				}
			}
		}
		
		System.out.println();
		for(Integer i : products)
		{
			System.out.println(i);
			total += i;
		}
		
		System.out.println(total);
	}
	
	private static boolean isPandigital(String s)
	{
		if(s.length() != 9)
			return false;
		
		boolean[] digits = new boolean[9];
		
		for( char c : s.toCharArray())
		{
			if( c == '0')
				return false;
			int digit = c - 48 - 1;	// 48 for conversion from digit characters to their number, 1 for removing 0
			if(digits[digit])
				return false;
			digits[digit] = true;
		}
		
		return true;
	}
	
}
