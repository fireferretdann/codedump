public class Prob43
{
	public static void main(String[] args)
	{
		long total = 0;
		for(long toTest = 123456789l; toTest < 9876543210l; toTest ++)
		{
			if((toTest/10000)%5 == 0) // checks that d6 is 5 or 0 which must be true for d4d5d6 to be divisible by 5.
			{
				if(hasProperty(toTest)  &&  isPandigital(toTest))
				{
					total += toTest;
					System.out.println("Adding " + toTest);
				}
			}
		}
		
		System.out.println(total);
	}
	
	public static boolean hasProperty(long n)
	{
		String nString = "" + n;
		
		while(nString.length() < 10)
			nString = "0" + nString;
		
		if(Integer.parseInt(nString.substring(1, 1+3))%2  != 0) return false;
		if(Integer.parseInt(nString.substring(2, 2+3))%3  != 0) return false;
		if(Integer.parseInt(nString.substring(3, 3+3))%5  != 0) return false;
		if(Integer.parseInt(nString.substring(4, 4+3))%7  != 0) return false;
		if(Integer.parseInt(nString.substring(5, 5+3))%11 != 0) return false;
		if(Integer.parseInt(nString.substring(6, 6+3))%13 != 0) return false;
		if(Integer.parseInt(nString.substring(7, 7+3))%17 != 0) return false;
		
		return true;
	}
	
	public static boolean isPandigital(long n)
	{
		if(n < 123456789  ||  n > 9876543210l)
			return false;
		
		boolean[] digitsPresent = new boolean[10];
		for(long p = 9; p >= 0; p --)
		{
			int digit = (int)(n / (long)Math.pow(10, p));
			
			if(digitsPresent[digit])
				return false;
			else
				digitsPresent[digit] = true;
			
			if(digit != 0)
				n %= Math.pow(10, p)*(digit);
		}
		return true;
	}
}