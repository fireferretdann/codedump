import java.math.BigInteger;	//Maybe...
import java.util.Random;

public class Prob27
{
	public static BigInteger two = new BigInteger("2");
	public static BigInteger three = new BigInteger("3");
	public static BigInteger six = new BigInteger("6");
	//public static Random rand = new Random();
	
	public static void main(String[] args)
	{
		BigInteger a;
		BigInteger b;
		BigInteger n;
		BigInteger maxProd = new BigInteger("0");
		
		int primeCount;
		int maxPrimes = 0;
		
		for(int i = -1000; i <= 1000; i ++)
		{
			a = new BigInteger("" + i);
			System.out.println("a = " + a);
			
			for(int j = -1000; j <= 1000; j ++)
			{
				b = new BigInteger("" + j);
				n = new BigInteger("0");
				primeCount = 0;
				
				while(isPrime(calculate(a, b, n)))
				{
					primeCount ++;
					n = n.add(BigInteger.ONE);
				}
				
				if(primeCount > maxPrimes)
				{
					maxPrimes = primeCount;
					maxProd = a.multiply(b);
					System.out.println("\nA new maximum was found with " + maxPrimes + " primes.\n");
				}
			}
		}
		
		System.out.println("The final maximum was " + maxPrimes + " primes with a product of " + maxProd);
	}
	
	public static BigInteger calculate(BigInteger a, BigInteger b, BigInteger n)
	{
		return n.multiply(n).add(n.multiply(a)).add(b);
	}
	
	public static boolean isPrime(BigInteger n)
	{
		if(! n.isProbablePrime(20))
			return false;
		
		if(n.mod(two).compareTo(BigInteger.ZERO) == 0  ||  n.mod(three).compareTo(BigInteger.ZERO) == 0)
		{
			if(n.compareTo(two) == 0  ||  n.compareTo(three) == 0)
				return true;
			else
				return false;
		}
		
		BigInteger sqrt = sqrt(n);
		BigInteger toCheck = two.add(three);
		
		while(toCheck.compareTo(sqrt) <= 0)
		{
			if(n.mod(toCheck).compareTo(BigInteger.ZERO) == 0  ||  n.mod(toCheck.add(two)).compareTo(BigInteger.ZERO) == 0)
				return false;
			else
				toCheck = toCheck.add(six);
		}
		
		return true;
	}
	
	public static BigInteger sqrt(BigInteger n)
	{
		if(n.compareTo(BigInteger.ONE) <= 0)
			return n;
		
		BigInteger a = new BigInteger(n.toString());
		BigInteger b = new BigInteger("1");
		
		while(a.compareTo(b) > 0)
		{
			a = (a.add(b)).divide(two);
			b = n.divide(a);
		}
		return a;
	}
}
