
public class Prob31
{
	public static void main(String[] args)
	{
		int coinValues[] = {200, 100, 50, 20, 10, 5, 2, 1};
		
		int total = 200;
		System.out.println(countWays(total, coinValues));
	}
	
	public static int countWays(int total, int[] values)
	{
		if(values.length == 1)
			return 1;
		
		int[] newValues = new int[values.length-1];
		System.arraycopy(values, 1, newValues, 0, newValues.length);
		
		if(values[0] > total)
		{
			return countWays(total, newValues);
		}
		
		if(values[0] == total)
		{
			return 1 + countWays(total, newValues);
		}
		
		return (countWays(total-values[0], values) + countWays(total, newValues));
	}
	
	public static String toString(int[] a)
	{
		String s = "[";
		
		for(int i : a)
			s += i + ", ";
		
		s += "]";
		
		return s;
	}
}
