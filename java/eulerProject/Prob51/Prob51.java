import java.util.ArrayList;

public class Prob51
{
	
	public static void main(String[] args)
	{
		String largestFamily = "";
		int largestFamilyCount = 0;
		int i = 11;
		while(largestFamilyCount < 9  &&  i < Integer.MAX_VALUE-1)
		{
			String[] families = filterReplacements(getReplacementString(i));
			for(String family : families)
			{
				int familyCount = countPrimesInFamily(family);
				if(familyCount > largestFamilyCount)
				{
					largestFamily = family;
					largestFamilyCount = familyCount;
					System.out.println("New largest Family: " + family);
					System.out.println("With count: " + familyCount);
				}
			}
			i += 2;
		}
	}
	
	public static int countPrimesInFamily(String family)
	{
		int count = 0;
		int start; // Allows for skipping 0 if family starts with a replacement (ex: x23)
		if(family.charAt(0) == 'x')
			start = 1;
		else
			start = 0;
		for(int i = start; i <= 9; i ++)
		{
			if(isPrime(doReplacement(family, i)))
				count ++;
		}
		return count;
	}
	
	public static int doReplacement(String family, int digit)
	{
		return Integer.parseInt(family.replace('x', ("" + digit).charAt(0)));
	}
	
	public static String[] filterReplacements(String[] reps)
	{
		ArrayList<String> result = new ArrayList<String>();
		
		for(String s : reps)
		{
			if(s.matches(".*[^024685x]")) // Making the *bold* assumption here that '5' and '2' are not the solution
			{
				result.add(s);
			}
		}
		
		return result.toArray(new String[0]);
	}
	
	// Note: Solution cannot have the last digit as a replacement, or 5 of the ten possibilities would be guarenteed not prime.
	public static String[] getReplacementString(int n)
	{
		int log10 = (int)Math.ceil(Math.log10(n));
		int replacementCount = (int)Math.pow(2, log10)-1;
		
		String[] reps = new String[replacementCount];
		
		String[] digits = new String[log10];
		String s = "" + n;
		for(int i = 0; i < digits.length; i ++)
			digits[i] = "" + s.charAt(i);
		
		for(int bitsToReplace = 0; bitsToReplace < replacementCount; bitsToReplace ++)
		{
			String thisReplacement = "";
			for(int d = 0; d < digits.length; d ++)
			{
				int theCoolerD = (int)Math.pow(2, d);
				if((theCoolerD&bitsToReplace) == 0)
				{
					thisReplacement += "x";
				}
				else
				{
					thisReplacement += digits[d];
				}
			}
			reps[bitsToReplace] = thisReplacement;
		}
		
		return reps;
	}
	
	public static boolean isPrime(int n)
	{
		if(n == 2  ||  n == 3)
			return true;
		if(n%2 == 0)
			return false;
		if(n%3 == 0)
			return false;
		
		for(int p = 5; p*p <= n; p += 6)
		{
			if(n%p == 0)
				return false;
			if(n%(p+2) == 0)
				return false;
		}
		
		return true;
	}
}