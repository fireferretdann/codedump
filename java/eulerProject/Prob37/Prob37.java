import java.util.TreeSet;

public class Prob37
{
	public static TreeSet<Integer> primes = new TreeSet<Integer>();
	public static TreeSet<Integer> trunkPrimes = new TreeSet<Integer>();
	
	public static void main(String[] args)
	{
		primes.add(2);
		primes.add(3);
		primes.add(5);
		primes.add(7);
		
		while(trunkPrimes.size() < 11)
		{
			int p = findNextPrime();
			primes.add(p);
			
			if(trunkLeft(p)  &&  trunkRight(p))
			{
				trunkPrimes.add(p);
				System.out.println("Adding trunk prime " + p);
			}
		}
		
		int total = 0;
		for(int p : trunkPrimes)
			total += p;
		
		System.out.println("The total is " + total);
	}
	
	
	public static boolean trunkLeft(int p)
	{
		String pString = "" + p;
		while(pString.length() > 1)
		{
			pString = pString.substring(1, pString.length());
			
			p = Integer.parseInt(pString);
			if(! isPrime(p))
				return false;
		}
		
		return true;
	}
	
	
	public static boolean trunkRight(int p)
	{
		String pString = "" + p;
		while(pString.length() > 1)
		{
			pString = pString.substring(0, pString.length()-1);
			
			p = Integer.parseInt(pString);
			if(! isPrime(p))
				return false;
		}
		
		return true;
	}
	
	
	public static int findNextPrime()
	{
		int toTest;
		// Goes through numbers to next mult of 6 checking for primes
		for(toTest = primes.last()+2; toTest%6 != 0; toTest ++)
			if(isPrime(toTest))
				return toTest;
		
		while(true)
		{
			if(isPrime(toTest-1))
				return toTest-1;
			if(isPrime(toTest+1))
				return toTest+1;
			toTest += 6;
		}
	}
	
	
	public static boolean isPrime(int toTest)
	{
		if(toTest < 2)
			return false;
		if(primes.contains(toTest))
			return true;
		
		for(int p : primes)
		{
			if(p*p > toTest)
				return true;
			
			if(toTest%p == 0)
				return false;
		}
		
		System.out.println("Huh? " + toTest + " broke something...");
		return true;
	}
}
