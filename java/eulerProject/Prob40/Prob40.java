public class Prob40
{
	public static String fractionalPart = "";
	public static void main(String[] args)
	{
		for(int i = 1; i < 1000000; i ++)
		{
			fractionalPart += i;
		}
		int digits[] = new int[7];
		for(int p = 0; p < 7; p++)
			digits[p] = d((int)Math.pow(10, p));
		for(int d : digits)
			System.out.println(d);
		System.out.println(d(1) * d(10) * d(100) * d(1000) * d(10000) * d(100000) * d(1000000));
	}
	
	public static int d(int n)
	{
		try
		{
			return Integer.parseInt("" + fractionalPart.charAt(n-1));
		}
		catch(Exception e)
		{
			System.err.println(e);
			return 0;
		}
	}
}