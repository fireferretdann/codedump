import java.io.BufferedReader;
import java.io.FileReader;

public class Prob59
{
	public static void main(String[] args)
	{
		String file = "";
		try
		{
			BufferedReader in = new BufferedReader(new FileReader("cipher.txt"));
			file = in.readLine();
		}
		catch(Exception e){ System.err.println(e);}
		
		String[] cipherText = file.split(",");
		char[] cipherArray = new char[cipherText.length];
		char[] decodeArray = new char[cipherText.length];
		for(int i = 0; i < cipherText.length; i ++)
			cipherArray[i] = (char)Integer.parseInt(cipherText[i]);
		
		for(char a = 'a'; a <= 'z'; a ++)
		{
			for(char b = 'a'; b <= 'z'; b ++)
			{
				for(char c = 'a'; c <= 'z'; c ++)
				{
					for(int i = 0; i < cipherArray.length; i ++)
					{
						if(i%3 == 0)
							decodeArray[i] = (char)(a^cipherArray[i]);
						if(i%3 == 1)
							decodeArray[i] = (char)(b^cipherArray[i]);
						if(i%3 == 2)
							decodeArray[i] = (char)(c^cipherArray[i]);
					}
					
					String decodedString = new String(decodeArray);
					if(decodedString.matches("[\\p{Punct}\\p{Alnum}\\p{Space}]+")  &&  decodedString.contains("the"))
					{
						System.out.println("" + a + b + c);
						int sum = 0;
						for(int i = 0; i < decodeArray.length; i ++)
							sum += decodeArray[i];
						System.out.println(sum);
						System.out.println(decodedString);
						System.out.println();
						(new java.util.Scanner(System.in)).nextLine();
					}
				}
			}
		}
	}
}