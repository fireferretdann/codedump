import java.util.TreeSet;

public class Prob74
{
	public static void main(String[] args)
	{
		int count = 0;
		for(int i = 0; i < 1000000; i ++)
		{
			if(countNonRepeats(i) >= 60)
				count ++;
		}
		System.out.println(count);
	}
	
	public static long nextInSequence(long n)
	{
		long total = 0;
		while(n > 0)
		{
			total += fact(n%10);
			n /= 10;
		}
		return total;
	}
	
	public static int countNonRepeats(long n)
	{
		TreeSet<Long> visited = new TreeSet<Long>();
		
		while(visited.add(n))
		{
			n = nextInSequence(n);
		}
		
		return visited.size();
	}
	
	public static long fact(long n)
	{
		long product = 1;
		for(int i = 1; i <= n; i ++)
		{
			product *= i;
		}
		return product;
	}
}