import java.util.HashSet;

public class Prob34
{	
	// Using a lookup for factorials for speed purposes.
	final public static int[] factorial = {1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880};
	public static HashSet<Integer> satisfiers = new HashSet<Integer>();
	
	public static void main(String[] args)
	{
		int max = factorial[9]*7;
		
		for(int i = 10; i < max; i ++)
			test(i);
		
		
		int total = 0;
		for(int i : satisfiers)
			total += i;
		
		System.out.println(total);
	}
	
	
	// This method tests if a number satisfies the desired property and adds it to the "satisfiers" list if it does
	public static void test(int toTest)
	{
		int sum = 0;
		int remainingDigits = toTest;
		
		while(remainingDigits > 0)
		{
			int currentDigit = remainingDigits%10;
			int currentFactorial = factorial[currentDigit];
			sum += currentFactorial;
			
			remainingDigits /= 10;
		}
		
		if(toTest == sum)
		{
			System.out.println("The number " + toTest + " satisfies!");
			satisfiers.add(toTest);
		}
	}
}
