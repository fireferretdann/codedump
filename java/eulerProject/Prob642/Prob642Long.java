public class Prob642Long
{
	
	public static void main(String[] args)
	{
		long max = Long.parseLong(args[0]);
		long endTime;
		long startTime = System.currentTimeMillis();
		
		long sum = 0l;
		
		for(long i = max; i >= 2; i --)
		{
			//long i = new long("94");
			long LPF = largestPrimeFactor(i);
			sum += LPF;
			sum %= 1000000000l;
			//System.out.println(i + ":\t" + LPF);
		}
		
		System.out.println(sum);
		
		endTime = System.currentTimeMillis();
		System.out.println(endTime-startTime);
	}
	
	public static long largestPrimeFactor(long n)
	{
		long result = 0l;
		
		if(n%2 == 0)
		{
			result = 2;
			while(n%2 == 0)
				n /= 2;
		}
		if(n%3 == 0)
		{
			result = 3;
			while(n%3 == 0)
				n /= 3;
		}
		
		for(long p = 5; p*p <= n; p += 6)
		{
			if(n%p == 0)
			{
				result = p;
				while(n%p == 0)
				{
					n /= p;
				}
			}
			if(n%(p+2) == 0)
			{
				result = p+2;
				while(n%(p+2) == 0)
				{
					n /= p+2;
				}
			}
		}
		
		if(n > result)
			return n;
		else
			return result;
	}
	
	public static long sqrt(long n)
	{
		//return (long)Math.sqrt((double)n);
		int bitLength = 64 - Long.numberOfLeadingZeros(n-1);
		long guess = 1l<<(bitLength/2);
		return (guess + n/guess)/2;
	}
}