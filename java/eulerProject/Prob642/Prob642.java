import java.util.TreeSet;
import java.util.SortedSet;
import java.util.Collections;

public class Prob642
{
	
	public static SortedSet<Long> primes = Collections.synchronizedSortedSet(new TreeSet<Long>());
	
	public static void main(String[] args)
	{
		long max = Long.parseLong(args[0]);
		long endTime;
		long startTime = System.currentTimeMillis();
		
		prepPrimes(max);
		System.out.println("primes prepped \t" + primes.size() + " primes");
		
		long sum = 0l;
		
		for(long i = max; i >= 2; i --)
		{
			//long i = new long("94");
			long LPF = largestPrimeFactor(i);
			sum += LPF;
			sum %= 1000000000l;
			//System.out.println(i + ":\t" + LPF);
		}
		
		System.out.println(sum);
		
		endTime = System.currentTimeMillis();
		System.out.println("It took : " + (endTime-startTime) + "ms");
	}
	
	public static long largestPrimeFactor(long n)
	{
		long result = 0l;
		
		for(long prime : primes)
		{
			if(n < prime*prime)
				break;
			
			while(n%prime == 0)
			{
				result = prime;
				n /= prime;
			}
		}
		
		if(n > result)
			return n;
		else
			return result;
		
		/*long result = 0l;
		
		if(n%2 == 0)
		{
			result = 2;
			while(n%2 == 0)
				n /= 2;
		}
		if(n%3 == 0)
		{
			result = 3;
			while(n%3 == 0)
				n /= 3;
		}
		
		for(long p = 5; p*p <= n; p += 6)
		{
			if(n%p == 0)
			{
				result = p;
				while(n%p == 0)
				{
					n /= p;
				}
			}
			if(n%(p+2) == 0)
			{
				result = p+2;
				while(n%(p+2) == 0)
				{
					n /= p+2;
				}
			}
		}
		
		if(n > result)
			return n;
		else
			return result;*/
	}
	
	public static long sqrt(long n)
	{
		//return (long)Math.sqrt((double)n);
		int bitLength = 64 - Long.numberOfLeadingZeros(n-1);
		long guess = 1l<<(bitLength/2);
		return (guess + n/guess)/2;
	}
	
	public static void prepPrimes(long max)
	{
		primes.add(2l);
		primes.add(3l);
		
		long possPrime = 5l;
		
		while(primes.last() <= max)
		{
			PrimeCheck p = new PrimeCheck();
			PrimeCheck p2 = new PrimeCheck();
			PrimeCheck p6 = new PrimeCheck();
			PrimeCheck p8 = new PrimeCheck();
			p.start(possPrime);
			p2.start(possPrime+2);
			p6.start(possPrime+6);
			p8.start(possPrime+8);
			
			while(p.isAlive() ||  p2.isAlive()  ||  p6.isAlive()  ||  p8.isAlive());
			if(p.isPrime)
				primes.add(possPrime);
			if(p2.isPrime)
				primes.add(possPrime+2);
			if(p6.isPrime)
				primes.add(possPrime+6);
			if(p8.isPrime)
				primes.add(possPrime+8);
			
			possPrime += 12;
		}
		
		/*for(long group = 0; group < max/groupSize; group++)
		{
			for(long i = 1; i <= groupSize; i += 2)
				possiblePrimes.add(i + group*groupSize);
			
			possiblePrimes.remove(1l);
			
			for(long prime : primes)
			{
				long n = 2;
				
				while(n*prime <= possiblePrimes.last())
				{
					possiblePrimes.remove(n*prime);
					n++;
				}
			}
			
			while(!possiblePrimes.isEmpty())
			{
				long prime = possiblePrimes.pollFirst();
				primes.add(prime);
				
				long n = 2;
				if(!possiblePrimes.isEmpty())
				{
					while(n*prime <= possiblePrimes.last())
					{
						possiblePrimes.remove(n*prime);
						n++;
					}
				}
			}
			System.out.println((group*groupSize+1) + " through " + ((group+1)*groupSize+1) + " prepped");
		}*/
	}
}