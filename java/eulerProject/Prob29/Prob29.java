import java.util.HashSet;
import java.math.BigInteger;

public class Prob29
{
	public static void main(String[] args)
	{
		HashSet<BigInteger> set = new HashSet<BigInteger>();
		for(int a = 2; a <= 100; a ++)
		{
			for(int b = 2; b <= 100; b++)
			{
				set.add(pow(a, b));
			}
		}
		
		System.out.println(set.size());
	}
	
	public static BigInteger pow(int a, int b)
	{
		BigInteger prod = new BigInteger("" + 1);
		BigInteger base = new BigInteger("" + a);
		for(int i = 0; i < b; i ++)
		{
			prod = prod.multiply(base);
		}
		
		return prod;
	}
}
