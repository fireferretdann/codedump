import java.util.ArrayList;

public class Prob50
{
	public static ArrayList<Integer> primes = new ArrayList<Integer>();
	public static final int LIMIT = 1000000;
	
	public static void main(String[] args)
	{
		prepPrimesTo(LIMIT);
		System.out.println("primes prepped");
		
		int longestSumLength = 0;
		long longestSum = 0;
		for(int start = 0; start < primes.size()-1; start ++)
		{
			for(int end = start+1; end < primes.size(); end ++)
			{
				if(longestSumLength < end-start)
				{
					long sum = sumPrimes(start, end);
					if(sum < LIMIT)
					{
						if(isPrime((int)sum))
						{
							longestSum = sum;
							longestSumLength = end-start;
							
							
							System.out.println();
							System.out.println("Length: " + longestSumLength);
							System.out.println("Sum: " + longestSum);
						}
					}
					else
					{
						break;
					}
				}
			}
		}
		
		System.out.println();
		System.out.println("Length: " + longestSumLength);
		System.out.println("Sum: " + longestSum);
		}
	
	public static long sumPrimes(int start, int end)
	{
		long sum = 0;
		for(int i = 0; i <= end-start; i++)
			sum += primes.get(start + i);
		return sum;
	}
	
	public static void prepPrimesTo(int maxPrime)
	{
		primes.add(2);
		primes.add(3);
		
		for(int p = 5; p <= maxPrime; p += 6)
		{
			isPrime(p);
			isPrime(p+2);
		}
	}
	
	public static boolean isPrime(int n)
	{
		if(primes.contains(n))
			return true;
		if(n%2 == 0)
			return false;
		if(n%3 == 0)
			return false;
		
		for(int p : primes)
		{
			if(n%p == 0)
				return false;
		}
		
		primes.add(n);
		return true;
	}
}