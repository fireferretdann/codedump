import java.util.ArrayList;

public class Prob58
{
	public static ArrayList<ArrayList<Long>> spiral = new ArrayList<ArrayList<Long>>();
	
	public static void main(String[] args)
	{
		int sideLength = 1;
		int total = 1;
		int hits = 0;
		
		do
		{
			sideLength += 2;
			total += 4;
			int sideSquared = sideLength*sideLength;
			if(sideSquared < 0)
			{
				System.err.println("negative number with sideLength: " + sideLength);
				System.exit(9001);
			}
			
			if(isPrime(sideSquared))
				hits ++;
			if(isPrime(sideSquared-1*(sideLength-1)))
				hits ++;
			if(isPrime(sideSquared-2*(sideLength-1)))
				hits ++;
			if(isPrime(sideSquared-3*(sideLength-1)))
				hits ++;
			
			System.out.println(hits/(double)total);
		} while(hits/(double)total >= .1);
		
		
		System.out.println(sideLength);
		
		
		/* This was too slow and i realized a better way
		spiral.add(new ArrayList<Long>());
		spiral.get(0).add(1l);
		System.out.println(diagonalPercentage());
		System.out.println(spiral.size()*spiral.size());
		System.out.println();
		
		do
		{
			addLayer();
			//System.out.println(spiral.size());
			//System.out.println(diagonalPercentage());
			//System.out.println(spiral.size()*spiral.size());
			//System.out.println();
		} while(diagonalPercentage() >= .1);
		
		
		System.out.println(spiral.size());*/
	}
	
	public static double diagonalPercentage()
	{
		int sideLength = spiral.size();
		int count = 0;
		
		for(int i = 0; i < sideLength; i ++)
		{
			if(isPrime(spiral.get(i).get(i)))
				count ++;
			if(isPrime(spiral.get(i).get(sideLength-i-1)))
				count ++;
		}
		
		//System.out.println(count + "/" + (2*sideLength - 1));
		
		return (double)count/(double)(2*sideLength - 1);
	}
	
	public static void addLayer()
	{
		int oldSideLength = spiral.size();
		long n = 1 + oldSideLength*oldSideLength; // Counts through numbers
		//long end = (1 + oldSideLength)*(1 + oldSideLength);
		
		// Up the right
		for(int i = oldSideLength - 1; i >= 0; i --)
		{
			spiral.get(i).add(n++);
		}
		
		// Left the top
		spiral.add(0, new ArrayList<Long>());
		for(int i = 0; i < oldSideLength+1; i ++)
		{
			spiral.get(0).add(0, n++);
		}
		
		// Down the left
		for(int i = 0; i < oldSideLength+1; i ++)
		{
			spiral.get(i).add(0, n++);
		}
		
		spiral.add(new ArrayList<Long>());
		for(int i = 0; i < oldSideLength+2; i ++)
		{
			spiral.get(oldSideLength+1).add(n++);
		}
	}
	
	public static void print()
	{
		for(ArrayList<Long> row : spiral)
		{
			for(long i : row)
			{
				System.out.print(i + "\t");
			}
			System.out.println();
		}
	}
	
	
	public static boolean isPrime(long n)
	{
		if(n < 2)
			return false;
		if(n == 2  ||  n == 3)
			return true;
		if(n%2 == 0)
			return false;
		if(n%3 == 0)
			return false;
		
		for(long p = 5; p*p <= n; p += 6)
		{
			if(n%p == 0)
				return false;
			if(n%(p+2) == 0)
				return false;
		}
		
		return true;
	}
}