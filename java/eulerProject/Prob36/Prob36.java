import java.util.LinkedList;

public class Prob36
{
	public static void main(String[] args)
	{
		LinkedList<Integer> doublePalindromes = new LinkedList<Integer>();
		
		for(int i = 1; i < 1000000; i ++)
		{
			if(isPalindrome10(i)  &&  isPalindrome2(i))
			{
				doublePalindromes.push(i);
				System.out.println("Adding the double palindrome " + i);
			}
		}
		
		int total = 0;
		for(int i : doublePalindromes)
		{
			total += i;
		}
		
		System.out.println("The total is " + total);
	}
	
	
	public static boolean isPalindrome10(int toTest)
	{
		int base = 10;
		return isPalindromeInBase(toTest, base);
	}
	
	
	public static boolean isPalindrome2(int toTest)
	{
		int base = 2;
		return isPalindromeInBase(toTest, base);
	}
	
	
	public static boolean isPalindromeInBase(int toTest, int base)
	{
		LinkedList<Integer> digits = new LinkedList<Integer>();
		while(toTest != 0)
		{
			digits.add(toTest%base);
			toTest /= base;
		}
		
		while(digits.size() > 1)
		{
			if(digits.peekFirst() == digits.peekLast())
			{
				digits.removeFirst();
				digits.removeLast();
			}
			else
				return false;
		}
		
		return true;
	}
}
