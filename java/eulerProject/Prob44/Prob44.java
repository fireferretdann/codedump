import java.util.HashMap;

/* Using the fact that Pk - Pj = Pa and Pk + Pj = Pb, 
 * we can conclude:
 * 	Pa + Pj = Pk
 * 	Pa + 2Pj = Pb
 * 	Pk + Pj = Pb
 */
public class Prob44
{
	public static HashMap<Long, Long> pentagonalNumbers = new HashMap<Long, Long>();
	
	public static void main(String[] args)
	{
		for(int i = 1; i < 100; i ++)
			if(isPent(i))
				System.out.println(i);
		
		long minD = Long.MAX_VALUE;
		long limit = 1000000;
		while(minD == Long.MAX_VALUE)
		{
			for(long j = 1; generatePent(j) < limit; j++)
			{
				long Pj =  generatePent(j);
				for(long k = j+1; generatePent(k) < limit; k++)
				{
					long Pk =  generatePent(k);
					long diff = Pk-Pj;
					if(diff < minD)
					{
						long Pb = Pk+Pj;
						long Pa = diff;
						
						if(isPent(Pa)  &&  isPent(Pb))
						{
							minD = diff;
							limit = minD;
							System.out.println("new minD: " + minD);
						}
					}
					else
						break;
				}
			}
			
			limit *= 2;
			System.out.println("new limit: " + limit);
		}
		System.out.println("minD: " + minD);
	}
	
	public static boolean isPent(long p)
	{
		if(p == 0)
			return false;
		
		if(pentagonalNumbers.containsValue(p))
			return true;
		
		long n1 = (long)((1 - Math.sqrt(24*p+1))/6);
		long n2 = (long)((1 + Math.sqrt(24*p+1))/6);
		
		return (generatePent(n1) == p  ||  generatePent(n2) == p);
	}
	
	public static long generatePent(long n)
	{
		if(n <= 0)
			return -1;
		if(pentagonalNumbers.containsKey(n))
			return pentagonalNumbers.get(n);
		
		long newPent = n*(3*n-1)/2;
		
		pentagonalNumbers.put(n, newPent);
		
		return newPent;
	}
}