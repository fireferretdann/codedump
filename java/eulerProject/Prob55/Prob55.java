import java.math.BigInteger;

public class Prob55
{
	public static final BigInteger TEN_THOUSAND = new BigInteger("10000");
	public static final BigInteger ONE_HUNDRED = new BigInteger("100");
	
	public static void main(String[] args)
	{
		//System.out.println(addToReverse(new BigInteger("10755470")));
		//System.out.println(isLychrel(new BigInteger("196")));
		int count = 0;
		
		for(long i = 0; i < 10000; i ++)
		{
			if(isLychrel(new BigInteger ("" + i)))
			{
				count ++;
				System.out.println(count + "th lychrel number is " + i);
			}
			
		}
		
		System.out.println(count);
	}
	
	public static boolean isLychrel(BigInteger n)
	{
		if(n.compareTo(BigInteger.ZERO) == 0)
			return false;
		//System.out.println(n);
		for(int i = 0; i < 50; i ++)
		{
			n = addToReverse(n);
			//System.out.println(n);
			if(isPalindrome(n))
				return false;
		}
		
		return true;
	}
	
	
	public static BigInteger addToReverse(BigInteger n)
	{
		long log = (long)Math.log10(n.doubleValue());
		BigInteger powOfTen = new BigInteger("1");
		for(long i = 0; i < log; i ++)
			powOfTen = powOfTen.multiply(BigInteger.TEN);
		
		
		BigInteger reverse = new BigInteger("0");
		BigInteger tempN = new BigInteger(n.toString());
		while(tempN.mod(BigInteger.TEN).compareTo(BigInteger.ZERO) == 0)
		{
			powOfTen = powOfTen.divide(BigInteger.TEN);
			tempN = tempN.divide(BigInteger.TEN); // Removes trailing zeros
		}
		
		while(reverse.compareTo(powOfTen) < 0)
		{
			reverse = reverse.multiply(BigInteger.TEN);
			reverse = reverse.add(tempN.mod(BigInteger.TEN));
			tempN = tempN.divide(BigInteger.TEN);
		}
		
		return n.add(reverse);
	}
	
	public static boolean isPalindrome(BigInteger n)
	{
		long log = (long)Math.log10(n.doubleValue());
		BigInteger powOfTen = new BigInteger("1");
		for(long i = 0; i < log; i ++)
			powOfTen = powOfTen.multiply(BigInteger.TEN);
		
		while(powOfTen.compareTo(BigInteger.ONE) > 0)
		{
			BigInteger[] nByPowOfTen = n.divideAndRemainder(powOfTen);
			n = nByPowOfTen[1];
			BigInteger[] nByTen = n.divideAndRemainder(BigInteger.TEN);
			n = nByTen[0];
			
			if((nByPowOfTen[0]).compareTo(nByTen[1]) != 0)
				return false;
			
			powOfTen = powOfTen.divide(ONE_HUNDRED);
		}
		
		return true;
	}
}