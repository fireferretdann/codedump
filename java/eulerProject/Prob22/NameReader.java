import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

public class NameReader
{
	public static ArrayList<String> getNames()
	{
		ArrayList<String> nameList = new ArrayList<String>();
		String names = "";
		try
		{
			BufferedReader in = new BufferedReader(new FileReader("p022_names.txt"));
			names = in.readLine();
		}
		catch(java.io.IOException e){System.err.println(e);}
		
		int lastComma = -1;
		for(int i = 0; i < names.length(); i ++)
		{
			if(names.charAt(i) == ',')
			{
				nameList.add(names.substring(lastComma+2, i-1));
				lastComma = i;
			}
		}
		nameList.add(names.substring(lastComma+2, names.length()-1));
		
		return nameList; 
	}
}
