import java.util.ArrayList;

public class Prob22
{
	public static void main(String[] args)
	{
		ArrayList<String> names = NameReader.getNames();
		System.out.println("names.size() = " + names.size());
		System.out.println("Last Name:" + names.get(names.size()-1));
		sort(names);
		
		long[] scores = new long[names.size()];
		
		for(long i = 0; i < names.size(); i ++)
		{
			
			// Not sure how they are indexing their lists... assuming first element is 1.
			scores[(int)i] = (i+1)*partialScore(names.get((int)i));
			/*System.out.println(partialScore(names.get((int)i)));
			System.out.println(scores[(int)i]);
			System.out.println();*/
		}
		
		checkSorting(names);
		
		long total = 0;
		for(long i : scores)
			total += i;
		
		System.out.println(total);
		
		System.out.println(partialScore("COLIN"));
		
		System.out.println(names.get(937));
		//for(String s : names)
		//	System.out.println(s);
	}
	
	public static long partialScore(String s)
	{
		long total = 0;
		for(char c : s.toCharArray())
		{
			total += c - 64; // 41 for offset of utf-16
		}
		return total;
	}
	
	public static void sort(ArrayList<String> list)
	{
		quickSort(list, 0, list.size());
	}
	
	public static void checkSorting(ArrayList<String> list)
	{
		for(int i = 0; i < list.size()-1; i ++)
			if(list.get(i).compareTo(list.get(i+1)) >= 0)
				System.out.println("Sorting Problem!");
		System.out.println("Sorted Correctly!");
	}
	
	// Implementation of quicksort using "median of three" pivot selection (not that it matters for this problem, but I'm bored)
	public static void quickSort(ArrayList<String>list, int begin, int end)
	{
		// Base case:
		if(end - begin <= 1)
			return;
		// Trivial case:
		if(end - begin == 2)
		{
			if(list.get(begin).compareTo(list.get(end - 1)) > 0)
				swap(list, begin, end-1);
			return;
		}
		
		
		String first = list.get(begin);
		String mid = list.get((begin+end)/2);
		String last = list.get(end-1);
		
		// Following block finds the pivot position
		String median;
		int pivot;
		if(first.compareTo(mid) * mid.compareTo(last) >= 0)
		{	//Median is mid. Trust me on this.
			median = mid;
			pivot = (begin+end)/2;
		}
		else if((first.compareTo(mid) * first.compareTo(last) >= 0))
		{	//Median is last. Trust me again.
			median = last;
			pivot = end-1;
		}
		else
		{	//Median is first. Try it, you wuss.
			median = first;
			pivot = begin;
		}
		
		// Stashes pivot away
		swap(list, pivot, end-1);
		
		// Following block partitions the sublist:
		int i = begin;
		int j = end-2;
		while(i < j)
		{	// Runs through sublist from both sides and swaps elements where
			// element on right is < pivot and element on left is > pivot.
			if(list.get(i).compareTo(median) <= 0 )		// i <= pivot
				i++;
			else if(list.get(j).compareTo(median) >= 0)	// j >= pivot
				j--;
			else
				swap(list, i, j);
		}
		
		// Puts pivot in its place
		swap(list, end-1, j);
		
		quickSort(list, begin, j);
		quickSort(list, j+1, end);
	}
	
	public static void swap(ArrayList<String> list, int i , int j)
	{
		String temp = list.get(i);
		list.set(i, list.get(j));
		list.set(j, temp);
	}
}
