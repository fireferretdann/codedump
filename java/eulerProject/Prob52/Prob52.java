public class Prob52
{
	public static final int HOW_MANY_MULTIPLES = 6;
	
	public static void main(String[] args)
	{
		int toCheck = 0;
		while(!multiplesHaveSameDigits(++toCheck));
		
		System.out.println(toCheck*1);
		System.out.println(toCheck*2);
		System.out.println(toCheck*3);
		System.out.println(toCheck*4);
		System.out.println(toCheck*5);
		System.out.println(toCheck*6);
		
		// Of fucking course it's the repeating digits of 1/7. duh.
	}
	
	public static boolean multiplesHaveSameDigits(int n)
	{
		char[][] mults = new char[HOW_MANY_MULTIPLES][];
		for(int m = 1; m <= mults.length; m ++)
		{
			mults[m-1] = (""+(m*n)).toCharArray();
		}
		
		for(char[] cs : mults)
		{
			sort(cs);
		}
		
		for(int mult = 1; mult < mults.length; mult ++)
		{
			for(int c = 0; c < mults[0].length; c ++)
			{
				if(mults[0][c] != mults[mult][c])
					return false;
			}
		}
		
		return true;
	}
	
	public static void sort(char[] a)
	{
		for(int i = 0; i < a.length-1; i ++)
		{
			for(int j = i+1; j < a.length; j ++)
			{
				if(a[j] < a[i])
				{ // Swap a[i] and a[j] (in place cause i'm cool)
					a[j] = (char)(a[j]^a[i]);
					a[i] = (char)(a[j]^a[i]);
					a[j] = (char)(a[j]^a[i]);
				}
			}
		}
	}
}