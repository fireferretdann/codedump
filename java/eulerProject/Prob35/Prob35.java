import java.util.TreeSet;

public class Prob35
{
	public static TreeSet<Integer> primes = new TreeSet<Integer>();
	public static TreeSet<Integer> circularPrimes = new TreeSet<Integer>();
	
	public static void main(String[] args)
	{
		int max = 1000000;
		populatePrimes(max);
		
		findCircularPrimes();
		
		System.out.println("There are " + circularPrimes.size() + " circular primes below " + max);
	}
	
	
	public static void findCircularPrimes()
	{
		for(int toTest : primes)
		{
			int numDigits = countDigits(toTest);
			boolean isCircularPrime = true;
			
			for(int i = 1; i < numDigits; i ++)
			{
				int currentCycle = cycleBy(toTest, i);
				if( !primes.contains(currentCycle))
				{
					isCircularPrime = false;
					break;
				}
			}
			
			if(isCircularPrime)
			{
				circularPrimes.add(toTest);
			}
		}
	}
	
	
	public static int cycleBy(int toCycle, int cycleAmount)
	{
		int multOfTen = 1;
		for(int i = 1; i < countDigits(toCycle); i ++)
		{
			multOfTen *= 10;
		}
		
		for(int i = 0; i < cycleAmount; i ++)
		{
			int cyclingDigit = toCycle%10;
			toCycle /=10;
			toCycle += cyclingDigit*multOfTen;
		}
		
		return toCycle;
	}
	
	
	public static void populatePrimes(int max)
	{
		primes.add(2);
		primes.add(3);
		
		for(int i = 6; i < max; i += 6)
		{
			if(isPrime(i-1))
				primes.add(i-1);
			if(isPrime(i+1))
				primes.add(i+1);
		}
	}
	
	
	public static boolean isPrime(int toTest)
	{
		for(int p : primes)
		{
			if(p*p > toTest)
				return true;
			
			if(toTest%p == 0)
				return false;
		}
		
		System.out.println("Huh? " + toTest + " broke something...");
		return true;
	}
	
	
	public static int countDigits(int i)
	{
		return ("" + i).toCharArray().length;
	}
}
