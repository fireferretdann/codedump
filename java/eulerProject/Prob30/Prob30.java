
public class Prob30
{
	public static void main(String[] args)
	{
		int end = 6 * pow(9, 5);	//Not sure about this maximum...
		int ans = 0;
		
		for(int toCheck = 2; toCheck < end; toCheck++)
		{
			String numString = "" + toCheck;
			char[] numChars = numString.toCharArray();
			
			int total = 0;
			for(char c : numChars)
			{
				int digit = (int) c - 48;
				total += pow(digit, 5);
			}
			
			if(total == toCheck)
			{
				System.out.println("New valid number: " + toCheck);
				ans += toCheck;
			}
		}
		
		System.out.println("The final answer is: " + ans);
	}
	
	public static int pow(int base, int pow)
	{
		int prod = 1;
		for(int i = 0; i < pow; i++)
		{
			prod *= base;
		}
		
		return prod;
	}
}
