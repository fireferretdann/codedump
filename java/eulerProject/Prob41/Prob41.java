public class Prob41
{
	// I am first trying to approach this under the assumption that the largest n-digit pan-digital prime is 9 digits, and will revisit if neccesary 
	// I'm dumb. All 9 digit pan-digital numbers are divisible by 3.
	
	public static void main(String[] args)
	{
		for(int toTest = 987654321; toTest >= 2; toTest--)
		{
			if(isPandigital(toTest))
			{
				if(isPrime(toTest))
				{
					System.out.println(toTest);
					System.exit(0);
				}
			}
		}
	}
	
	public static boolean isPrime(int n)
	{
		if(n%2 == 0  ||  n%3 == 0)
		{
			if(n == 2  ||  n == 3)
				return true;
			else
				return false;
		}
		
		double sqrt = Math.sqrt(n);
		int toCheck = 5;
		
		while(toCheck <= sqrt)
		{
			if(n%toCheck == 0  ||  n%(toCheck+2) == 0)
				return false;
			else
				toCheck += 6;
		}
		
		return true;
	}
	
	public static boolean isPandigital(int n)
	{
		int digits = ("" + n).length();
		
		boolean[] digitsPresent = new boolean[digits];
		for(int p = digits-1; p >= 0; p --)
		{
			int digitMinusOne = n / (int)Math.pow(10, p) - 1;

			if(digitMinusOne == -1  ||  digitMinusOne >= digits)
			{
				return false;
			}
			else if(digitsPresent[digitMinusOne])
				return false;
			else
				digitsPresent[digitMinusOne] = true;
			n %= Math.pow(10, p)*(digitMinusOne+1);
		}
		return true;
	}
}