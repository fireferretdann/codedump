import java.math.*;

public class Prob26
{
	public static void main(String[] args)
	{
		MathContext twoThousand = new MathContext(3000, RoundingMode.FLOOR);
		BigDecimal one = new BigDecimal(1, twoThousand);
		BigDecimal counter = new BigDecimal(2, twoThousand);
		BigDecimal end = new BigDecimal(1000, twoThousand);
		BigDecimal temp;
		int maxRepeats = 0;
		int maxD = 0;
		
		/*String toMatch = "0.25121212121212121212121212121212121212121212121212";
		String regex = "^.*(" + toMatch.substring(10, 10+2) + ")+$";
		System.out.println("test:");
		System.out.println("regex: " + regex);
		System.out.println(toMatch.matches(regex));
		toMatch = "0.1234567897549358374889929211";
		System.out.println(toMatch.matches(regex));//*/
		
		
		while(counter.compareTo(end) < 0)
		{
			temp = one.divide(counter, twoThousand);
			
			int repeats = checkRepeats(temp);
			if(repeats >= maxRepeats)
			{
				maxRepeats = repeats;
				maxD = counter.intValue();
			}
			
			System.out.println("1/" + counter.intValue() + " has a repeat of length: " + repeats);
			
			counter = counter.add(one);
		}
		
		System.out.println("\nThe solution is: " + maxD);//*/
	}
	
	public static int checkRepeats(BigDecimal n)
	{
		int repeats = 0;
		
		String numString = n.toPlainString();
		if(numString.length() < 1000)
			return 1;
		numString = numString.substring(numString.indexOf('.') + 1, numString.length());
		
		for(int start = 0; start < numString.length()/2; start ++)
		{
			//System.out.println("Starting at character: " + start);
			for(int repeatLength = 1; repeatLength < (numString.length() - start)/2; repeatLength ++)
			{
				String regex = "^(" + numString.substring(start, start+repeatLength) + ")+$";
				if(numString.substring(start, numString.length()).matches(regex))
				{
					
					return repeatLength;
				}
			}
		}
		
		System.err.println("No repeating decimal found.");
		return repeats;
	}
}
