public class Prob45
{
	public static void main(String[] args)
	{
		for(long i = 1; i < 100; i++)
			if(isPent(i))
				System.out.println(i);
		for(long i = 1; i < 100; i++)
			if(isHex(i))
				System.out.println(i);
		boolean notFound = true;
		long i = 286;
		while(notFound)
		{
			long t = generateTri(i);
			if(isHex(t)  &&  isPent(t))
			{
				System.out.println(t);
				notFound = false;
			}
			i++;
		}
	}
	
	public static long generateTri(long n)
	{
		return n*(n+1)/2;
	}
	
	public static boolean isPent(long p)
	{
		if(p == 0)
			return false;
		
		long n1 = (long)((1 - Math.sqrt(24*p+1))/6);
		long n2 = (long)((1 + Math.sqrt(24*p+1))/6);
		
		if(n1 > 0  &&  n1*(3*n1-1)/2 == p)
			return true;
		if(n2 > 0  &&  n2*(3*n2-1)/2 == p)
			return true;
		return false;
	}
	
	public static boolean isHex(long h)
	{
		if(h == 0)
			return false;
		
		long n1 = (long)((1 - Math.sqrt(8*h+1))/4);
		long n2 = (long)((1 + Math.sqrt(8*h+1))/4);
		
		if(n1 > 0  &&  n1*(2*n1-1) == h)
			return true;
		if(n2 > 0  &&  n2*(2*n2-1) == h)
			return true;
		return false;
	}
}