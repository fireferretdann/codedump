public class Fraction implements Comparable
{
	public long numerator;
	public long denominator;
	
	public Fraction(long n, long d)
	{
		numerator = n;
		denominator = d;
	}
	
	public double getValue()
	{
		return (double)numerator/(double)denominator;
	}
	
	public boolean equals(Object o)
	{
		return (o != null  &&  o instanceof Fraction && ((Fraction)o).getValue() == this.getValue());
	}
	
	public int compareTo(Object o)
	{
		if(o != null  &&  o instanceof Fraction)
		{
			if(((Fraction)o).getValue() < this.getValue())
				return 1;
			else if(((Fraction)o).getValue() == this.getValue())
				return 0;
			else
				return -1;
		}
		else
			return -1;
	}
}