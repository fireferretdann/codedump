import java.util.TreeSet;

public class Prob71
{
	public static void main(String[] args)
	{
		//TreeSet<Fraction> fractions = new TreeSet<Fraction>();
		Fraction closest = new Fraction(2, 7);
		for(long d = 8; d <= 1000000; d ++)
		{
			for(long n = (d*closest.numerator)/closest.denominator; n < (3*d)/7; n ++)
			{
				closest = new Fraction(n, d);
				System.out.println(closest.numerator + "/" + closest.denominator);
			}
		}
		
		System.out.println(closest.numerator + "/" + closest.denominator);
		
		// Fraction result = fractions.lower(new Fraction(3, 7));
		// System.out.println(result.numerator + "/" + result.denominator);
	}
}