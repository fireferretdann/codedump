import java.io.BufferedReader;
import java.io.FileReader;
import java.util.TreeSet;

public class PrimeGenerator
{
	public static TreeSet<Long> primes = readPrimes();
	
	public static void main(String[] args)
	{
		System.out.println();
		primes.add(2l);
		primes.add(3l);
		long suspect = primes.last();
		suspect = suspect - (suspect%6) + 5; // Next possible prime(ish)
		while(true)
		{
			if(isPrime(suspect))
			{
				System.out.println(suspect);
				primes.add(suspect);
			}
			if(suspect+2 > 0  &&  isPrime(suspect+2))
			{
				System.out.println(suspect+2);
				primes.add(suspect+2);
			}
			
			suspect += 6;
			if(suspect < 0)
				break;
		}
	}
	
	public static boolean isPrime(long n)
	{
		for(long p : primes)
		{
			if(n%p == 0)
				return false;
			if(p*p > n)
				return true;
		}
		return true;
	}
	
	public static boolean isPrime(long n, TreeSet<Long> ps)
	{
		for(long p : ps)
		{
			if(n%p == 0)
				return false;
			if(p*p > n)
				return true;
		}
		return true;
	}
	
	public static TreeSet<Long> readPrimes()
	{
		TreeSet<Long> ps = new TreeSet<Long>();
		try
		{
			BufferedReader in = new BufferedReader(new FileReader("primes.txt"));
			
			String line = in.readLine();
			while(line != null)
			{
				ps.add(Long.parseLong(line));
				line = in.readLine();
			}
			
			in.close();
		}
		catch(java.io.IOException e){System.err.println(e);} // Add creating a file functionality for completeness.
		
		ps.pollLast();
		
		return ps;
	}
}