import java.util.LinkedList;

public class Prob64
{
	public static double ERR = .000001;
	
	public static void main(String[] args)
	{
		int count = 0;
		for(int i = 2; i <= 10000; i ++)
		{
			int period = getPeriod(i, ERR);
			System.out.println("Period of " + i + ":\t"+ period);
			if(period%2 == 1)
				count ++;
		}
		
		System.out.println(count);
	}
	
	public static int getPeriod(int square, double err)
	{
		double target = Math.sqrt(square)%1;
		if(target == 0)
			return 0;
		
		LinkedList<Integer> terms = new LinkedList<Integer>();
		LinkedList<Double> rems = new LinkedList<Double>();
		
		double rem = target;
		while(!nearlyContains(rems, rem, err))
		{
			rems.add(rem);
			
			rem = 1.0/rem;
			terms.addLast((int)rem);
			rem = rem - (int)rem;
		}
		
		if(terms.size() < 2.3*Math.sqrt(square))
			return terms.size();
		else
			return getPeriod(square, err*1.02);
	}
	
	public static boolean nearlyContains(LinkedList<Double> a, double d, double err)
	{
		for(double i : a)
		{
			if(d < i + err  &&  d > i - err)
				return true;
		}
		return false;
	}
	
	public static double continuousFraction(LinkedList<Integer> terms)
	{
		terms = (LinkedList<Integer>)terms.clone();
		if(terms.size() == 0)
			return 0;
		else if(terms.size() == 1)
			return 1.0/terms.getFirst();
		else
			return  1.0/(terms.pollFirst() + continuousFraction(terms));
	}
}