
public class Prob28
{
	public static void main(String[] args)
	{
		int sum = 1;
		int toAdd = 1;
		
		for(int layer = 2; layer < 1001; layer += 2)
		{
			for(int i = 0; i < 4; i ++)
			{
				toAdd += layer;
				System.out.println("Adding " + toAdd);
				sum += toAdd;
			}
		}
		
		System.out.println(sum);
	}
}
