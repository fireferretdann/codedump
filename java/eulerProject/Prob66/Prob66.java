public class Prob66
{
	public static void main(String[] args)
	{
		long maxX = 0;
		long maxD = 0;
		for(long D = 0; D <= 1001; D ++)
		{
			if(!isSquare(D))
			{
				long x = getXSolution(D);
				if(x > maxX)
				{
					maxX = x;
					maxD = D;
					System.out.println(D + ": " + getXSolution(D));
				}
			}
		}
		
		System.out.println(maxD);
	}
	
	public static long getXSolution(long D)
	{
		long x = 2;
		while(!isValidSolution(D, x))
			x ++;
		return x;
	}
	
	public static boolean isValidSolution(long D, long x)
	{
		return isSquare((double)(x*x - 1.0)/(double)D);
	}
	
	public static boolean isSquare(double possSqr)
	{
		return 0 == Math.sqrt(possSqr)%1.0;
	}
	
	public static boolean isSquare(long possSqr)
	{
		return isSquare((double)possSqr);
	}
}