public class Prob39
{
	public static final double ROOT_TWO = Math.sqrt(2);
	
	
	public static void main(String[] args)
	{
		int maxCountPerimeter = 0;
		int maxCount = 0;
		for(int n = 1; n <= 1000; n++)
		{
			int countForN = countSolutions(n);
			if(countForN > maxCount)
			{
				maxCount = countForN;
				maxCountPerimeter = n;
			}
		}
		System.out.println(maxCountPerimeter);
	}
	
	public static int countSolutions(int n)
	{
		int count = 0;
		
		for(int a = 1; a < n/(2 + ROOT_TWO); a++) // n/(2 + ROOT_TWO) is maximum value for shortest leg (but that can't be integer sides)
		{
			int b = a;
			while(a*a + b*b < (n-(a+b))*(n-(a+b))) // Condition tests that c given by current triangle is < c given by desired perimeter (the value n-(a+b) is c)
			{
				b ++;
			}
			if(a*a + b*b == (n-(a+b))*(n-(a+b))) // Tests if triangle meets our critereon;
			{
				count ++;
			}
		}
		return count;
	}
}