

public class Prob24
{
	public static void main(String[] args)
	{
		int n = 10;
		int[] intsLeft = new int[n];
		
		for(int i = 0; i < intsLeft.length; i++)
			intsLeft[i] = i;
		
		int place = 1000000;
		place --;
		
		printChooser(intsLeft, place);

		/*for(int place = 0; place < factorial(n); place ++)
		{
			printChooser(intsLeft, place);
			System.out.println();
		}*/
	}
	
	public static void printChooser(int[] ints, int place)
	{
		if(ints.length == 1)
		{
			System.out.println(ints[0]);
			return;
		}	
		int divisor = factorial(ints.length-1);
		System.out.println(ints[place/divisor]);
		
		// Copies all but used
		int[] newInts = new int[ints.length-1];
		for(int i = 0; i < place/divisor; i ++)
			newInts[i] = ints[i];
		for(int i = place/divisor; i < newInts.length; i ++)
			newInts[i] = ints[i+1];
		
		printChooser(newInts, place%divisor);
	}
	
	public static int factorial(int n)
	{
		int prod = 1;
		for(int i = n; i > 1; i --)
			prod *= i;
		return prod;
	}
}
