import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashSet;

public class Prob60
{
	public static HashSet<Integer> primes = readPrimes();
	public static final int primeCount = 12345;
	public static int lastPrime;
	
	public static void main(String[] args)
	{
		primes.remove(2); // Cannot have a 2 as that would create an even number when concatenating
		
		int smallestSum = lastPrime*5;
		
		int[] groupOfPrimes = new int[5];
		
		for(groupOfPrimes[0] = 3; groupOfPrimes[0] <= lastPrime; groupOfPrimes[0] += 2)
		{
			if(sum(groupOfPrimes) < smallestSum  &&  primes.contains(groupOfPrimes[0]))
			{
				System.out.println("New groupOfPrimes[0]: " + groupOfPrimes[0]);
				for(groupOfPrimes[1] = groupOfPrimes[0]; groupOfPrimes[1] > 2; groupOfPrimes[1] -= 2)
				{
					if(sum(groupOfPrimes) < smallestSum  &&  primes.contains(groupOfPrimes[1])  &&  canConcat(groupOfPrimes, 2))
					{
						for(groupOfPrimes[2] = groupOfPrimes[1]; groupOfPrimes[2]  > 2; groupOfPrimes[2] -= 2)
						{
							if(sum(groupOfPrimes) < smallestSum  &&  primes.contains(groupOfPrimes[2])  &&  canConcat(groupOfPrimes, 3))
							{
								for(groupOfPrimes[3] = groupOfPrimes[2]; groupOfPrimes[3]  > 2; groupOfPrimes[3] -= 2)
								{
									if(sum(groupOfPrimes) < smallestSum  &&  primes.contains(groupOfPrimes[3])  &&  canConcat(groupOfPrimes, 4))
									{
										for(groupOfPrimes[4] = groupOfPrimes[3]; groupOfPrimes[4]  > 2; groupOfPrimes[4] -= 2)
										{
											if(sum(groupOfPrimes) < smallestSum  &&  primes.contains(groupOfPrimes[4])  &&  canConcat(groupOfPrimes, 5))
											{
												smallestSum = sum(groupOfPrimes);
												System.out.println("New smallestSum: " + smallestSum);
												for(int i = 0; i < groupOfPrimes.length; i ++)
													System.out.println("prime " + i + ": " + groupOfPrimes[i]);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		System.out.println(smallestSum);
		System.out.println("disregard if " + (lastPrime*5));
	}
	
	public static boolean canConcat(int[] a, int count)
	{
		for(int i = 0; i < count - 1; i ++)
		{
			for(int j = i+1; j < count; j ++)
			{
				int toTest1 = Integer.parseInt(a[i] + "" + a[j]);
				int toTest2 = Integer.parseInt(a[j] + "" + a[i]);
				if(!isPrime(toTest1))
					return false;
				if(!isPrime(toTest2))
					return false;
			}
		}
		return true;
	}
	
	public static boolean isPrime(int n)
	{
		if(primes.contains(n))
			return true;
		if(n < lastPrime)
			return false;
		
		if(n%2 == 0)
			return false;
		for(int i : primes)
			if(n%i == 0)
				return false;
			
		return true;
	}
	
	public static int sum(int[] a)
	{
		int sum = 0;
		for(int n : a)
			sum += n;
		return sum;
	}
	
	public static HashSet<Integer> readPrimes()
	{
		HashSet<Integer> ps = new HashSet<Integer>(primeCount);
		try
		{
			BufferedReader in = new BufferedReader(new FileReader("..\\intPrimes.txt"));
			
			String line = in.readLine();
			while(line != null  &&  line != ""  &&  ps.size() < primeCount)
			{
				lastPrime = Integer.parseInt(line);
				ps.add(lastPrime);
				line = in.readLine();
			}
			
			in.close();
		}
		catch(java.io.IOException e){System.err.println(e);} // Add creating a file functionality for completeness.
		
		return ps;
	}
}