import java.util.TreeSet;

public class Prob49
{	// I realized partway through that i misunderstood the problem, so im doing it a really inefficient way. sorry :/
	public static void main(String[] args)
	{
		for(int i = 1000; i < 9999; i++)
		{
			if(isPrime(i))
			{
				TreeSet<Integer> primePerms = new TreeSet<Integer>();
				
				int[] perms = getPermutations(i);
				for(int perm : perms)
				{
					if(isPrime(perm))
					{
						primePerms.add(perm);
					}
				}
				
				if(primePerms.size() >= 3)
				{
					checkPrimePerms(primePerms);
				}
			}
		}
	}
	
	public static void checkPrimePerms(TreeSet<Integer> pps)
	{
		int a = pps.first();
		while(a != pps.lower(pps.last()))
		{
			int b = pps.higher(a);
			while(b != pps.last())
			{
				if(pps.contains(b + (b-a)))
				{
					System.out.println(a);
					System.out.println(b);
					System.out.println(b + (b-a));
					System.out.println();
				}
				b = pps.higher(b);
			}
			a = pps.higher(a);
		}
	}
	
	public static int[] getPermutations(int n)
	{
		int[] perms = new int[24];
		
		String[] digits = new String[4];
		String s = "" + n;
		for(int i = 0; i < 4; i ++)
			digits[i] = "" + s.charAt(i);
		
		int pos = 0;
		
		// Heap's algorithm adapted from wikipedia's pseudocode:
		int[] c = new int[n];

		perms[pos++] = n;
		
		int i = 0;
		while(i < 4)
		{
			if(c[i] < i)
			{
				if(i%2 == 0)
					swap(digits, 0, i);
				else
					swap(digits, c[i], i);
				perms[pos++] = getInt(digits);
				c[i] += 1;
				i = 0;
			}
			else
			{
				c[i] = 0;
				i += 1;
			}
		}
		// End of Heap's algorithm
		
		return perms;
	}
	
	public static int getInt(String[] digits)
	{
		String result = "";
		for(String d : digits)
			result += d;
		
		return Integer.parseInt(result);
	}
	
	public static boolean isPrime(int n)
	{
		if(n%2 == 0)
			return false;
		if(n%3 == 0)
			return false;
		
		for(int p = 5; p*p <= n; p += 6)
		{
			if(n%p == 0)
				return false;
			if(n%(p+2) == 0)
				return false;
		}
		
		return true;
	}
	
	public static void swap(String[] a, int i, int j)
	{
		String temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}
}