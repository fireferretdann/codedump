/*
//	This class is used to handle simplifying fractions properly.
//	This class also tests for the desired pseudo-simplification property.
*/

public class Fraction
{
	// Stores the numerator and denominator
	int numer;
	int denom;
	
	// For easier simplification when numerator is zero	
	final static Fraction ZERO = new Fraction(0, 1);
	
	// For testing this class
	public static void main(String args[])
	{
		// Tries to simplify all fractions with denominator up to max and prints them
		int max = 8;
		for(int i = 1; i <= max; i ++)
		{
			for(int j = 0; j <=i; j++)
			{
				Fraction toTest = new Fraction(j, i);
				Fraction simp = toTest.simplify();
				
				System.out.println(toTest + "\tsimplified is:\t" + simp);
			}
		}
	}
	
	
	// Creates a fraction
	public Fraction(int numerator, int denominator)
	{
		numer = numerator;
		denom = denominator;
	}
	
	
	// A method to check for pseudo-simplification property
	// Returns a pair of fractions or an empty array based on this fraction
	public Fraction[] pseudoSimplify()
	{
		// Makes the numerator and denominator strings for easier manipulation
		String topString = "" + numer;
		String bottomString = "" + denom;
		
		// Tests each digit of the numerator to see if the denominator also has it
		for(char digit : topString.toCharArray())
		{
			if(digit != '0')
			{
				if(bottomString.contains("" + digit))
				{
					// Removes the digit if they both have it
					String newTop = topString.replaceFirst("" + digit, "");
					String newBottom = bottomString.replaceFirst("" + digit, "");
					
					// Creates a new fraction for testing equality
					Fraction pseudo = new Fraction(Integer.parseInt(newTop), Integer.parseInt(newBottom));
					
					// Tests if the two fractions are equal, and thus, the pseudo-similarity property is fulfilled
					if(pseudo.equals(this))
					{
						Fraction toReturn[] = new Fraction[2];
						toReturn[0] = this;
						toReturn[1] = pseudo;
						return toReturn;
					}
				}
			}
		}
		return new Fraction[0];
	}
	
	
	// A method to simplify this fraction to simplest form
	public Fraction simplify()
	{
		// Handles fractions equal to zero
		if(numer == 0)
			return ZERO;
		
		
		int numerator = numer;
		int denominator = denom;
		
		// Search through possible factors to simplify with
		for(int factor = 2; factor <= numer; factor ++)
		{
			// Simplify partially if the factor is valid and repeat that factor again
			if(numerator%factor == 0  &&  denominator%factor == 0)
			{
				numerator /= factor;
				denominator /= factor;
				
				factor --;
			}
		}
		
		Fraction simplified = new Fraction(numerator, denominator);
		return simplified;
	}
	
	
	// Returns the fraction as a string in numerator/denominator form
	// For example two thirds would return the string "2/3" (without quotes)
	public String toString()
	{
		return numer + "/" + denom;
	}
	
	
	// Tests if this fraction is equal to another
	public boolean equals(Fraction that)
	{
		// Simplify both fractions and test if their numerators and denominators are equal.
		Fraction me = this.simplify();
		Fraction them = that.simplify();
		
		return me.numer == them.numer  &&  me.denom == them.denom;
	}
}
