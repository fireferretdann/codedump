/*
	I am trying a new style of coding for this problem.
	This style of coding involves writing all the comments BEFORE the code.
	This will be a challenge for me.
*/
import java.util.ArrayList;

public class Prob33
{
	public static void main(String[] args)
	{
		// Start with the initial assumption that the denominators are all less than 100.
		int maxDenom = 99;
		ArrayList<Fraction[]> listOfPairs = new ArrayList<Fraction[]>();
		
		// Go through all two-digit fractions testing for pseudo-simplification property and save them if they have it
		for(int denominator = 11; denominator < maxDenom; denominator ++)
		{
			for(int numerator = 10; numerator < denominator; numerator ++)
			{
				Fraction toTest = new Fraction(numerator, denominator);
				Fraction[] currentPair = toTest.pseudoSimplify();
				
				if(currentPair.length == 2)
					listOfPairs.add(currentPair);
			}
		}
		
		// Print the fractions that have the desired property for me to sort through and remove trivial examples
		for(Fraction[] pair : listOfPairs)
		{
			System.out.println(pair[0] + " = " + pair[1] + " = " + pair[1].simplify());
		}
		
	}
}
