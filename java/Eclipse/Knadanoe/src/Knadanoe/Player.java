package Knadanoe;

import java.awt.Color;
import java.awt.Graphics;

public class Player extends GameObject
{

	public Player(int x, int y, ID id)
	{
		super(x, y, id);
	}

	@Override
	public void tick() {
		x += velX;
		y += velY;
	}

	@Override
	public void render(Graphics g) {
		if(id == ID.Player)
			g.setColor(Color.white);
		else
			g.setColor(Color.lightGray);
		g.fillRect(x,  y,  25, 25);
	}

}
