package Knadanoe;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyInput extends KeyAdapter
{
	private Handler handler;
	
	public KeyInput(Handler handler)
	{
		this.handler = handler;
	}
	
	public void keyPressed(KeyEvent e)
	{
		int key = e.getExtendedKeyCode();
		
		for(GameObject obj : handler.objects)
		{
			if(obj.getID() == ID.Player)
			{
				// Key events for P1

				if(key == KeyEvent.VK_W)
					obj.setVelY(-1);
				if(key == KeyEvent.VK_S)
					obj.setVelY(1);
				if(key == KeyEvent.VK_A)
					obj.setVelX(-1);
				if(key == KeyEvent.VK_D)
					obj.setVelX(1);
			}
		}
	}
	
	public void keyReleased(KeyEvent e)
	{
		int key = e.getExtendedKeyCode();
		
		for(GameObject obj : handler.objects)
		{
			if(obj.getID() == ID.Player)
			{
				// Key events for P1

				if(key == KeyEvent.VK_W)
					obj.setVelY(0);
				if(key == KeyEvent.VK_S)
					obj.setVelY(0);
				if(key == KeyEvent.VK_A)
					obj.setVelX(0);
				if(key == KeyEvent.VK_D)
					obj.setVelX(0);
			}
		}
	}
}
