import java.util.Random;

public class Beastie
{
	public static Random rand = new Random();
	
	private String name;
	
	private int accuracyScore;
	private int attackScore;
	private int constitutionScore;
	private int defenseScore;
	private int dodgeScore;
	private int hpMax;
	private int luckScore;
	private int speedScore;
	
	private int hpCurrent;
	private double speedEffective;
	private double luckEffective;
	private double defenseEffective;
	
	public static void main(String args[])
	{
		int[] stats = {10,10,10,10,10,100,10,10,};
		Beastie bee = new Beastie("Bee", stats);
		System.out.println(bee);
		
		while(bee.getHealth() > 0)
		{
			bee.attack(bee);
			System.out.println(bee);
		}
	}
	
	public Beastie(String beastieName, int[] stats)
	{
		accuracyScore = stats[0];
		attackScore = stats[1];
		constitutionScore = stats[2];
		defenseScore = stats[3];
		dodgeScore = stats[4];
		hpMax = stats[5];
		luckScore = stats[6];
		speedScore = stats[7];
		
		hpCurrent = hpMax;
		speedEffective = 100-speedScore;
		luckEffective = ((double)luckScore)/100.0;
		defenseEffective = ((double)defenseScore)/100.0;
		
		name = beastieName;
	}
	
	public int getHealth()
	{
		return hpCurrent;
	}
	
	public int getDodge()
	{
		return dodgeScore;
	}
	
	public double getLuck()
	{
		return luckEffective;
	}
	
	public void attack(Beastie target)
	{
		double hitChance = (100.0 + accuracyScore - target.getDodge() + (this.getLuck()/target.getLuck()) - (target.getLuck()/this.getLuck()))/200.0;
		double r = rand.nextDouble();
		//System.out.println("Hit chance: " + hitChance);
		//System.out.println("random number: " + r);
		if(r > hitChance)
		{
			int damage = attackScore;
			damage += (int)(rand.nextDouble()/luckEffective);
			
			target.beHit(damage);
		}
		else
		{
			System.out.println("Attack missed");
		}
	}
	
	public void beHit(int damage)
	{
		hpCurrent -= (1-defenseEffective)*damage + 1;
	}
	
	public String toString()
	{
		String s = "";
		s += name + "\n";
	
		s += accuracyScore + "\n";
		s += attackScore + "\n";
		s += constitutionScore + "\n";
		s += defenseScore + "\n";
		s += dodgeScore + "\n";
		s += hpCurrent + "/" + hpMax + "\n";
		s += luckScore + "\n";
		s += speedScore + "\n";
		
		return s;
	}
}