import java.awt.Point;
//import java.util.Math;

public class World
{
	int w;
	int h;
	Cell[][] grid;
	boolean wrapping = false;
	boolean diagonalNeighbors = false;
	
	public static void main(String[] args)
	{
		World w = new World(150, 35);
		w.fillGrid(new Cell());
		Cell c = new Cell('b');
		w.add(c, 2, 14);
		w.display();
		System.out.println(c);
		System.out.println(w.locate(c));
	}
	
	public World()
	{
		w = 20;
		h = 20;
		grid = new Cell[w][h];
	}
	
	public World(int width, int height)
	{
		w = width;
		h = height;
		grid = new Cell[w][h];
	}
	
	public void remove(Cell c)
	{
		this.add(new Cell(), c.getX(), c.getY());
	}
	
	public void add(Cell c, int x, int y)
	{
		grid[x][y] = c;
		c.added(this, x, y);
	}
	
	public Cell get(Point p)
	{
		return grid[p.x][p.y];
	}
	
	public Cell get(int x, int y)
	{
		return grid[x][y];
	}
	
	public Point locate(Cell c)
	{
		for(int x = 0; x < w; x ++)
		{
			for(int y = 0; y < h; y ++)
			{
				if(grid[x][y] instanceof Cell && grid[x][y].equals(c))
				{
					return new Point(x, y);
				}	
			}
		}
		return null;
	}
	
	public Cell[] getNeighbors(Cell c)
	{
		return getNeighbors(c.getX(), c.getY());
	}
	
	public Cell[] getNeighbors(Point l)
	{
		return getNeighbors(l.x, l.y);
	}
	
	public Cell[] getNeighbors(int x, int y)
	{
		Cell res[];
		if(diagonalNeighbors)
		{
			res = new Cell[8];
			int x1 = Math.max(0, x-1);
			int x2 = Math.min(w, x+1);
			int y1 = Math.max(0, y-1);
			int y2 = Math.min(h, y+1);
			int count = 0;
			
			for(int j = y1; j <= y2; j++)
				for(int i = x1; i <= x2; i++)
					res[count++] = grid[i][j];
		}
		else
		{
			res = new Cell[4];
			int count = 0;
			if(x-1 >= 0)
				res[count++] = grid[x-1][y];
			if(x+1 < w)
				res[count++] = grid[x+1][y];
			if(y-1 >= 0)
				res[count++] = grid[x][y-1];
			if(y+1 < h)
				res[count++] = grid[x][y+1];
		}
		
		return res;
	}
	
	public String toString()
	{
		String res = "";
		for(int y = 0; y < h; y ++)
		{
			res += '|';
			for(int x = 0; x < w; x++)
			{
				if(grid[x][y] instanceof Cell)
					res += grid[x][y].character();
				else
					res += '?';
			}
			res += "|\n";
		}
		return res;
	}
	
	public void display()
	{
		System.out.println(this);
	}
	
	public void fillGrid(Cell c)
	{
		for(int x = 0; x < grid.length; x++)
		{
			for(int y = 0; y < grid[0].length; y ++)
			{
				add(c.clone(), x, y);
			}
		}
	}
}
