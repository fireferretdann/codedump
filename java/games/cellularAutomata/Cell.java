public class Cell
{
	char c;
	World w;
	boolean inWorld;
	int x;
	int y;
	
	public Cell()
	{
		c = ' ';
		w = null;
		inWorld = false;
		x = -1;
		y = -1;
	}
	
	public Cell(char character)
	{
		c = character;
		w = null;
		inWorld = false;
		x = -1;
		y = -1;
	}
	
	public void added(World world, int xPos, int yPos)
	{
		w = world;
		inWorld = true;
		x = xPos;
		y = yPos;
	}
	
	public Cell[] getNeighbors()
	{
		if(inWorld)
			return w.getNeighbors(this);
		return (new Cell[0]);
	}
	
	public int getX()
	{
		return x;
	}
	
	public int getY()
	{
		return y;
	}
	
	public char character()
	{
		return c;
	}
	
	public Cell clone()
	{
		return new Cell(c);
	}
	
	public String toString()
	{
		return "\"" + c + "\" Cell at (" + x + ", " + y + ")";
	}
	
	public void step(){}
}
