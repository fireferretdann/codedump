import java.util.Scanner;

public class BattleWorld extends World
{
	public Cell[][] tempGrid;
	public BattleWorld(int width, int height)
	{
		super(width, height);
		tempGrid = new Cell[w][h];
	}
	
	public BattleWorld()
	{
		super(150, 35);
		tempGrid = new Cell[w][h];
	}
	
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		BattleWorld world = new BattleWorld();
		world.fillGrid(new Cell());
		BattleCell a = new BattleCell('a');
		world.add(a, 50, 20);
		
		world.display();
		do
		{
			world.step();
			world.display();
		}
		while(input.nextLine().isEmpty());
	}
	
	public void step()
	{
		deepCopy(grid, tempGrid);
		for(Cell[] row : tempGrid)
			for(Cell c : row)
				if(c != null)
					c.step();
	}
	
	public void deepCopy(Cell[][] src, Cell[][] dest)
	{
		for(int i = 0; i < src.length; i ++)
			for(int j = 0; j < src[i].length; j ++)
				dest[i][j] = src[i][j];
	}
}
