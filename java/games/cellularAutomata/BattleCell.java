import java.util.Random;

public class BattleCell extends Cell
{
	private Random rand = new Random();
	private double reproductiveChance;
	private double deathChance;
	
	public BattleCell()
	{
		super();
		reproductiveChance = .25;
		deathChance = .05;
	}
	
	public BattleCell(char character)
	{
		super(character);
		reproductiveChance = .25;
		deathChance = .05;
	}
	
	public void step()
	{
		for(Cell other : getNeighbors())
			if(other != null && other.character()==' ' && rand.nextDouble() < reproductiveChance)
				w.add(new BattleCell(this.c), other.getX(), other.getY());
		
		if(rand.nextDouble() < deathChance)
			w.remove(this);
	}
	
	public String toString() { return super.toString(); }
}
