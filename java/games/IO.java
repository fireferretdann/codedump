import java.util.Scanner;

public class IO
{
	public final static Scanner input = new Scanner(System.in);
	public final static java.io.PrintStream output = System.out;
	
	// Testing the IO class
	public static void main(String args[])
	{
		IO.print("This is a test of the IO class");
		IO.print();
		
		IO.print("Enter a character now: ");
		IO.print(IO.getChar());
	}
	
	// Gets the next line from the input
	public static String getLine()
	{
		return input.nextLine();
	}
	
	// Gets the next line from the input
	public static int[] getPoint()
	{
		return new int[] {IO.getInt(), IO.getInt()};
	}
	
	// Gets the next String from the input
	public static int getInt()
	{
		return input.nextInt();
	}
	
	// Prints the given string WITHOUT a new line
	public static void printpart(String in)
	{
		System.out.print(in);
	}
	
	// Prints a new line
	public static void print()
	{
		System.out.println();
	}
	
	// Prints the given string WITH a new line
	public static void print(int in)
	{
		System.out.println(in);
	}
	
	// Prints the given string WITH a new line
	public static void print(String in)
	{
		System.out.println(in);
	}
	
	// Prints the given array
	public static void print(int[] array)
	{
		System.out.println(arrayToString(array));
	}
	
	// Prints the given array
	public static void print(Object[] array)
	{
		System.out.println(arrayToString(array));
	}
	
	// Converts the array to a nice looking string
	public static String arrayToString(int[] array)
	{
		String s = "[";
		
		if(array.length  > 0)
		{
			s += array[0];
			
			for (int i = 1; i < array.length; i ++)
				s += ", " + array[i];
		}
		
		s += "]";
		
		return s;
	}
	
	// Converts the array to a nice looking string
	public static String arrayToString(Object[] array)
	{
		String s = "[";
		
		if(array.length  > 0)
		{
			s += array[0];
			
			for (int i = 1; i < array.length; i ++)
				s += ", " + array[i];
		}
		
		s += "]";
		
		return s;
	}
}
