import java.util.Scanner;

public class Calculometron
{
	public Scanner input;
	public Problem currentProblem;
	private long score;
	private String[] wrongMessages = {"WRONG.", "Incorrect", "Nope", "Try again!"};
	
	public Calculometron()
	{
		//Do save loading or make new game
		input = new Scanner(System.in);
		score = 0;
	}
	
	public static void main(String[] args)
	{
		Calculometron game = new Calculometron();
		game.play();
	}
	
	public void play()
	{
		start();
		String userInput;
		
		do
		{
			handleInput(userInput);
			System.out.println("\n\n\nScore:\t" + score);
		} while (true);
	}
	
	private void getProblem(String userInput)
	{
		if(userInput.toLowerCase().contains("add"))
		{
			userInput = input.nextLine();
			shouldQuit(userInput);
			currentProblem = new AddProblem();
			prompt();
		}
		else if(userInput.toLowerCase().contains("mul"))
		{
			userInput = input.nextLine();
			shouldQuit(userInput);
			currentProblem = new MultProblem();
			prompt();
		}
	}
	
	private void prompt()
	{
		currentProblem.prompt();
	}
	
	private void handleInput(String in)
	{
		in = in.toLowerCase();
		shouldQuit(in);
		if(currentProblem == null)
		{
			System.out.println("What kinda problem do you want?");
			getProblem(in);
		}
		else if(currentProblem.checkAnswer(in))
		{
			score += currentProblem.getValue();
			currentProblem = null;
		}
		else
		{
			printIncorrect();
			currentProblem = null;
		}
		
	}
	
	private void start()
	{
		System.out.println("Welcome to the game!");
	}
	
	private void shouldQuit(String in)
	{
		in = in.toLowerCase();
		if (in.equals("quit") || in.equals("q") || in.equals("exit") || in.equals("close"))
		{
			//Save n shit
			System.exit(0);
		}
	}
	
	private void printIncorrect()
	{
		System.out.println(wrongMessages[(int)(wrongMessages.length*Math.random())]);
		System.out.println();
	}
}
