

public class AddProblem extends Problem
{
	int a;
	int b;
	
	public AddProblem()
	{
		super();
		value = 1;
		System.out.println("AddProblem value=" + value);
	}
	
	/**
	* @override
	*/
	public void createProblem()
	{
		a = (int)(Math.random()*10);
		b = (int)(Math.random()*10);
		
		answer = "" + (a + b);
		System.out.println("AddProblem answer=" + answer);
		
	}
	
	public void prompt()
	{
		System.out.println(a + " + " + b);
	}
}
