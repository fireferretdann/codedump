
public class Problem
{
	public String answer;
	public long value;
	
	public Problem()
	{
		createProblem();
		value = 0;
	}
	
	public void createProblem()
	{
		answer = "" + (int)(Math.random()*10);
	}
	
	public void prompt()
	{
		System.out.println("What is the number?");
		
		System.out.println("The answer is: " + answer);
	}
	
	public boolean checkAnswer(String ans)
	{
		return ans.equals(answer);
	}
	
	public long getValue()
	{
		return value;
	}
}
