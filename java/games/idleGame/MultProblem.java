


public class MultProblem extends Problem
{
	int a;
	int b;
	
	public MultProblem()
	{
		super();
		value = 2;
		System.out.println("MultProblem value=" + value);
	}
	
	/**
	* @override
	*/
	public void createProblem()
	{
		a = (int)(Math.random()*10);
		b = (int)(Math.random()*10);
		
		answer = "" + (a * b);
		System.out.println("MultProblem answer=" + answer);
		
	}
	
	public void prompt()
	{
		System.out.println(a + " * " + b);
	}
}
