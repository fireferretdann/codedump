import java.util.ArrayList;
import java.util.Scanner;

public class Smusher
{
	public static ArrayList<Bug> bugs = new ArrayList<Bug>();
	public static Scanner input = new Scanner(System.in);
	public static int totalBugs = 0;
	
	
	public static void main(String[] args)
	{
		bugs.add(new Bug());
		do
		{
			bugs.add(new Bug());
		} while(Math.random() < .85);
		
		System.out.println("Uh-Oh, looks like your code has some bugs!");
		System.out.println();
		
		while(bugs.size() > 0)
		{
			if(bugs.size() == 1)
				System.out.println("There is " + bugs.size() + " bug in your code!");
			else
				System.out.println("There are " + bugs.size() + " bugs in your code!");
			System.out.println("Press enter to smush one!");
			input.nextLine();
			
			smush();
		}
		
		System.out.println();
		System.out.println("You smushed " + totalBugs + " in total!");
	}
	
	
	public static void smush()
	{
		// This is temporary
		
		if(Math.random() < .25)
		{
			bugs.add(new Bug());
			bugs.add(new Bug());
			bugs.add(new Bug());
			bugs.add(new Bug());
		}
		
		bugs.remove(0);
		totalBugs ++;
	}
}
