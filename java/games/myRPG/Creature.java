
public class Creature
{
	public int hp;
	
	
	public Creature()
	{
		// Creates default bad guy
		hp = 10;
	}
	
	
	public Creature(boolean isGood)
	{
		// Creates default character if isGood
		hp = 0;
		if(isGood)
		{
			hp = 20;
		}
		else
		{
			hp = 10;
		}
	}
	
	
	public int getHP()
	{
		return hp;
	}
}
