
public class RandomEvent
{
	public RandomEvent()
	{
		System.out.println("Created Event!");
	}
	
	
	public void prompt()
	{
		System.out.print("A thing happened! What will you do: ");
	}
	
	
	public void resolve(String action)
	{
		System.out.println("You did \"" + action + "\"");
	}
}
