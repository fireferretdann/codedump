import java.util.Scanner;


public class GameLoop
{
	public Scanner input;
	public Creature player;
	
	
	public GameLoop()
	{
		player = new Creature(true);
		System.out.println("Player created");
		input = new Scanner(System.in);
	}
	
	
	public void play()
	{
		System.out.println("Playing game!");
		System.out.println(player.getHP());
		
		RandomEvent e = new RandomEvent();
		e.prompt();
		String action = getInput();
		e.resolve(action);
	}
	
	public String getInput()
	{
		String out = input.nextLine();
		if(out.equals("quit"));
			System.exit(0);
		return out;
	}
}
