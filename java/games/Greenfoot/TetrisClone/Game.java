import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This class is the main game.
 * 
 * @author Daniel Clark
 */
public class Game extends World
{
    private Piece fallingPiece;
    private Piece onDeck;
    private int counter;
    private int fallPeriod = 1000;
    
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public Game()
    {
        // The board is 10 x 22, starting at (1,1) and ending at (11,23)
        super(20, 23, 15);
        
        GreenfootImage image = this.getBackground();
        image.setColor(Color.BLACK);
        image.fill();
        
        this.setBackground(image);
    }
    
    public void act()
    {
        getInput();
        fall();
    }
    
    public void getInput()
    {
        String key = Greenfoot.getKey();
        if(key != null)
        {
            if(key.equals("up") || key.equals("w"))
                pieceRotate();
            if(key.equals("down") || key.equals("s"))
                pieceFall();
            if(key.equals("left") || key.equals("a"))
                fallingPiece.moveLeft();
            if(key.equals("right") || key.equals("d"))
                fallingPiece.moveRight();
        }
    }
    
    public void fall()
    {
        counter ++;
        if(counter % fallPeriod == 0)
            pieceFall();
    }
    
    public void pieceFall()
    {
        if(canFall())
            fallingPiece.setLocation(fallingPiece.getX(), fallingPiece.getY());
        else
            pieceLand();
    }
    
    public void pieceRotate()
    {
    }
    
    public boolean canFall()
    {
        return true;
    }
    
    public void pieceLand()
    {
        fallingPiece = onDeck;
        onDeck = new Piece();
        fallingPiece.setLocation(5, 4); // Temp
    }
}
