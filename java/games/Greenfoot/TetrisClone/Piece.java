import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Piece here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Piece extends Actor
{
    public void moveRight()
    {
        // Need to do check
        setLocation(getX()+1, getY());
    }
    
    public void moveLeft()
    {
        // Need to do check
        setLocation(getX()-1, getY());
    }
}
