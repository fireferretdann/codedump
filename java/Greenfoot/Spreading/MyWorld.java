import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class MyWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MyWorld extends World
{
    public void act()
    {
        MouseInfo mouse = Greenfoot.getMouseInfo();
        
        if(mouse != null  &&  mouse.getButton() == 1)
        {
            if(mouse.getActor() instanceof Area)
                ((Area)mouse.getActor()).fill();
        }
        if(mouse != null  &&  mouse.getButton() == 3)
        {
            if(mouse.getActor() instanceof Area)
                ((Area)mouse.getActor()).empty();
        }
        
        for(Area a : getObjects(Area.class))
            a.prepare();
        for(Area a : getObjects(Area.class))
            a.update();
    }
    
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(10, 10, 60);
        
        for(int x = 0; x < 10; x ++)
            for(int y = 0; y < 10; y ++)
                this.addObject(new Area(), x, y);
    }
}
