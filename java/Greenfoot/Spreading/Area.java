import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
/**
 * Write a description of class Area here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Area extends Actor
{
    double value;
    double nextValue;
    double viscosity; // The weight of itself when updating
    
    public void prepare()
    {
        nextValue = 0;
        List<Area> neighbors = this.getNeighbours(1, true, Area.class);
        
        for(Area a : neighbors)
        {
            nextValue += a.value;
        }
        
        nextValue += this.value*viscosity;
        
        nextValue /= neighbors.size() + viscosity;
    }
    
    public void update()
    {
        value = nextValue;
        updateImage();
    }
    
    public Area()
    {
        value = 0;
        nextValue = 0;
        viscosity = 4000;
        updateImage();
    }
    
    public void fill()
    {
        this.value = 255;
        updateImage();
    }
    
    public void empty()
    {
        this.value = 0;
        updateImage();
    }
    
    public void updateImage()
    {
        this.getImage().setTransparency((int)value);
    }
}
